FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env

# Create app directory
WORKDIR /usr/src/karaokemaster-api

COPY . ./

# dotnet publish likes to grab the wrong version of dependencies sometimes
# running it a second time with a specific project fixes that (and still grabs everything else)
# https://github.com/dotnet/sdk/issues/3886
# RUN dotnet publish -f net7.0 -c Release
RUN dotnet publish KaraokeMaster.Api -f net7.0 -c Release -o out

RUN \
    . ./build.env && \
    mv .sentryclirc ~/ && \
    curl -sL https://sentry.io/get-cli/ | bash && \
    sentry-cli releases new $SENTRY_RELEASE && \
    sentry-cli releases set-commits $SENTRY_RELEASE --auto && \
    sentry-cli releases finalize $SENTRY_RELEASE

# Runtime

FROM mcr.microsoft.com/dotnet/aspnet:7.0

# Create app directory
WORKDIR /usr/src/karaokemaster-api

# Copy assets from the build environment
COPY --from=build-env /usr/src/karaokemaster-api/out ./
COPY --from=build-env /usr/src/karaokemaster-api/build.env ./
COPY --from=build-env /usr/src/karaokemaster-api/entrypoint.sh ./
COPY --from=build-env /usr/src/karaokemaster-api/healthcheck.sh ./
RUN chmod +x /usr/src/karaokemaster-api/entrypoint.sh
RUN chmod +x /usr/src/karaokemaster-api/healthcheck.sh
    
ENV COMPlus_DbgEnableMiniDump=1
ENV COMPlus_DbgMiniDumpName=/var/opt/kmdata/coredump.%d

EXPOSE 5000
EXPOSE 5353
EXPOSE 53212/udp

HEALTHCHECK --interval=30s --timeout=10s --start-period=60s --start-interval=5s \
    CMD /bin/bash -c "/usr/src/karaokemaster-api/healthcheck.sh 5000"
    
ENTRYPOINT [ "/usr/src/karaokemaster-api/entrypoint.sh" ]