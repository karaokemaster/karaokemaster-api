// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;

namespace KaraokeMaster.Api
{
    public class AutofacConfig
    {
        public static void Build(ContainerBuilder builder)
        {
            // builder.Register(c => c.Resolve<IOptions<Options>>().Value as Domain.Configuration);
            
            var uri = new Uri(Assembly.GetExecutingAssembly().Location);
            string path = Path.GetDirectoryName(uri.LocalPath);
            // Log.Debug($"Searching path {path} for modules");
            var assemblyPaths = Directory.EnumerateFiles(path, "*.dll", SearchOption.TopDirectoryOnly)
                .Select(a => new FileInfo(a))
                .Where(a => a.Name.StartsWith("KaraokeMaster."))
                .Select(a => a.FullName)
                .ToList();
            // Log.Debug($"Found {assemblyPaths.Count} assemblies");
            var assemblies = assemblyPaths.Select(a =>
                {
                    try
                    {
                        return Assembly.LoadFrom(a);
                    }
                    catch
                    {
                        return null;
                    }
                }
            ).Where(a => a != null).ToList();
            // Log.Info($"Loaded {assemblyPaths.Count} assemblies");
            assemblies.ForEach(assembly => builder.RegisterAssemblyModules(assembly));
            // Log.Info($"Registered {assemblyPaths.Count} assemblies");
        }
    }
}