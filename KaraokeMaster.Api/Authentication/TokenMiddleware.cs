// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using KaraokeMaster.Api.Controllers;
using KaraokeMaster.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace KaraokeMaster.Api.Authentication
{
    public class TokenMiddleware
    {
        private const string AuthorizationHeader = "Authorization";

        private readonly RequestDelegate _next;
        private readonly IOptions<TokenConfiguration> _tokenOptions;
        private readonly IUserService _userService;

        public TokenMiddleware(RequestDelegate next, IOptions<TokenConfiguration> tokenOptions, IUserService userService)
        {
            _next = next;
            _tokenOptions = tokenOptions;
            _userService = userService;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Headers.ContainsKey(AuthorizationHeader))
            {
                var authHeader = context.Request.Headers[AuthorizationHeader].ToString();
                if (authHeader.StartsWith("bearer", StringComparison.InvariantCultureIgnoreCase))
                {
                    var bearerToken = authHeader.Substring(7); // Strip header down to token

                    await AttachUserToContext(context, bearerToken);
                }
            }
            else if (context.Request.Query["access_token"].Count > 0)
            {
                var token = context.Request.Query["access_token"].First();

                await AttachUserToContext(context, token);
            }
            
            await _next(context);
        }

        private async Task AttachUserToContext(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_tokenOptions.Value.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken) validatedToken;

                var isEmcee = jwtToken.Claims.Any(c => c.Type == "emcee");
                if (isEmcee)
                {
                    // Emcee token
                    // TODO: Handle this in a more sophisticated way
                    // TODO: Remove user role from Emcee auth
                    var emcee = new EmceeController.EmceeUser();
                    
                    context.Items["User"] = emcee;
                    context.User = new GenericPrincipal(emcee, new[] { "Emcee", "User" });

                    return;
                }
                
                var isAdmin = jwtToken.Claims.Any(c => c.Type == "admin");
                if (isAdmin)
                {
                    // Admin token
                    // TODO: Handle this in a more sophisticated way
                    // TODO: Remove user role from Emcee auth
                    var admin = new AdminController.AdminUser();
                    
                    context.Items["User"] = admin;
                    context.User = new GenericPrincipal(admin, new[] { "Admin", "Emcee", "User" });

                    return;
                }
                
                var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
                // attach user to context on successful jwt validation
                var user = await _userService.GetUserById(userId);
                context.Items["User"] = user;
                context.User = new GenericPrincipal(user, new []{ "User" });
            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}