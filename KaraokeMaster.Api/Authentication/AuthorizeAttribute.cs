// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Security.Principal;
using KaraokeMaster.Api.Controllers;
using KaraokeMaster.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace KaraokeMaster.Api.Authentication
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string _role;

        public AuthorizeAttribute()
        {
            _role = null;
        }

        public AuthorizeAttribute(string role)
        {
            _role = role;
        }
        
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (_role != null && context.HttpContext.User is GenericPrincipal user && user.IsInRole(_role))
                return;
            
            if (context.HttpContext.Items["User"] is User or EmceeController.EmceeUser or AdminController.AdminUser)
                return;
            
            // Check if anonymous access is allowed
            if (context.Filters.ToList().Any(f => f is AllowAnonymousAttribute)) return;

            // not logged in
            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        }
    }
}