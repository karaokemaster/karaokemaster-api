// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using KaraokeMaster.Domain.Models;

namespace KaraokeMaster.Api.Authentication
{
    public class AuthenticateResponse
    {
        public AuthenticateResponse(string token)
        {
            UserId = int.MinValue;
            Username = null;
            Icon = null;
            Color = null;
            Token = token;
        }

        public AuthenticateResponse(User user, string token)
        {
            UserId = user.UserId;
            Username = user.Username;
            Icon = user.Icon;
            Color = user.Color;
            Token = token;
        }

        public int UserId { get; }
        public string Username { get; }
        public string Icon { get; }
        public string Color { get; }
        public string Token { get; }
    }
}