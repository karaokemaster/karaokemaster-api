// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Api.Hubs;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Queue;
using KaraokeMaster.Services.Recommendations;
using KaraokeMaster.Services.Search;
using KaraokeMaster.Services.Session;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Api.Controllers
{
    [Authorize]
    [Route("v1/[controller]")]
    public class QueueController : Controller
    {
        private readonly IQueueService _queueService;
        private readonly IUserService _userService;
        private readonly IRecommendationService _recommendationService;
        private readonly ISessionService _sessionService;
        private readonly IHubContext<QueueHub> _queueHubContext;
        private readonly IHubContext<EmceeHub> _emceeHubContext;

        public QueueController(IQueueService queueService, IUserService userService, IRecommendationService recommendationService, ISessionService sessionService, IHubContext<QueueHub> queueHubContext, IHubContext<EmceeHub> emceeHubContext)
        {
            _queueService = queueService;
            _userService = userService;
            _recommendationService = recommendationService;
            _sessionService = sessionService;
            _queueHubContext = queueHubContext;
            _emceeHubContext = emceeHubContext;
        }
        
        [HttpPost("")]
        public async Task<ActionResult> Enqueue([FromBody] SearchResultDto video, string username)
        {
            try
            {
                var user = User.Identity as User ?? await _userService.EnsureUserExists(username);

                await _queueService.Add(video, user.Username);

                await SendQueueUpdate();

                // TODO: Continue?
                // TODO: Move to different hub?
                var recs = await _recommendationService.GetRecommendations(user.Username, false);
                await _queueHubContext.Clients.Group($"user_{user.UserId}").SendAsync("UpdateRecommendations", recs);

                // TODO: Continue?
                // TODO: Move to different hub?
                var previouslyPlayed = await _recommendationService.GetPreviouslyPlayed(user.Username);
                await _queueHubContext.Clients.Group($"user_{user.UserId}").SendAsync("UpdatePreviouslyPlayed", previouslyPlayed);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }
        
        [HttpDelete("{queueId}")]
        public async Task<ActionResult> Dequeue(int queueId)
        {
            try
            {
                await _queueService.Remove(queueId);

                await SendQueueUpdate();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }

        [HttpPost("{queueId}/move/before/{beforeId}")]
        [HttpPost("{queueId}/move/after/{afterId}")]
        [HttpPost("{queueId}/move/between/{afterId}/{beforeId}")]
        public async Task<ActionResult> Move(int queueId, int? afterId, int? beforeId)
        {
            try
            {
                await _queueService.Move(queueId, afterId, beforeId);

                await SendQueueUpdate();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }
        
        [HttpPost("{queueId}/lyrics")]
        public async Task<ActionResult> SetLyrics(int queueId, [FromBody] string lyrics)
        {
            try
            {
                await _queueService.SetLyrics(queueId, lyrics);

                await SendQueueUpdate();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }
        
        [HttpPost("{queueId}/rating/{rating}")]
        public async Task<ActionResult> SetRating(int queueId, int rating)
        {
            try
            {
                await _queueService.SetRating(queueId, rating);

                await SendQueueUpdate();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }
        
        [AllowAnonymous]
        [HttpPost("{queueId}/lightingCue/{cue}")]
        public async Task<ActionResult> SetLightingCue(int queueId, string cue)
        {
            try
            {
                await _queueService.SetLightingCue(queueId, cue);

                await SendQueueUpdate();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }
        
        [HttpPost("{queueId}/user/{user}")]
        public async Task<ActionResult> SetQueueEntryUser(int queueId, string user)
        {
            try
            {
                await _queueService.SetUser(queueId, user);

                await SendQueueUpdate();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }

        private async Task SendQueueUpdate()
        {
            var queue = await _queueService.Get();
            await _queueHubContext.Clients.All.SendAsync("UpdateQueue", queue);
 
            var played = await _sessionService.GetAllSessionVideos(DateTime.Now.AddHours(-6).Date);
            await _emceeHubContext.Clients.All.SendAsync("UpdatePlayedVideos", played);
        }
    }
}