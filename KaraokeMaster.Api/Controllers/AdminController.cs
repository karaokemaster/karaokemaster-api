// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Api.Hubs;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Kiosk;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace KaraokeMaster.Api.Controllers;

[Authorize("Admin")]
[Route("v1/[controller]")]
public class AdminController : Controller
{
    private readonly ILogger<AdminController> _logger;
    private readonly IOptions<TokenConfiguration> _tokenOptions;
    private readonly IKioskService _kioskService;
    private readonly IHubContext<KioskHub> _kioskHubContext;

    public AdminController(ILoggerFactory loggerFactory, IOptions<TokenConfiguration> tokenOptions, IKioskService kioskService, IHubContext<KioskHub> kioskHubContext)
    {
        _logger = loggerFactory.CreateLogger<AdminController>();
        _tokenOptions = tokenOptions;
        _kioskService = kioskService;
        _kioskHubContext = kioskHubContext;
    }

    [AllowAnonymous]
    [HttpPost("[action]")]
    public async Task<ActionResult> Login()
    {
        // TODO: Get certificate, etc.
        var token = GenerateJwtToken();

        var response = new AuthenticateResponse(token);

        return Json(response);
    }

    // TODO: Rework parameters
    [HttpPost("[action]")]
    public async Task<ActionResult> SetKioskDisplayUrl([FromForm] int kioskId, [FromForm] int displayId, [FromForm] string url)
    {
        var kiosk = await _kioskService.Get(kioskId);

        if (kiosk.Displays.All(d => d.DisplayId != displayId))
            kiosk.Displays.Add(new KioskDisplay { KioskId = kioskId, DisplayId = displayId, DisplayUrl = url });
        else
            kiosk.Displays.First(d => d.DisplayId == displayId).DisplayUrl = url;

        kiosk = await _kioskService.Update(kiosk);
        
        foreach (var display in kiosk.Displays)
        {
            await KioskHub.SendDisplaySettings(_kioskHubContext.Clients, kiosk, display);
        }

        return Json(kiosk);
    }

    [HttpPost("[action]")]
    public async Task<ActionResult> ReloadKioskDisplay([FromQuery] int kioskId, [FromQuery] int displayId)
    {
        var kiosk = await _kioskService.Get(kioskId);
        var display = kiosk.Displays.First(d => d.DisplayId == displayId);

        await KioskHub.SendDisplaySettings(_kioskHubContext.Clients, kiosk, display);

        return Json(display);
    }
    
    [HttpPost("[action]")]
    public async Task<ActionResult> LogoutKiosk([FromQuery] int kioskId)
    {
        _logger.LogInformation($"Kiosk {kioskId} logout requested");

        var kiosk = await _kioskService.Get(kioskId);
        var publicKey = KioskHub.ToHex(kiosk.PublicKey);

        // TODO: Authenticate
        await _kioskHubContext.Clients.Groups(publicKey).SendAsync("Logout");
        
        return Ok();
    }
    
    [HttpPost("[action]")]
    public async Task<ActionResult> QuitKiosks()
    {
        _logger.LogInformation("Kiosk quit requested");
        
        // TODO: Authenticate
        await _kioskHubContext.Clients.All.SendAsync("Quit");
        
        return Ok();
    }
    
    [AllowAnonymous]
    [HttpPost("[action]")]
    public async Task<ActionResult> ShutdownKiosks()
    {
        _logger.LogInformation("Kiosk shutdown requested");
        
        // TODO: Authenticate
        await _kioskHubContext.Clients.All.SendAsync("Shutdown");
        
        return Ok();
    }

    [AllowAnonymous]
    [HttpGet("~/healthcheck")]
    public async Task<ActionResult> HealthCheck()
    {
        // TODO: The actual healthcheck!

        return Ok(new { status = "UP" });
    }
    
    private string GenerateJwtToken()
    {
        // generate token that is valid for 7 days
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_tokenOptions.Value.Secret);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[] { new Claim("admin", "true") }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    public class AdminUser : IIdentity
    {
        public string AuthenticationType => "Admin";
        public bool IsAuthenticated => true;
        public string Name { get; }
    }
}