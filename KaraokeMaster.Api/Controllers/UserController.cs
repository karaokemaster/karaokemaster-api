// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Api.Hubs;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Queue;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Sentry;
using WebPush;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Api.Controllers
{
    [Authorize]
    [Route("v1/[controller]")]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        private readonly IUserStateService _userStateService;
        private readonly IQueueService _queueService;
        private readonly IOptions<TokenConfiguration> _tokenOptions;
        private readonly IHubContext<Hubs.Hub> _hubContext;
        private readonly IHubContext<QueueHub> _queueHubContext;
        private readonly IHubContext<EmceeHub> _emceeHubContext;

        public UserController(ILoggerFactory loggerFactory, IUserService userService, IUserStateService userStateService, IQueueService queueService, IOptions<TokenConfiguration> tokenOptions, IHubContext<Hubs.Hub> hubContext, IHubContext<QueueHub> queueHubContext, IHubContext<EmceeHub> emceeHubContext)
        {
            _logger = loggerFactory.CreateLogger<UserController>();
            _userService = userService;
            _userStateService = userStateService;
            _queueService = queueService;
            _tokenOptions = tokenOptions;
            _hubContext = hubContext;
            _queueHubContext = queueHubContext;
            _emceeHubContext = emceeHubContext;
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ActionResult> Login([FromHeader] string username)
        {
            var user = await _userService.EnsureUserExists(username);
            await _userStateService.Login(user.Username);

            await SendUserList();
            
            var token = GenerateJwtToken(user);

            var response = new AuthenticateResponse(user, token);

            return Json(response);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> SubscribePush([FromBody] PushSubscription subscription)
        {
            var authUser = User.Identity as User;

            _logger.LogTrace($"SubscribePush for user [{authUser?.UserId}], endpoint: {subscription?.Endpoint}");
            
            if (authUser == null) return Unauthorized();
            
            var user = await _userService.EnsureUserExists(authUser.Username);
            await _userStateService.Login(user.Username, subscription);

            return Ok();
        }
        
        [HttpGet("[action]")]
        public async Task<ActionResult> GetActive()
        {
            var users = await _userStateService.GetAllUsers();

            return Json(users);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAll()
        {
            var users = await _userService.GetAll();

            return Json(users);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> RatedVideos()
        {
            var user = User.Identity as User;
            
            var videos = await _userService.GetRatedVideos(user.Username);

            return Json(videos);
        }
        
        [HttpPost("")]
        public async Task<ActionResult> UpdateUser([FromBody] User user)
        {
            try
            {
                user = await _userService.UpdateUser(user);

                await _hubContext.Clients.Groups($"user_{user.UserId}").SendAsync("UserProfile", user);

                await SendQueueUpdate();
                await SendUserList();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> ChangeUsername([FromBody] User user, string username)
        {
            try
            {
                var dbUser = await _userService.GetUserById(user.UserId);
                if (dbUser == null)
                    return NotFound();
                
                // TODO: Refactor to use a return value instead of inferring meaning of null response
                dbUser = await _userService.ChangeUsername(dbUser, username);
                // null response from ChangeUsername = user already exists
                if (dbUser == null)
                    return Conflict();

                await _hubContext.Clients.Groups($"user_{dbUser.UserId}").SendAsync("UserProfile", dbUser);

                await SendQueueUpdate();
                await SendUserList();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> SearchIcons([FromHeader] string query)
        {
            string[] icons;
            
            try
            {
                var results = await _userService.SearchIcons(query);

                icons = results.ToArray();
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }

            return Json(icons);
        }
        
        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_tokenOptions.Value.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.UserId.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private async Task SendQueueUpdate()
        {
            var queue = await _queueService.Get();

            await _queueHubContext.Clients.All.SendAsync("UpdateQueue", queue);
        }

        private async Task SendUserList()
        {
            var users = await _userStateService.GetAllUsers();

            await _emceeHubContext.Clients.All.SendAsync("UpdateUsers", users);
        }
    }
}
