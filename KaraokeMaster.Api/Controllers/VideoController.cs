// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Threading.Tasks;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Recommendations;
using KaraokeMaster.Services.Search;
using Microsoft.AspNetCore.Mvc;

namespace KaraokeMaster.Api.Controllers
{
    [Authorize]
    [Route("v1/[controller]")]
    public class VideoController : Controller
    {
        private readonly ISearchService _searchService;
        private readonly IRecommendationService _recommendationService;

        public VideoController(ISearchService searchService, IRecommendationService recommendationService)
        {
            _searchService = searchService;
            _recommendationService = recommendationService;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SearchResultDto>> Search(string query)
        {
            var user = User.Identity as User;

            return await _searchService.Search(query, user?.Username);
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SearchResultDto>> Recommendations(string username = null)
        {
            var user = User.Identity as User;

            // TODO: Eliminate?, using hub instead
            var recommendations = await _recommendationService.GetRecommendations(username ?? user?.Username);
            return recommendations;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SearchResultDto>> TopVideos(string username = null)
        {
            var user = User.Identity as User;

            var topVideos = await _recommendationService.GetTopVideos(username ?? user?.Username);
            return topVideos;
        }
    }
}
