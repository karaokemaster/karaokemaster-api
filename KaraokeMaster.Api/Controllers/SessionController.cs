// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Services.Session;
using Microsoft.AspNetCore.Mvc;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Api.Controllers
{
    [Authorize]
    [Route("v1/[controller]")]
    public class SessionController : Controller
    {
        private readonly ISessionService _sessionService;

        public SessionController(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> Get()
        {
            try
            {
                var user = User.Identity as User;
                var sessions = await _sessionService.GetSessions(user.Username);

                return Json(sessions);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }
        }
        
        [HttpGet("{session:DateTime}")]
        public async Task<ActionResult> GetSessionVideos(DateTime session)
        {
            try
            {
                var user = User.Identity as User;
                var sessionDto = await _sessionService.GetSession(user.Username, session);

                return Json(sessionDto);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
                return Problem();
            }
        }
    }
}
