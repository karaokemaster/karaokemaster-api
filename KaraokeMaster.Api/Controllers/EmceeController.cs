// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Api.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace KaraokeMaster.Api.Controllers;

[Authorize("Emcee")]
[Route("v1/[controller]")]
public class EmceeController : Controller
{
    private readonly ILogger<EmceeController> _logger;
    private readonly IOptions<TokenConfiguration> _tokenOptions;

    public EmceeController(ILoggerFactory loggerFactory, IOptions<TokenConfiguration> tokenOptions)
    {
        _logger = loggerFactory.CreateLogger<EmceeController>();
        _tokenOptions = tokenOptions;
    }

    [AllowAnonymous]
    [HttpPost("[action]")]
    public async Task<ActionResult> Login()
    {
        var token = GenerateJwtToken();

        var response = new AuthenticateResponse(token);

        return Json(response);
    }

    private string GenerateJwtToken()
    {
        // generate token that is valid for 7 days
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_tokenOptions.Value.Secret);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[] { new Claim("emcee", "true") }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    public class EmceeUser : IIdentity
    {
        public string AuthenticationType => "Emcee";
        public bool IsAuthenticated => true;
        public string Name { get; }
    }
}