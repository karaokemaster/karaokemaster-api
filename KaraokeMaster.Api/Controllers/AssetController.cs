// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Net.Codecrete.QrCodeGenerator;
using QRCoder;

namespace KaraokeMaster.Api.Controllers
{
    [Route("v1/[controller]")]
    public class AssetController : Controller
    {
        private const string MIME_TYPE_SVG = "image/svg+xml";

        private readonly IConfiguration config;
        private readonly IMemoryCache cache;
        private readonly MemoryCacheEntryOptions cacheOptions;

        public AssetController(IConfiguration configuration, IMemoryCache memoryCache)
        {
            config = configuration;
            cache = memoryCache;

            cacheOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(10));
        }

        [HttpGet("[action]")]
        public IActionResult WiFiNetworkQrCode()
        {
            // TODO: Move all this crap into a service
            var ssid = config["Splash:WiFiNetworkSsid"];
            var psk = config["Splash:WiFiNetworkPsk"];

            var wifiPayload = new PayloadGenerator.WiFi(ssid, psk, PayloadGenerator.WiFi.Authentication.WPA);

            var qr = QrCode.EncodeText(wifiPayload.ToString(), QrCode.Ecc.Quartile);
            string qrCodeAsSvg = qr.ToSvgString(4);
            
            // var qrGenerator = new QRCodeGenerator();
            // var qrCodeData = qrGenerator.CreateQrCode(wifiPayload, QRCodeGenerator.ECCLevel.Q);
            // var qrCode = new SvgQRCode(qrCodeData);
            // string qrCodeAsSvg = qrCode.GetGraphic(20);

            // TODO: Cache the output
            byte[] qrCodeBytes = UTF8Encoding.UTF8.GetBytes(qrCodeAsSvg);
            return File(qrCodeBytes, MIME_TYPE_SVG);
        }

        [HttpGet("[action]")]
        public IActionResult PublicUrlQrCode()
        {
            // TODO: Move all this crap into a service
            var publicUrl = config["Splash:PublicUrl"];

            var urlPayload = new PayloadGenerator.Url(publicUrl);

            var qr = QrCode.EncodeText(urlPayload.ToString(), QrCode.Ecc.Quartile);
            string qrCodeAsSvg = qr.ToSvgString(4);

            // var qrGenerator = new QRCodeGenerator();
            // var qrCodeData = qrGenerator.CreateQrCode(urlPayload, QRCodeGenerator.ECCLevel.Q);
            // var qrCode = new SvgQRCode(qrCodeData);
            // string qrCodeAsSvg = qrCode.GetGraphic(20);

            // TODO: Cache the output
            byte[] qrCodeBytes = UTF8Encoding.UTF8.GetBytes(qrCodeAsSvg);
            return File(qrCodeBytes, MIME_TYPE_SVG);
        }
    }
}
