// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using Microsoft.AspNetCore.SignalR;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Api.Hubs
{
    public class Hub : HubBase
    {
        public override async Task OnConnectedAsync()
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("hub:OnConnectedAsync", "Hub.OnConnectedAsync");
                scope.Transaction = transaction;

                try
                {
                    if (Context.User?.Identity is User user)
                    {
                        await Groups.AddToGroupAsync(Context.ConnectionId, $"user_{user.UserId}");
                    }
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                }
                
                try
                {
                    await Clients.Caller.SendAsync("ServerInfo", ServerInfo);
                    
                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }
  
        public async Task Beat(double tempo)
        {
            try
            {
                // TODO: Better select which clients get this, since only the MIDI Conductor does
                await Clients.AllExcept(Context.ConnectionId).SendAsync("Beat", tempo);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
            }
        }

        public async Task BeatEcho(long timestamp, bool alive)
        {
            try
            {
                // TODO: Better select which clients get this, since only the Beat Detector does
                await Clients.AllExcept(Context.ConnectionId).SendAsync("BeatEcho", timestamp, alive);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
            }
        }
        
        public async Task RequestCues()
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("hub:RequestCues", "Hub.RequestCues");

                scope.Transaction = transaction;

                try
                {
                    // TODO: This really only needs to go to the MIDI Controller...
                    await Clients.AllExcept(Context.ConnectionId).SendAsync("RequestCues");
                    
                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }

        public async Task UpdateCues(Cue[] cues)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("hub:UpdateCues", "Hub.UpdateCues");

                scope.Transaction = transaction;

                try
                {
                    await Clients.AllExcept(Context.ConnectionId).SendAsync("UpdateCues", cues);
                    
                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }

        public async Task SendCue(int index)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("hub:SendCue", "Hub.SendCue");
                transaction.SetExtra(nameof(index), index);

                scope.Transaction = transaction;

                try
                {
                    // TODO: This really only needs to go to the MIDI Controller...
                    // TODO: Ideally, we'd send a separate confirmation message back to the clients instead of reusing this
                    await Clients.All.SendAsync("SendCue", index);
                    
                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }
    }
}