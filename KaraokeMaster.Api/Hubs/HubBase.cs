// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reflection;

namespace KaraokeMaster.Api.Hubs
{
    public abstract class HubBase : Microsoft.AspNetCore.SignalR.Hub
    {
        private readonly string _version;
        private readonly string _versionSha;
        private readonly DateTime _commitDate;
        private readonly string _sentryRelease;

        public HubBase()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var gitVersionInformationType = assembly.GetType("GitVersionInformation");
            _version = gitVersionInformationType?.GetField("SemVer")?.GetValue(null)?.ToString();
            _versionSha = gitVersionInformationType?.GetField("Sha")?.GetValue(null)?.ToString();
            DateTime.TryParse(gitVersionInformationType?.GetField("CommitDate")?.GetValue(null)?.ToString(), out _commitDate);
            // TODO: Use the result from Program.cs
            _sentryRelease = Environment.GetEnvironmentVariable("SENTRY_RELEASE") ?? $"kmapi-{_version}";
        }

        public _ServerInfo ServerInfo => new _ServerInfo
        {
            ApiRelease = _sentryRelease,
            ApiVersion = _version,
            ApiSha = _versionSha,
            ApiDate = _commitDate,
        };
        
        public class _ServerInfo
        {
            public string ApiRelease { get; init; }
            public string ApiVersion { get; init; }
            public string ApiSha { get; init; }
            public DateTime ApiDate { get; init; }
        }
    }
}