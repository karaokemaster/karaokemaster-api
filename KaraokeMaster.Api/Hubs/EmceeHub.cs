// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Microphone;
using KaraokeMaster.Services.Session;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using RxSignalrStreams.Extensions;

namespace KaraokeMaster.Api.Hubs;

public class EmceeHub : Microsoft.AspNetCore.SignalR.Hub
{
    private readonly ILogger<EmceeHub> _logger;
    private readonly IUserStateService _userStateService;
    private readonly ISessionService _sessionService;
    private readonly IMicrophoneService _microphoneService;

    public EmceeHub(ILoggerFactory loggerFactory, IUserStateService userStateService, ISessionService sessionService, IMicrophoneService microphoneService)
    {
        _logger = loggerFactory.CreateLogger<EmceeHub>();
        _userStateService = userStateService;
        _sessionService = sessionService;
        _microphoneService = microphoneService;
    }

    public override async Task OnConnectedAsync()
    {
        await SendUserList();
        await SendPlayedVideoList();
    }

    public async Task SendUserList()
    {
        var users = await _userStateService.GetAllUsers();

        await Clients.Caller.SendAsync("UpdateUsers", users);
    }

    public async Task SendPlayedVideoList()
    {
        var played = await _sessionService.GetAllSessionVideos(DateTime.Now.AddHours(-6).Date);

        await Clients.Caller.SendAsync("UpdatePlayedVideos", played);
    }
    
    public ChannelReader<MicrophoneDto> SubscribeMicrophones(CancellationToken cancellationToken)
    {
        _logger.LogDebug("Microphone subscription starting");
        cancellationToken.Register(() => _logger.LogDebug("Microphone subscription stopping"));
        
        return _microphoneService.StreamMicrophones().ToNewestValueStream(cancellationToken);
    }
}