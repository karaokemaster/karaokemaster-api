// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Kiosk;
using KaraokeMaster.Services.Session;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using TweetNaclSharp;

namespace KaraokeMaster.Api.Hubs;

// Paste the following into jsFiddle to generate a new keypair if needed
// *** Add JS ref: https://unpkg.com/tweetnacl@1.0.3/nacl-fast.js
// const apiKey = nacl.box.keyPair();
//
// console.log("// For KaraokeMaster.Api:");
// console.log("private readonly byte[] _apiSecretKey = {" + apiKey.secretKey.join() + "};");
//
// console.log("// For Kiosk:");
// console.log("let api_public_key: [u8; 32] = [" + apiKey.publicKey.join() + "];");

public class KioskHub : Microsoft.AspNetCore.SignalR.Hub
{
    private readonly ILogger<KioskHub> _logger;
    private readonly IUserStateService _userStateService;
    private readonly ISessionService _sessionService;
    private readonly IKioskService _kioskService;

    private static readonly byte[] _apiSecretKey = {206,155,110,136,41,64,65,29,155,72,37,244,64,100,135,46,119,90,56,194,194,186,248,4,6,249,114,212,9,103,201,18};

    public KioskHub(ILoggerFactory loggerFactory, IUserStateService userStateService, ISessionService sessionService, IKioskService kioskService)
    {
        _logger = loggerFactory.CreateLogger<KioskHub>();
        _userStateService = userStateService;
        _sessionService = sessionService;
        _kioskService = kioskService;
    }

    public override async Task OnConnectedAsync()
    {
        _logger.LogInformation("Kiosk connected");

        await base.OnConnectedAsync();
    }

    public override async Task OnDisconnectedAsync(Exception exception)
    {
        _logger.LogInformation("Kiosk disconnected");

        await base.OnDisconnectedAsync(exception);
    }

    public async Task Connect(string name, string encryptedKey, string ephemeralKey)
    {
        _logger.LogInformation("Client connected {Name}", name);
        await Groups.AddToGroupAsync(Context.ConnectionId, "Kiosk");

        var encryptedKeyBytes = Convert.FromBase64String(encryptedKey).Skip(16).ToArray();
        var clientEphemeralKey = Convert.FromBase64String(ephemeralKey);

        var nonce = new byte[24];
        var clientKeyEncodedBytes = Nacl.BoxOpen(encryptedKeyBytes, nonce, clientEphemeralKey, _apiSecretKey);
        var clientKeyString = System.Text.Encoding.Default.GetString(clientKeyEncodedBytes);
        var clientKey = Convert.FromBase64String(clientKeyString);
        
        var publicKey = ToHex(clientKey);
        _logger.LogInformation("Key {Key}", publicKey);
        await Groups.AddToGroupAsync(Context.ConnectionId, publicKey);

        if (clientKey == null)
            throw new ArgumentException(null, nameof(publicKey));
        
        var kiosk = await _kioskService.EnsureKioskExists(name, clientKey);

        _logger.LogDebug($"Kiosk ID {kiosk.KioskId} connected");

        await Clients.Groups(publicKey).SendAsync("SetKioskId", kiosk.KioskId);
        
        
        foreach (var display in kiosk.Displays)
        {
            _logger.LogDebug($"Sending kiosk settings for {kiosk.Name} display {display.DisplayId}...");
            await SendDisplaySettings(Clients, kiosk, display);
        }
    }
    
    public async Task SendUserList()
    {
        var users = await _userStateService.GetAllUsers();

        await Clients.Caller.SendAsync("UpdateUsers", users);
    }

    public async Task SendPlayedVideoList()
    {
        var played = await _sessionService.GetAllSessionVideos(DateTime.Now.AddHours(-6).Date);

        await Clients.Caller.SendAsync("UpdatePlayedVideos", played);
    }

    public static async Task SendDisplaySettings(IHubClients<IClientProxy> clients, Kiosk kiosk, KioskDisplay display)
    {
        var publicKey = ToHex(kiosk.PublicKey);

        var settings = JsonSerializer.Serialize(display, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
        
        var encryptedSettings = Encrypt(kiosk.PublicKey, settings);

        await clients.Groups(publicKey).SendAsync("ConfigureDisplay", encryptedSettings);
    }
    
     public static string ToHex(byte[] source)
     {
         try 
         { 
             var hex = BitConverter.ToString(source).Replace("-", "");
             return hex;
         }
         catch
         {
             return "---";
         }
     }

     private static string Encrypt(byte[] clientKey, string data)
     {
         var combinedKey = Nacl.BoxBefore(clientKey, _apiSecretKey);

         var nonce = new byte[24];
         var dataBytes = Encoding.UTF8.GetBytes(data);
        
         var cipherBytes = Nacl.BoxAfter(dataBytes, nonce, combinedKey);

         var cipher = Convert.ToBase64String(cipherBytes);

         return cipher;
     }
}