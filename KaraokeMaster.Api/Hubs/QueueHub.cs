// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using KaraokeMaster.Services.Queue;
using KaraokeMaster.Services.Recommendations;
using KaraokeMaster.Services.Search;
using KaraokeMaster.Services.Session;
using Microsoft.AspNetCore.SignalR;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Api.Hubs
{
    // TODO: Rename to UserHub ?
    public class QueueHub : HubBase
    {
        private readonly IQueueService _queueService;
        private readonly IRecommendationService _recommendationService;
        private readonly ISearchService _searchService;
        private readonly ISessionService _sessionService;
        
        public QueueHub(IQueueService queueService, IRecommendationService recommendationService, ISearchService searchService, ISessionService sessionService)
        {
            _queueService = queueService;
            _recommendationService = recommendationService;
            _searchService = searchService;
            _sessionService = sessionService;
        }
        
        public override async Task OnConnectedAsync()
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("queueHub:OnConnectedAsync", "QueueHub.OnConnectedAsync");
                scope.Transaction = transaction;

                try
                {
                    if (Context.User?.Identity is User user)
                    {
                        await Groups.AddToGroupAsync(Context.ConnectionId, $"user_{user.UserId}");

                        var recs = await _recommendationService.GetRecommendations(user.Username);
                        await Clients.Caller.SendAsync("UpdateRecommendations", recs);

                        var previouslyPlayed = await _recommendationService.GetPreviouslyPlayed(user.Username);
                        await Clients.Caller.SendAsync("UpdatePreviouslyPlayed", previouslyPlayed);

                        var sessions = await _sessionService.GetSessions(user.Username);
                        await Clients.Caller.SendAsync("UpdateSessions", sessions);
                    }
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                }

                try
                {
                    await Clients.Caller.SendAsync("ServerInfo", ServerInfo);

                    var queue = await _queueService.Get();

                    await Clients.Caller.SendAsync("UpdateQueue", queue);
                    
                    transaction.Finish();
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }
                
        public async Task UpdateQueueState(int queueId, int state, int? startNextQueueId)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("queueHub:UpdateQueueState", "QueueHub.UpdateQueueState");
                transaction.SetExtra(nameof(queueId), queueId);
                transaction.SetExtra(nameof(state), state);

                scope.Transaction = transaction;

                try
                {
                    if (state == 1)
                        await _queueService.StartVideo(queueId);
                    else
                        await _queueService.EndVideo(queueId);

                    if (state < 1 && startNextQueueId.HasValue)
                        await _queueService.StartVideo(startNextQueueId.Value);

                    var queue = await _queueService.Get();

                    await Clients.All.SendAsync("UpdateQueue", queue);
                    
                    transaction.Finish();
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }

        public async Task SearchAutocomplete(string query)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("queueHub:SearchAutocomplete", "QueueHub.SearchAutocomplete");
                transaction.SetExtra(nameof(query), query);

                scope.Transaction = transaction;

                try
                {
                    var user = Context.User?.Identity as User;
                    
                    var results = await _searchService.Autocomplete(query, user?.Username);
                    
                    await Clients.Caller.SendAsync("UpdateAutocomplete", results);
                    
                    transaction.Finish();
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                }
            });
        }
    }
}