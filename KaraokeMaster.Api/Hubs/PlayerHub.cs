// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using KaraokeMaster.Services.Player;
using KaraokeMaster.Services.Reaction;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using RxSignalrStreams.Extensions;
using Sentry;

namespace KaraokeMaster.Api.Hubs
{
    public class PlayerHub : Microsoft.AspNetCore.SignalR.Hub
    {
        private readonly ILogger<PlayerHub> _logger;
        private readonly IPlayerStateService _playerStateService;
        private readonly IReactionService _reactionService;

        public PlayerHub(ILoggerFactory loggerFactory, IPlayerStateService playerStateService, IReactionService reactionService)
        {
            _logger = loggerFactory.CreateLogger<PlayerHub>();
            _playerStateService = playerStateService;
            _reactionService = reactionService;
        }

        public async Task PublishPlayerState(ChannelReader<PlayerState> stream)
        {
            var cts = new CancellationTokenSource();

            _logger.LogDebug("Player connected; publishing state...");
            
            Task.Run(async () =>
            {
                try
                {
                    while (await stream.WaitToReadAsync(cts.Token))
                    {
                        while (stream.TryRead(out var playerState))
                        {
                            _playerStateService.Update(playerState);
                        }
                    }
                }
                catch { }
            }, cts.Token);

            try
            {
                await stream.Completion;
            }
            catch (TaskCanceledException)
            { }
            catch (Exception e)
            {
                SentrySdk.CaptureException(e);
            }
            finally
            {
                _logger.LogDebug("Player disconnected");
                cts.Cancel();
            }
            
            _playerStateService.Update(PlayerState.Default);
        }

        public ChannelReader<PlayerState> SubscribePlayerState(CancellationToken cancellationToken)
        {
            _logger.LogDebug("Player state subscription starting");

            var stream = Observable.Create<PlayerState>(observer =>
            {
                var disposable = _playerStateService.Subscribe(observer);

                return Disposable.Create(() =>
                {
                    disposable.Dispose();
                    _logger.LogDebug("Player state subscription stopped");

                });
            }).Publish();

            stream.Connect();
            
            return stream.ToNewestValueStream(cancellationToken);
        }
        
        public async Task SendKeyDown(string code)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("playerHub:SendKeyDown", "PlayerHub.SendKeyDown");
                transaction.SetExtra(nameof(code), code);

                scope.Transaction = transaction;

                try
                {
                    await Clients.AllExcept(Context.ConnectionId).SendAsync("PlayerKeyDown", code);
                    
                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                    throw;
                }
            });
        }

        public async Task SendPlayerControl(string action)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("playerHub:SendPlayerControl", "PlayerHub.SendKeyDown");
                transaction.SetExtra(nameof(action), action);

                scope.Transaction = transaction;

                try
                {
                    await Clients.All.SendAsync("ControlPlayer", action);
                    
                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                    throw;
                }
            });
        }

        public async Task SendReaction(ReactionDto reaction)
        {
            await SentrySdk.ConfigureScopeAsync(async scope =>
            {
                var transaction = SentrySdk.StartTransaction("playerHub:SendReaction", "PlayerHub.SendReaction");
                transaction.SetExtra(nameof(reaction), reaction);

                scope.Transaction = transaction;

                try
                {
                    await _reactionService.Add(reaction);

                    transaction.Finish(SpanStatus.Ok);
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    transaction.Finish(SpanStatus.InternalError);
                    throw;
                }
            });
        }
        
        public ChannelReader<ReactionDto> SubscribeReactions(CancellationToken cancellationToken)
        {
            _logger.LogDebug("Reaction subscription starting");

            var stream = Observable.Create<ReactionDto>(observer =>
            {
                var disposable = _reactionService.Subscribe(observer);

                return Disposable.Create(() =>
                {
                    disposable.Dispose();
                    _logger.LogDebug("Reaction subscription stopped");

                });
            }).Publish();

            stream.Connect();
            
            return stream.ToBufferedStream(cancellationToken);
        }
    }
}