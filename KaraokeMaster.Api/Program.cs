// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Autofac.Extensions.DependencyInjection;
using KaraokeMaster.Api.Banner;
using Loki.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            StartupBanner.Print(out var sentryRelease);
            
            CreateHostBuilder(sentryRelease, args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string sentryRelease, string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseSentry(sentry =>
                        {
                            sentry.Release = sentryRelease;
                        })
                        .UseUrls("http://*:5000")
                        .ConfigureKestrel(serverOptions =>
                        {
                            serverOptions.ListenAnyIP(5000);
                        })
                        .ConfigureLogging((hostingContext, logging) =>
                        {
                            logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                            logging.AddConsole();
                            logging.AddDebug();
                            logging.AddLoki(options =>
                            {
                                options.ApplicationName = "KaraokeMaster.Api";
                                options.IncludeScopes = true;
                                options.IncludePredefinedFields = true;

                                options.AdditionalFields.Add("version", sentryRelease);
                            });
                        })
                        .UseStartup<Startup>();
                });
    }
}
