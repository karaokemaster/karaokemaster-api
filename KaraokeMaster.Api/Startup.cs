// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using KaraokeMaster.Api.Authentication;
using KaraokeMaster.Api.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sentry.AspNetCore;

namespace KaraokeMaster.Api
{
    public class Startup
    {
        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
                .AddJsonFile(new AzureModuleConfiguration(), "appsettings.iot.json", true, true)
                .AddEnvironmentVariables()
                ;
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public ILifetimeScope AutofacContainer { get; private set; }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<TokenConfiguration>(Configuration.GetSection(TokenConfiguration.ConfigurationKey));
            
            services.AddCors(options =>
            {
                options.AddPolicy("CorsAllowAny",
                    builder =>
                    {
                        var hosts = Configuration
                            .GetSection("CORS:AllowedHosts")
                            .GetChildren()
                            .Select(h => h.Value)
                            .ToArray();
                        
                        builder
                            .WithOrigins(hosts)
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials()
                            ;
                    });
            });

            services.AddHttpClient();
            
            services.AddMemoryCache(); // Add memory caching for blacklisted tokens
            
            services.AddLogging(builder =>
                builder
                    .AddDebug()
                    .AddConsole()
                    .AddConfiguration(Configuration.GetSection("Logging"))
                    .SetMinimumLevel(LogLevel.Trace)
            );

            services.AddControllers();
            services.AddHttpContextAccessor();
            
            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
                options.DisableImplicitFromServicesParameters = true;
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterInstance(Configuration);
            
            AutofacConfig.Build(builder);
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            // app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseSentryTracing();

            app.UseMiddleware<TokenMiddleware>();
            
            app.UseCors("CorsAllowAny");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<Hub>("hub");
                endpoints.MapHub<PlayerHub>("hubs/player");
                endpoints.MapHub<QueueHub>("hubs/queue");
                endpoints.MapHub<EmceeHub>("hubs/emcee");
                endpoints.MapHub<KioskHub>("hubs/kiosk");
            });
        }
    }
}
