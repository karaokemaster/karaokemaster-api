// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace KaraokeMaster.Api
{
    public class AzureModuleConfiguration : IFileProvider
    {
        private static readonly Action<object> _cancelTokenSource = state => ((CancellationTokenSource)state).Cancel();
        
        private readonly ModuleClient _moduleClient;
        private readonly ConcurrentBag<CancellationTokenSource> _cancellationTokenSources;
        private IFileInfo _fileInfo = new InMemoryFile("{}");
        
        public AzureModuleConfiguration()
        {
            _cancellationTokenSources = new ConcurrentBag<CancellationTokenSource>();
            
            try
            {
                // TODO: Refactor this into a service, assuming DI can do something to help
                var isIot = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("IOTEDGE_WORKLOADURI"));

                if (isIot)
                {
                    var moduleTask = ModuleClient.CreateFromEnvironmentAsync();
                    moduleTask.Wait();
                    _moduleClient = moduleTask.Result;
                }
                else
                {
                    var iotConnection = Environment.GetEnvironmentVariable("IOT_CONNECTION");
                    if (!string.IsNullOrWhiteSpace(iotConnection))
                    {
                        _moduleClient = ModuleClient.CreateFromConnectionString(iotConnection);
                    }
                    else
                    {
                        // TODO: Something ?
                        return;
                    }
                }

                _moduleClient.SetDesiredPropertyUpdateCallbackAsync(DesiredPropertyUpdateCallback, null);
                
                LoadConfiguration().Wait();
            }
            catch (Exception e)
            {
                // TODO: Use a logging thing
                Console.Error.WriteLine(e);
            }
        }

        public IFileInfo GetFileInfo(string _) => _fileInfo;
        
        public IDirectoryContents GetDirectoryContents(string _) => null;

        public IChangeToken Watch(string _)
        {
            var cts = new CancellationTokenSource();
            var token = new CancellationChangeToken(cts.Token);

            _cancellationTokenSources.Add(cts);

            return token;
        }

        private async Task DesiredPropertyUpdateCallback(TwinCollection desiredProperties, object userContext)
        {
            // TODO: Use a logging thing here...
            Console.WriteLine("Desired properties updated");
            
            await LoadConfiguration(desiredProperties);

            ResetChangeToken();
        }

        private async Task LoadConfiguration()
        {
            var twin = await _moduleClient.GetTwinAsync();
            
            await LoadConfiguration(twin.Properties.Desired);
        }

        private async Task LoadConfiguration(TwinCollection desiredProperties)
        {
            var json = desiredProperties.ToJson(Formatting.Indented);

            // TODO: Use a logging thing
            Console.WriteLine(json);

            _fileInfo = new InMemoryFile(json);

            // TODO: Report running configuration back to hub - though should probably happen elsewhere, once it's actually loaded
        }
        
        private void ResetChangeToken()
        {
            if (_cancellationTokenSources.TryTake(out var cts))
            {
                Task.Factory.StartNew(
                    _cancelTokenSource,
                    cts,
                    CancellationToken.None,
                    TaskCreationOptions.DenyChildAttach,
                    TaskScheduler.Default);    
            }
        }
        
        private class InMemoryFile : IFileInfo
        {
            private readonly byte[] _data;
            public InMemoryFile(string json) => _data = Encoding.UTF8.GetBytes(json);
            public Stream CreateReadStream() => new MemoryStream(_data);
            public bool Exists { get; } = true;
            public long Length => _data.Length;
            public string PhysicalPath { get; } = null;
            public string Name { get; } = string.Empty;
            public DateTimeOffset LastModified { get; } = DateTimeOffset.UtcNow;
            public bool IsDirectory { get; } = false;
        }
    }
}