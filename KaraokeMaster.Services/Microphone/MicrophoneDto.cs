// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

namespace KaraokeMaster.Services.Microphone;

public class MicrophoneDto
{
    public string Name { get; set; }
    public string Color { get; set; }
    public byte? Battery { get; set; }
    public byte? RfLevel { get; set; }
    public byte? RfAntenna { get; set; }
    public bool RfPilot { get; set; }
    public byte? AfLevel { get; set; }
    public byte? AfPeak { get; set; }
    public bool Mute { get; set; }
}