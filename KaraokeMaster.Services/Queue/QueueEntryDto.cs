// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;

namespace KaraokeMaster.Services.Queue
{
    public class QueueEntryDto
    {
        public int QueueId { get; set; }
        public string VideoId { get; set; }
        public string Title { get; set; }
        public int Duration { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Lyrics { get; set; }
        public int Order { get; set; }
        public string User { get; set; }
        public string UserIcon { get; set; }
        public string UserColor { get; set; }
        public string LightingCue { get; set; }
        public int Rating { get; set; }
        public DateTime? PlaybackDate { get; set; }   
    }
}