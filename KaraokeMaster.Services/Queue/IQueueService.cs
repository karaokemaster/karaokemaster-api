// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Threading.Tasks;
using KaraokeMaster.Services.Search;

namespace KaraokeMaster.Services.Queue
{
    public interface IQueueService
    {
        Task<List<QueueEntryDto>> Get();
        Task<List<QueueEntryDto>> GetRecent();
        Task SetLyrics(int queueId, string lyrics);
        Task SetLightingCue(int queueId, string cue);
        Task SetRating(int queueId, int rating);
        Task SetUser(int queueId, string username);
        Task Add(SearchResultDto entry, string user);
        Task Move(int queueId, int? afterId, int? beforeId);
        Task Remove(int queueId);
        Task StartVideo(int queueId);
        Task EndVideo(int queueId);
    }
}