// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;

namespace KaraokeMaster.Services.Search;

public interface ISearchProvider
{
    Task AddOrUpdate(Video video);
    Task AddOrUpdate(IEnumerable<Video> video);
    Task AddOrUpdate(QueueEntry entry);
    Task AddOrUpdate(IEnumerable<QueueEntry> entries);
    Task<List<Video>> Search(string query);
}