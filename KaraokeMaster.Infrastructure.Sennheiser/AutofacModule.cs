// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Autofac;
using KaraokeMaster.Application.Autofac;
using KaraokeMaster.Infrastructure.Sennheiser.Configuration;
using KaraokeMaster.Infrastructure.Sennheiser.Workers;
using KaraokeMaster.Services.Microphone;
using Microsoft.Extensions.Hosting;

namespace KaraokeMaster.Infrastructure.Sennheiser;

public class AutofacModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        // builder.RegisterType<EventListener>();
            
        builder.RegisterConfig<SennheiserConfiguration>(SennheiserConfiguration.ConfigurationKey);
        
        builder.RegisterType<SennheiserWorker>()
            .As<IHostedService>()
            .As<IMicrophoneService>()
            .SingleInstance();
            
        base.Load(builder);
    }
}