// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using KaraokeMaster.Infrastructure.Sennheiser.Configuration;
using KaraokeMaster.Services.Microphone;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace KaraokeMaster.Infrastructure.Sennheiser.Workers;

public class SennheiserWorker : IMicrophoneService, IHostedService, IDisposable
{
    private const int Port = 53212;
    private const int ResolutionMs = 100;
    private readonly TimeSpan ResolutionTimeSpan = TimeSpan.FromMilliseconds(ResolutionMs);

    private readonly ILogger _logger;
    private readonly IOptionsMonitor<SennheiserConfiguration> _configuration;

    private readonly Subject<MicrophoneDto> _subject;
    private readonly IObservable<MicrophoneDto> _stream;
    private readonly CancellationTokenSource _workerCancellationToken;

    private CancellationTokenSource? _pushCancellationToken;
    private IDisposable? _configListener;

    public SennheiserWorker(ILoggerFactory loggerFactory, IOptionsMonitor<SennheiserConfiguration> metricsConfiguration)
    {
        _logger = loggerFactory.CreateLogger<SennheiserWorker>();
        _configuration = metricsConfiguration;
        _workerCancellationToken = new CancellationTokenSource();
        
        _subject = new Subject<MicrophoneDto>();
        var c = _subject.Publish();
        c.Connect();

        _stream = Observable.Create<MicrophoneDto>(observer =>
        {
            var subscription = c.Subscribe(observer);

            return subscription;
        });
    }
    
    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Starting Sennheiser microphone service");

        StartListener();

        _configListener = _configuration.OnChange(OnConfigurationChange);
            
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Stopping Sennheiser microphone service");

        _configListener?.Dispose();
        _configListener = null;

        Stop();
        
        _workerCancellationToken.Cancel();
        
        return Task.CompletedTask;
    }

    public IObservable<MicrophoneDto> StreamMicrophones()
    {
        // TODO: Properly handle multiple connected subscribers
        var observable = Observable.Create<MicrophoneDto>(observer =>
        {
            _logger.LogDebug("Starting microphone update stream...");
            
            Start();

            var subscription = _stream
                .GroupBy(m => m.Name)
                .SelectMany(m => m.Sample(ResolutionTimeSpan / 10))
                .Subscribe(observer);
            
            return Disposable.Create(() =>
            {
                _logger.LogDebug("Stopping microphone update stream...");

                try
                {
                    subscription?.Dispose();
                    Stop();
                }
                catch { }
            });
        });

        return observable;
    }
    
    public void Dispose()
    {
        _configListener?.Dispose();
    }
    
    private void Start()
    {
        _pushCancellationToken = new CancellationTokenSource();

        foreach (var microphone in _configuration.CurrentValue.Microphones)
        {
            MonitorMicrophone(microphone.Address, _pushCancellationToken.Token);
        }
    }

    private void Stop()
    {
        _pushCancellationToken?.Cancel();
        _pushCancellationToken = null;
    }
    
    private void OnConfigurationChange(SennheiserConfiguration configuration)
    {
        // TODO: Don't restart if nothing changed            
        _logger.LogInformation("Configuration changed; restarting...");

        if (_pushCancellationToken != null)
        {
            Stop();
            Start();
        }
    }

    private void StartListener()
    {
        new Thread(async () =>
        {
            var listener = new UdpClient(new IPEndPoint(IPAddress.Any, Port));

            while (!_workerCancellationToken.IsCancellationRequested)
            {
                var result = await listener.ReceiveAsync(_workerCancellationToken.Token);
                var message = Encoding.UTF8.GetString(result.Buffer);

                var updates = message.Split('\r');

                var microphone = _configuration.CurrentValue.Microphones.FirstOrDefault(m => m.Address == result.RemoteEndPoint.Address.ToString());
                if (microphone == null)
                {
                    _logger.LogWarning($"Received update from unknown microphone {result.RemoteEndPoint.Address}");
                    continue;
                }

                var bat = GetAttribute(updates, "Bat");
                
                byte? battery = bat == null || bat == "?" ? null : byte.Parse(bat);
                byte? rfLevel = GetAttributePart<byte?>(updates, "RF", 0);
                byte? rfAntenna = GetAttributePart<byte?>(updates, "RF", 1);
                byte? rfPilot = GetAttributePart<byte?>(updates, "RF", 2);
                byte? afLevel = GetAttributePart<byte?>(updates, "AF", 0);
                byte? afPeak = GetAttributePart<byte?>(updates, "AF", 1);
                byte? afMute = GetAttributePart<byte?>(updates, "AF", 2);

                var mic = new MicrophoneDto
                {
                    Name = microphone.Name,
                    Color = microphone.Color,
                    Battery = battery,
                    RfLevel = rfLevel,
                    RfAntenna = rfAntenna,
                    RfPilot = rfPilot > 0,
                    AfLevel = afLevel,
                    AfPeak = afPeak,
                    Mute = (afMute ?? 1) > 0
                };
                
                _subject.OnNext(mic);
            }
        }).Start();
    }

    private string? GetAttribute(string[] updates, string prefix)
    {
        var update = updates.FirstOrDefault(u => u.StartsWith(prefix + " "));

        if (update == null) return null;

        return update.Replace(prefix, "").Trim();
    }
    
    private T? GetAttributePart<T>(string[] updates, string prefix, int index, string separator = " ")
    {
        var update = updates.FirstOrDefault(u => u.StartsWith(prefix + " "));

        if (update == null) return default;

        var parts = update.Replace(prefix, "").Trim().Split(separator);

        if (parts.Length < index + 1) return default;
        
        try
        {
            var converter = TypeDescriptor.GetConverter(typeof(T?));
            // Cast ConvertFromString(string text) : object to (T)
            return (T?)converter.ConvertFromString(parts[index]);
        }
        catch (NotSupportedException)
        {
            return default;
        }
    }
    
    private void MonitorMicrophone(string address, CancellationToken cancellationToken)
    {
        _logger.LogInformation($"Starting monitoring of {address}");
        
        var endpoint = new IPEndPoint(IPAddress.Parse(address), Port);
        var client = new UdpClient();

        client.Connect(endpoint);

        Task.Run(async () =>
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var request = $"Push 10 {ResolutionMs} 4\r";
                var requestBytes = Encoding.UTF8.GetBytes(request);

                await client.SendAsync(requestBytes, requestBytes.Length);

                await Task.Delay(10000, cancellationToken);
            }
            
            _logger.LogInformation($"Stopped monitoring of {address}");
        }, cancellationToken);
    }
}