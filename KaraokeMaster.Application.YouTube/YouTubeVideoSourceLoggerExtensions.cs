// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Application.YouTube
{
    internal static class YouTubeVideoSourceLoggerExtensions
    {
        private static readonly Func<ILogger, string, IDisposable> _searchScope = LoggerMessage.DefineScope<string>("Search: [{Query}]");
        private static readonly Action<ILogger, string, Exception> _searchCacheBeginLookup =
            LoggerMessage.Define<string>(
                LogLevel.Trace, 
                new EventId(AutofacModule.LogEventIdBase + 1, "Search.Cache.Begin"), 
                "Checking cache for search query {Query}"
            );
        private static readonly Action<ILogger, string, Exception> _searchCacheFound =
            LoggerMessage.Define<string>(
                LogLevel.Information, 
                new EventId(AutofacModule.LogEventIdBase + 2, "Search.Cache.Found"), 
                "Using cached results for query {Query}"
            );
        private static readonly Action<ILogger, string, Exception> _searchCacheSave =
            LoggerMessage.Define<string>(
                LogLevel.Trace, 
                new EventId(AutofacModule.LogEventIdBase + 3, "Search.Cache.Save"), 
                "Saving search results with cache key {Key}"
            );
        private static readonly Action<ILogger, Exception> _searchCacheFailure =
            LoggerMessage.Define(
                LogLevel.Warning, 
                new EventId(AutofacModule.LogEventIdBase + 97, "Search.Cache.Failure"), 
                "Failure reading cache"
            );
        private static readonly Action<ILogger, string, Exception> _searchCacheSaveFailure =
            LoggerMessage.Define<string>(
                LogLevel.Warning, 
                new EventId(AutofacModule.LogEventIdBase + 98, "Search.Cache.Save.Failure"), 
                "Failed to save cached search results for query {Query}"
            );
        private static readonly Action<ILogger, Exception> _searchError =
            LoggerMessage.Define(
                LogLevel.Error, 
                new EventId(AutofacModule.LogEventIdBase + 99, "Search.Error"), 
                "YouTube search error"
            );
        private static readonly Action<ILogger, string, Exception> _searchUri =
            LoggerMessage.Define<string>(
                LogLevel.Trace, 
                new EventId(AutofacModule.LogEventIdBase + 3, "Search.Uri"), 
                "Search - searchUri: {SearchUri}"
            );
        
        public static IDisposable SearchScope(this ILogger<YouTubeVideoSource> logger, string query) => _searchScope(logger, query);
        public static void SearchCacheBeginLookup(this ILogger<YouTubeVideoSource> logger, string query) => _searchCacheBeginLookup(logger, query, null);
        public static void SearchCacheFound(this ILogger<YouTubeVideoSource> logger, string query) => _searchCacheFound(logger, query, null);
        public static void SearchCacheSave(this ILogger<YouTubeVideoSource> logger, string key) => _searchCacheSave(logger, key, null);
        public static void SearchCacheFailure(this ILogger<YouTubeVideoSource> logger, Exception ex) => _searchCacheFailure(logger, ex);
        public static void SearchCacheSaveFailure(this ILogger<YouTubeVideoSource> logger, string query, Exception ex) => _searchCacheSaveFailure(logger, query, ex);
        public static void SearchError(this ILogger<YouTubeVideoSource> logger, Exception ex) => _searchError(logger, ex);
        public static void SearchUri(this ILogger<YouTubeVideoSource> logger, string uri) => _searchUri(logger, uri, null);
    }
}