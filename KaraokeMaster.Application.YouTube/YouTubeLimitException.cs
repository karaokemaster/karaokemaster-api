// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;
using KaraokeMaster.Application.YouTube.Models;
using KaraokeMaster.Domain.Models;

namespace KaraokeMaster.Application.YouTube
{
    [Serializable]
    public class YouTubeLimitException : VideoSourceLimitException
    {
        private readonly string _requestUri;
        private readonly string _kind;
        private readonly YouTubeError _youTubeError;

        public YouTubeLimitException()
        { }

        public YouTubeLimitException(string kind, YouTubeError youTubeError, string requestUri = null)
            : base(GetMessage(kind, youTubeError, requestUri))
        {
            _kind = kind;
            _youTubeError = youTubeError;
            _requestUri = requestUri;
        }

        protected YouTubeLimitException(SerializationInfo info, StreamingContext ctxt) : base(info, ctxt)
        {
            _kind = info.GetString("Kind");
            _youTubeError = info.GetValue("YouTubeError", typeof(YouTubeError)) as YouTubeError;
            _requestUri = info.GetString("RequestUri");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Kind", _kind);
            info.AddValue("YouTubeError", _youTubeError);
            info.AddValue("RequestUri", _requestUri);
            
            base.GetObjectData(info, context);
        }

        private static string GetMessage(string kind, YouTubeError youTubeError, string requestUri)
        {
            var error = $"Quota exception: {kind}, Error code {youTubeError.Code} ({youTubeError.Status}): {youTubeError.Message}";
            if (!string.IsNullOrEmpty(requestUri))
                error += $"\n\tRequest URI: {requestUri}";

            return error;
        }
    }
}