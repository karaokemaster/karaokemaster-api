// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using KaraokeMaster.Application.YouTube.Models;
using KaraokeMaster.Domain.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Sentry;

namespace KaraokeMaster.Application.YouTube
{
    public class YouTubeVideoSource : IVideoSource
    {
        private readonly ILogger<YouTubeVideoSource> _logger;
        private readonly IOptionsMonitor<YouTubeConfiguration> _youTubeConfiguration;
        private readonly IMemoryCache _memoryCache;
        private readonly IDistributedCache _distributedCache;
        private readonly MemoryCacheEntryOptions _cacheOptions;

        
        public YouTubeVideoSource(ILoggerFactory loggerFactory, IOptionsMonitor<YouTubeConfiguration> youTubeConfiguration, IMemoryCache memoryCache, IDistributedCache distributedCache)
        {
            _logger = loggerFactory.CreateLogger<YouTubeVideoSource>();
            _youTubeConfiguration = youTubeConfiguration;
            _memoryCache = memoryCache;
            _distributedCache = distributedCache;

            _cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10));
        }

        public async Task<IEnumerable<Video>> Search(string query)
        {
            return await Search(query, _youTubeConfiguration.CurrentValue.MaxResults, VideoDuration.Any);
        }

        public async Task<IEnumerable<Video>> Search(string query, int maxResults)
        {
            return await Search(query, maxResults, VideoDuration.Any);
        }

        private async Task<IEnumerable<Video>> Search(string query, int maxResults, VideoDuration duration)
        {
            var span = SentrySdk.GetSpan()?.StartChild("YouTubeVideoSource.Search");
            span?.SetExtra(nameof(query), query);

            var key = $"Search: {query}";

            using (_logger.SearchScope(query))
            {
                try
                {
                    _logger.SearchCacheBeginLookup(query);

                    var cacheData = await _distributedCache.GetAsync(key);

                    if (cacheData != null)
                    {
                        var cacheString = Encoding.UTF8.GetString(cacheData);
                        var cache = JsonConvert.DeserializeObject<List<Video>>(cacheString);

                        _logger.SearchCacheFound(query);
                        
                        span?.SetExtra("cached", true);
                        span?.Finish();
                        
                        return cache;
                    }
                }
                catch (Exception ex)
                {
                    _logger.SearchCacheFailure(ex);
                }

                try
                {
                    var config = _youTubeConfiguration.CurrentValue;

                    var encodedQuery = Uri.EscapeDataString($"{query} +(karaoke | lyrics)");
                    var apiKey = config.ApiKey;
                    var regionCode = config.RegionCode;

                    var builder = new UriBuilder($"https://www.googleapis.com/youtube/v3/search?key={apiKey}");
                    builder.Query += "&part=snippet";
                    builder.Query += $"&q={encodedQuery}";
                    builder.Query += $"&maxResults={maxResults}";
                    builder.Query += "&type=video";
                    builder.Query += "&videoEmbeddable=true";
                    builder.Query += $"&regionCode={regionCode}";
                    builder.Query += "&order=relevance";
                    if (duration != VideoDuration.Any)
                        builder.Query += $"&videoDuration={duration.ToParameter()}";

                    var searchUri = builder.ToString();
                    _logger.SearchUri(searchUri);

                    var results = await GetYouTubeAsync<SearchResult>(searchUri);

                    // TODO: Should include thumbnail dimensions as well
                    var searchResults = results
                        .Items
                        .Select(
                            r => new Video
                            {
                                Title = r.Snippet.Title,
                                YouTubeVideoId = r.Id.VideoId,
                                ThumbnailUrl = r.Snippet.Thumbnails.Medium.Url,
                            })
                        .ToList();

                    try
                    {
                        _logger.SearchCacheSave(key);

                        var dataString = JsonConvert.SerializeObject(searchResults);
                        var data = Encoding.UTF8.GetBytes(dataString);

                        await _distributedCache.SetAsync(key, data,
                            new DistributedCacheEntryOptions {SlidingExpiration = TimeSpan.FromHours(12)});
                    }
                    catch (Exception ex)
                    {
                        _logger.SearchCacheSaveFailure(query, ex);
                    }

                    span?.Finish();
                    
                    return searchResults;
                }
                catch (Exception ex)
                {
                    _logger.SearchError(ex);
                    span?.Finish(SpanStatus.InternalError);
                    
                    return Array.Empty<Video>();
                }
            }
        }

        public async Task<IEnumerable<Video>> GetRelated(string videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("YouTubeVideoSource.GetRelated");
            span?.SetExtra(nameof(videoId), videoId);

            var key = $"Recommendations: {videoId}";

            using (_logger.BeginScope("GetRelated - VideoId: {VideoId}", videoId))
            {
                try
                {
                    _logger.LogTrace("Checking cache for recommendations for {VideoId}", videoId);

                    var cacheData = await _distributedCache.GetAsync(key);

                    if (cacheData != null)
                    {
                        var cacheString = Encoding.UTF8.GetString(cacheData);
                        var cache = JsonConvert.DeserializeObject<List<Video>>(cacheString);

                        _logger.LogTrace("Using cached recommendations for video {VideoId}", videoId);
                        
                        span?.SetExtra("cached", true);
                        span?.Finish();
                        
                        return cache;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, "Failure reading cache");
                }

                try
                {
                    var config = _youTubeConfiguration.CurrentValue;

                    var apiKey = config.ApiKey;
                    var maxRelated = config.MaxRelated;
                    var regionCode = config.RegionCode;

                    var builder = new UriBuilder($"https://www.googleapis.com/youtube/v3/search?key={apiKey}");
                    builder.Query += "&part=snippet";
                    builder.Query += $"&relatedToVideoId={videoId}";
                    builder.Query += $"&maxResults={maxRelated}";
                    builder.Query += "&type=video";
                    builder.Query += "&videoEmbeddable=true";
                    builder.Query += $"&regionCode={regionCode}";

                    var searchUri = builder.ToString();
                    _logger.LogTrace("GetRelated - searchUri: {SearchUri}", searchUri);

                    var results = await GetYouTubeAsync<SearchResult>(searchUri);

                    // TODO: Should include thumbnail dimensions as well
                    var recs = results
                        .Items
                        .Where(i => !string.IsNullOrEmpty(i?.Snippet?.Title) &&
                                    !string.IsNullOrEmpty(i?.Snippet?.Description))
                        .Where(i => i.Snippet.Title.IndexOf("lyrics", StringComparison.InvariantCultureIgnoreCase) > -1
                                    || i.Snippet.Title.IndexOf("karaoke", StringComparison.InvariantCultureIgnoreCase) >
                                    -1
                                    || i.Snippet.Description.IndexOf("lyrics",
                                        StringComparison.InvariantCultureIgnoreCase) > -1
                                    || i.Snippet.Description.IndexOf("karaoke",
                                        StringComparison.InvariantCultureIgnoreCase) > -1
                        )
                        .Select(
                            r => new Video
                            {
                                Title = r.Snippet.Title,
                                YouTubeVideoId = r.Id.VideoId,
                                ThumbnailUrl = r.Snippet.Thumbnails.Medium.Url,
                            })
                        .ToList();

                    try
                    {
                        _logger.LogTrace("Saving search recommendations with cache key {Key}", key);

                        var dataString = JsonConvert.SerializeObject(recs);
                        var data = Encoding.UTF8.GetBytes(dataString);

                        await _distributedCache.SetAsync(key, data);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning(ex, "Failed to save cached recommendations for video {VideoId}", videoId);
                    }

                    span?.Finish();
                    
                    return recs;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "YouTube search error");
                    span?.Finish(SpanStatus.InternalError);
                    
                    return Array.Empty<Video>();
                }
            }
        }

        public async Task<IEnumerable<Video>> GetTopVideos()
        {
            var span = SentrySdk.GetSpan()?.StartChild("YouTubeVideoSource.GetTopVideos");

            var key = $"TopVideos";

            using (_logger.BeginScope("GetTopVideos"))
            {
                try
                {
                    _logger.LogTrace("Checking cache for top videos");

                    var cacheData = await _distributedCache.GetAsync(key);

                    if (cacheData != null)
                    {
                        var cacheString = Encoding.UTF8.GetString(cacheData);
                        var cache = JsonConvert.DeserializeObject<List<Video>>(cacheString);

                        _logger.LogInformation("Using cached results for top videos");
                        
                        span?.SetExtra("cached", true);
                        span?.Finish();
                        
                        return cache;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, "Failure reading cache");
                }

                try
                {
                    var config = _youTubeConfiguration.CurrentValue;
                    var maxResults = config.MaxResults * 3 / config.TopVideosKeywords.Length;
                    
                    var searches = config
                        .TopVideosKeywords
                        .Select(async q => await Search(q, maxResults, VideoDuration.Short));
                    
                    var searchResults = (await Task.WhenAll(searches))
                        .SelectMany(s => s)
                        .ToList();
                    
                    try
                    {
                        _logger.LogTrace("Saving top videos with cache key {Key}", key);

                        var dataString = JsonConvert.SerializeObject(searchResults);
                        var data = Encoding.UTF8.GetBytes(dataString);

                        await _distributedCache.SetAsync(key, data,
                            new DistributedCacheEntryOptions {SlidingExpiration = TimeSpan.FromHours(12)});
                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning(ex, "Failed to save cached search results for top videos");
                    }

                    span?.Finish();
                    
                    return searchResults;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "YouTube search error");
                    span?.Finish(SpanStatus.InternalError);
                    
                    return Array.Empty<Video>();
                }
            }
        }

        public async Task<Video> GetVideoDetails(string videoId)
        {
            return (await GetVideoDetails(new[] {videoId})).FirstOrDefault();
        }
        
        public async Task<IEnumerable<Video>> GetVideoDetails(IEnumerable<string> videoIds)
        {
            var span = SentrySdk.GetSpan()?.StartChild("YouTubeVideoSource.GetVideoDetails");
            
            var ids = videoIds.ToList();

            using (_logger.BeginScope("GetVideoDetails: [{VideoId}]", string.Join(",", ids)))
            {
                try
                {
                    var config = _youTubeConfiguration.CurrentValue;

                    var apiKey = config.ApiKey;

                    var builder = new UriBuilder($"https://www.googleapis.com/youtube/v3/videos?key={apiKey}");
                    builder.Query += "&part=snippet,contentDetails";
                    builder.Query += $"&id={string.Join(",", ids)}";

                    var requestUri = builder.ToString();

                    _logger.LogTrace("GetVideoDetails - requestUri: {Uri}", requestUri);

                    var results = await GetYouTubeAsync<VideoResult>(requestUri);

                    // TODO: Handle unavailable videos
                    var videoResults = results
                        .Items
                        // TODO: Consolidate this logic w/ related, etc.
                        .Select(
                            r => new Video
                            {
                                Title = r.Snippet.Title,
                                YouTubeVideoId = r.Id,
                                ThumbnailUrl = r.Snippet.Thumbnails.Medium.Url,
                                Duration = (int) XmlConvert.ToTimeSpan(r.ContentDetails.Duration).TotalSeconds,
                                Unavailable = false
                            })
                        .ToList();

                    span?.Finish();
                    
                    return videoResults;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "YouTube video details error");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        private async Task<T> GetYouTubeAsync<T>(string requestUri)
            where T : IYouTubeResult
        {
            var client = new HttpClient();
            var response = await client.GetAsync(requestUri);

            if (!response.IsSuccessStatusCode)
            {
                var error = await response.Content.ReadAsAsync<ErrorResult>();
                var kind = error.Error.Errors.FirstOrDefault()?.Reason;
                
                if (kind != null && kind.Contains("limit", StringComparison.InvariantCultureIgnoreCase))
                    throw new YouTubeLimitException(kind, error.Error, requestUri);
                
                throw new YouTubeException(error.Error, requestUri);
            }

            var results = await response.Content.ReadAsAsync<T>();
            if (results.Error != null)
                throw new YouTubeException(results.Error, requestUri);

            return results;
        }
    }
}