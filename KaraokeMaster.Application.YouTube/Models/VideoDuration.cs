// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

namespace KaraokeMaster.Application.YouTube.Models
{
    public enum VideoDuration
    {
        [Display(Name = "any")]
        Any,
        [Display(Name = "short")]
        Short,
        [Display(Name = "medium")]
        Medium,
        [Display(Name = "long")]
        Long
    }

    public static class VideoDurationExtensions
    {
        public static string ToParameter(this VideoDuration duration)
        {
            var fieldInfo = typeof(VideoDuration).GetField(duration.ToString());

            var descriptionAttributes = fieldInfo?.GetCustomAttributes(typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null) 
                return string.Empty;
            
            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : duration.ToString();
        }
    }
}