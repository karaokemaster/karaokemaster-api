// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

namespace KaraokeMaster.Domain.Models.Genius
{
    public class SearchResultHit
    {
        public SearchResultItem Result { get; set; }
    }

    public class SearchResultItem
    {
        public string Full_Title { get; set; }
        public long Id { get; set; }
        public string Api_Path { get; set; }
    }
}

// {
// annotation_count: 19,
// api_path: "/songs/3039923",
// full_title: "HUMBLE. by Kendrick Lamar",
// header_image_thumbnail_url: "https://images.genius.com/0780c76f8d3ab762a0ad67ac26fa9709.300x169x1.jpg",
// header_image_url: "https://images.genius.com/0780c76f8d3ab762a0ad67ac26fa9709.1000x563x1.jpg",
// id: 3039923,
// lyrics_owner_id: 104344,
// lyrics_state: "complete",
// path: "/Kendrick-lamar-humble-lyrics",
// pyongs_count: 1015,
// song_art_image_thumbnail_url: "https://images.genius.com/4387b0bcc88e07676997ba73793cc73c.300x300x1.jpg",
// song_art_image_url: "https://images.genius.com/4387b0bcc88e07676997ba73793cc73c.1000x1000x1.jpg",
// -stats: {
// unreviewed_annotations: 0,
// concurrents: 2,
// hot: false,
// pageviews: 10087014
// },
// title: "HUMBLE.",
// title_with_featured: "HUMBLE.",
// url: "https://genius.com/Kendrick-lamar-humble-lyrics",
// -primary_artist: {
// api_path: "/artists/1421",
// header_image_url: "https://images.genius.com/f3a1149475f2406582e3531041680a3c.1000x800x1.jpg",
// id: 1421,
// image_url: "https://images.genius.com/25d8a9c93ab97e9e6d5d1d9d36e64a53.1000x1000x1.jpg",
// is_meme_verified: true,
// is_verified: true,
// name: "Kendrick Lamar",
// url: "https://genius.com/artists/Kendrick-lamar",
// iq: 38414
// }
// }