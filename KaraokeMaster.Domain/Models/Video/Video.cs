// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KaraokeMaster.Domain.Models
{
    public class Video
    {
        public Video() {}

        public Video(Video source)
        {
            VideoId = source.VideoId;
            Title = source.Title;
            YouTubeVideoId = source.YouTubeVideoId;
            Duration = source.Duration;
            ThumbnailUrl = source.ThumbnailUrl;
            Unavailable = source.Unavailable;
            LastUpdate = source.LastUpdate;
            Options = source.Options;
            PlayCount = source.PlayCount;
            LastPlayed = source.LastPlayed;
        }
        
        [Key]
        public int VideoId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string YouTubeVideoId { get; set; }
        [Required]
        public int Duration { get; set; }
        [Required]
        public string ThumbnailUrl { get; set; }
        [Required]
        public bool Unavailable { get; set; }
        [Required]
        public DateTime LastUpdate { get; set; }

        public VideoOptions Options { get; set; }

        [NotMapped]
        public int PlayCount { get; set; }
        [NotMapped]
        public DateTime? LastPlayed { get; set; }
    }
}