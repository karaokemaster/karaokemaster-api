// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KaraokeMaster.Domain.Models
{
    public interface IVideoRepository
    {
        Task<Video> AddOrUpdate(Video video);
        Task<Video> Get(int videoId);
        Task<Video> Get(string youTubeVideoId);
        Task<List<Video>> Get(IEnumerable<string> videoIds);
        Task<List<Video>> GetAll();
        Task<List<Video>> Find(string query);
        Task<List<Video>> GetStale(DateTime updatedBefore, int maxVideos);
        Task<List<Video>> GetRecommendationsForUser(string name);
        Task<List<Video>> GetPreviouslyPlayedForUser(string name);
        Task<Video> GetVideoDetails(string videoId);
    }
}