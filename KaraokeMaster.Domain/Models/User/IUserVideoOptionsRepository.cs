// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Threading.Tasks;

namespace KaraokeMaster.Domain.Models
{
    public interface IUserVideoOptionsRepository
    {
        Task AddOrUpdate(UserVideoOptions options);
        Task<UserVideoOptions> Get(int userId, int videoId);
        Task<List<UserVideoOptions>> Get(int userId, IEnumerable<int> videoIds);
        Task<List<UserVideoOptions>> GetAll(int userId);
    }
}