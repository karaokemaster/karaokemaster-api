// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Domain.Models.Configuration;
using KaraokeMaster.Services.Recommendations;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sentry;

namespace KaraokeMaster.Application
{
    public class RecommendationService : IRecommendationService
    {
        private readonly ILogger<RecommendationService> _logger;
        private readonly IOptionsMonitor<RecommendationConfiguration> _recommendationConfiguration;
        private readonly IMemoryCache _memoryCache;
        private readonly ISearchService _searchService;
        private readonly IUserRepository _userRepository;
        private readonly IVideoRepository _videoRepository;
        private readonly IVideoRecommendationRepository _videoRecommendationRepository;
        private readonly IVideoOptionsRepository _videoOptionsRepository;
        private readonly IUserVideoOptionsRepository _userVideoOptionsRepository;
        private readonly IQueueRepository _queueRepository;
        private readonly IEnumerable<IVideoSource> _videoSources;
        private readonly MemoryCacheEntryOptions _cacheOptions;
        private readonly TimeSpan _tenMinutes = TimeSpan.FromMinutes(10);

        public RecommendationService(
            ILoggerFactory loggerFactory, 
            IOptionsMonitor<RecommendationConfiguration> recommendationConfiguration,
            IMemoryCache memoryCache, 
            ISearchService searchService, 
            IUserRepository userRepository,
            IVideoRepository videoRepository, 
            IVideoRecommendationRepository videoRecommendationRepository,
            IVideoOptionsRepository videoOptionsRepository,
            IUserVideoOptionsRepository userVideoOptionsRepository,
            IQueueRepository queueRepository,
            IEnumerable<IVideoSource> videoSources
            )
        {
            _logger = loggerFactory.CreateLogger<RecommendationService>();
            _recommendationConfiguration = recommendationConfiguration;
            _memoryCache = memoryCache;
            _searchService = searchService;
            _userRepository = userRepository;
            _videoRepository = videoRepository;
            _videoRecommendationRepository = videoRecommendationRepository;
            _videoOptionsRepository = videoOptionsRepository;
            _userVideoOptionsRepository = userVideoOptionsRepository;
            _queueRepository = queueRepository;
            _videoSources = videoSources;

            _cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10));
        }

        public async Task<IEnumerable<SearchResultDto>> GetRecommendations(string name, bool useCache = true)
        {
            var span = SentrySdk.GetSpan()?.StartChild("RecommendationService.GetRecommendations");
            span?.SetExtra(nameof(name), name);
            span?.SetExtra(nameof(useCache), useCache);

            // TODO: Further refactor this into additional methods
            using (_logger.BeginScope("GetRecommendations for [{Name}], UseCache: {UseCache}", name, useCache))
            {
                var user = await _userRepository.Get(name);
                var cachedRecommendations = new List<SearchResultDto>();
                var userCacheKey = $"recommendations_user_{name}";
                if (useCache && _memoryCache.TryGetValue(userCacheKey, out List<SearchResultDto> cachedRecs))
                {
                    _logger.LogDebug("Loaded {Count} recommendations from cache", cachedRecs.Count);
                    
                    cachedRecommendations = cachedRecs;
                }

                // TODO: Also grab recent queue entries to power YT recs
                var recommendations = (await _videoRepository.GetRecommendationsForUser(name))
                    .Where(v => v.Duration < _tenMinutes.TotalSeconds)
                    // TODO: Consolidate this logic w/ related
                    .Select(v => new SearchResultDto
                    {
                        Title = v.Title,
                        VideoId = v.YouTubeVideoId,
                        ThumbnailUrl = v.ThumbnailUrl,
                        HasLyrics = !string.IsNullOrWhiteSpace(v.Options?.Lyrics),
                        PlayCount = v.PlayCount,
                        LastPlayed = v.LastPlayed
                    })
                    .ToList();
                
                _logger.LogDebug("Loaded {Count} recommendations from database", recommendations.Count);

                var recent = (await _queueRepository.GetAll(true))
                    // TODO: Would be better to use playback bucket somehow...
                    .Where(q => q.Order >= 0 || q.PlaybackDate > DateTime.Now.AddHours(-12))
                    .ToList();

                var queue = (await _queueRepository.GetAll()).ToList();
                var userRecent = recent
                    .Union(queue)
                    // TODO: Maybe look up user by ID first?
                    .Where(r => r.User.Username == name)
                    .Select(q => new SearchResultDto
                    {
                        Title = q.Video.Title,
                        VideoId = q.Video.YouTubeVideoId,
                        ThumbnailUrl = q.Video.ThumbnailUrl,
                        HasLyrics = !string.IsNullOrWhiteSpace(q.Video?.Options?.Lyrics)
                    })
                    .ToList();

                _logger.LogDebug("Using {Count} recent videos for user", userRecent.Count);

                // TODO: It might be smarter to handle recommendation sources separately (favorites, recent plays, related), and re-merge them according to some precedence later on
                // TODO: Refactor to support multiple video sources
                // var youTubeCount = _recommendationConfiguration.CurrentValue.YouTubeCount;
                // var recTasks = recommendations
                //     .Take(youTubeCount)
                //     .Union(userRecent.Take(youTubeCount))
                //     .Select(async v => await _searchService.GetRelated(v.VideoId))
                //     .ToList();
                //
                // var ytRecs = (await Task.WhenAll(recTasks)).SelectMany(v => v)
                //     .GroupBy(r => r.VideoId)
                //     .Select(g => new {g.Key, Count = g.Count(), Video = g.First()})
                //     .OrderByDescending(g => g.Count)
                //     .Take(youTubeCount * 3)
                //     .Select(g => g.Video)
                //     .Shuffle()
                //     .Take(youTubeCount * 2)
                //     .ToList();
                //
                // _logger.LogDebug("Retrieved {Count} unique recommendations from YouTube", ytRecs.Count);

                recommendations = recommendations
                    // .Union(ytRecs)
                    // Next line is the same effect as Distinct(v => v.VideoId)
                    .GroupBy(v => v.VideoId).Select(v => v.FirstOrDefault())
                    .Where(v => v != null && !userRecent.Any(r => r.VideoId == v.VideoId))
                    .ToList();

                // TODO: Centralize this logic, since we're using it elsewhere (i.e. SearchService)
                var videos = await _videoRepository
                    .Get(recommendations.Select(v => v.VideoId));
                
                var videoOptions = await _videoOptionsRepository.Get(
                    videos.Select(v => v.VideoId).ToList()
                );
                
                var userOptions = await _userVideoOptionsRepository.Get(
                    user.UserId,
                    videos.Select(v => v.VideoId).ToList()
                );

                recommendations = recommendations
                    .GroupJoin(
                        videos,
                        r => r.VideoId,
                        v => v.YouTubeVideoId,
                        (r, v) => new { Recommendation = r, Video = v }
                    )
                    .SelectMany(
                        x => x.Video.DefaultIfEmpty(),
                        (x, v) => new { x.Recommendation, Video = v }
                    )
                    .GroupJoin(
                        videoOptions,
                        r => r.Video?.VideoId,
                        o => o.VideoId,
                        (x, o) => new { x.Recommendation, x.Video, Options = o }
                    )
                    .SelectMany(
                        x => x.Options.DefaultIfEmpty(),
                        (x, o) => new { x.Recommendation, x.Video, Options = o }
                    )
                    .GroupJoin(
                        userOptions,
                        r => r.Video?.VideoId,
                        o => o.VideoId,
                        (x, o) => new { x.Recommendation, x.Video, x.Options, UserOptions = o }
                    )
                    .SelectMany(
                        x => x.UserOptions.DefaultIfEmpty(),
                        (x, o) => new { x.Recommendation, x.Video, x.Options, UserOptions = o }
                    )
                    .Select(
                        x => new SearchResultDto
                        {
                            Title = x.Recommendation.Title,
                            VideoId = x.Recommendation.VideoId,
                            ThumbnailUrl = x.Recommendation.ThumbnailUrl,
                            HasLyrics = !string.IsNullOrWhiteSpace(x.Options?.Lyrics),
                            Rating = (x.UserOptions?.Good == true ? 1 : 0) + (x.UserOptions?.Bad == true ? -1 : 0),
                            PlayCount = x.Recommendation.PlayCount,
                            LastPlayed = x.Recommendation.LastPlayed,
                            // TODO: Duration
                        })
                    .Where(v => v.Rating >= 0)
                    .ToList();
                
                var newRecommendations = recommendations
                    .Where(v => !cachedRecommendations.Any(c => c.VideoId == v.VideoId))
                    .Shuffle()
                    .ToList();

                _logger.LogDebug("Found {NewCount} new recommendations out of {TotalCount} total", newRecommendations.Count, recommendations.Count);

                var maxCount = _recommendationConfiguration.CurrentValue.MaxCount;
                var output = cachedRecommendations
                    .Where(v => !queue.Any(q => q.Video.YouTubeVideoId == v.VideoId))
                    .Union(newRecommendations)
                    .GroupBy(v => v.VideoId)
                    .Select(v => v.FirstOrDefault())
                    .Take(maxCount)
                    .GroupBy(v => v.Rating)
                    .OrderByDescending(x => x.Key)
                    .SelectMany(x => x)
                    .ToList();

                _memoryCache.Set(userCacheKey, output, _cacheOptions);

                _logger.LogDebug("Saved {Count} recommendations to cache", output.Count);

                span?.Finish();
                
                return output;
            }
        }

        public async Task<IEnumerable<SearchResultDto>> GetPreviouslyPlayed(string name, bool useCache = true)
        {
            var span = SentrySdk.GetSpan()?.StartChild("RecommendationService.GetPreviouslyPlayed");
            span?.SetExtra(nameof(name), name);
            span?.SetExtra(nameof(useCache), useCache);

            using (_logger.BeginScope("GetPreviouslyPlayed for [{Name}], UseCache: {UseCache}", name, useCache))
            {
                var user = await _userRepository.Get(name);

                // var userCacheKey = $"previous_user_{name}";
                // if (useCache && _memoryCache.TryGetValue(userCacheKey, out List<SearchResultDto> cachedPlayed)) {
                //     // TODO: Log
                //     return cachedPlayed;
                // }

                // TODO: Incorporate user options
                var videos = await _videoRepository.GetPreviouslyPlayedForUser(name);

                var videoOptions = await _videoOptionsRepository.Get(
                    videos.Select(v => v.VideoId).ToList()
                );

                var userOptions = await _userVideoOptionsRepository.Get(
                    user.UserId,
                    videos.Select(v => v.VideoId).ToList()
                );

                var previouslyPlayed = videos
                    .GroupJoin(
                        videoOptions,
                        r => r.VideoId,
                        o => o.VideoId,
                        (x, o) => new {Video = x, Options = o}
                    )
                    .SelectMany(
                        x => x.Options.DefaultIfEmpty(),
                        (x, o) => new {x.Video, Options = o}
                    )
                    .GroupJoin(
                        userOptions,
                        r => r.Video.VideoId,
                        o => o.VideoId,
                        (x, o) => new {x.Video, x.Options, UserOptions = o}
                    )
                    .SelectMany(
                        x => x.UserOptions.DefaultIfEmpty(),
                        (x, o) => new {x.Video, x.Options, UserOptions = o}
                    )
                    .Select(
                        x => new SearchResultDto
                        {
                            Title = x.Video.Title,
                            VideoId = x.Video.YouTubeVideoId,
                            ThumbnailUrl = x.Video.ThumbnailUrl,
                            HasLyrics = !string.IsNullOrWhiteSpace(x.Options?.Lyrics),
                            Rating = (x.UserOptions?.Good == true ? 1 : 0) + (x.UserOptions?.Bad == true ? -1 : 0),
                            PlayCount = x.Video.PlayCount,
                            LastPlayed = x.Video.LastPlayed,
                            // TODO: Duration
                        })
                    .Where(v => v.Rating >= 0)
                    .OrderByDescending(v => v.Rating)
                    .ToList();

                _logger.LogDebug("Found {Count} previously played videos", previouslyPlayed.Count);

                // _memoryCache.Set(userCacheKey, previouslyPlayed, _cacheOptions);

                span?.Finish();

                return previouslyPlayed;
            }
        }

        public async Task<IEnumerable<SearchResultDto>> GetTopVideos(string name, bool useCache = true)
        {
            var span = SentrySdk.GetSpan()?.StartChild("RecommendationService.GetTopVideos");
            span?.SetExtra(nameof(name), name);
            span?.SetExtra(nameof(useCache), useCache);

            using (_logger.BeginScope("GetTopVideos for [{Name}], UseCache: {UseCache}", name, useCache))
            {
                var user = !string.IsNullOrEmpty(name) ? await _userRepository.Get(name) : null;

                var topVideoTasks = _videoSources
                    .Select(s => s.GetTopVideos())
                    .ToList();

                await Task.WhenAll(topVideoTasks);

                var topVideos = topVideoTasks
                    .SelectMany(t => t.Result)
                    .Where(v => v != null && v.Duration < _tenMinutes.TotalSeconds)
                    .ToList();
                
                _logger.LogDebug("Retrieved {Count} top video recommendations", topVideos.Count);

                var videoOptions = await _videoOptionsRepository.Get(
                    topVideos.Select(v => v.VideoId).ToList()
                );

                var userOptions = user == null ? new List<UserVideoOptions>()
                    : await _userVideoOptionsRepository.Get(
                        user.UserId,
                        topVideos.Select(v => v.VideoId).ToList()
                    );

                var output = topVideos
                    .GroupJoin(
                        videoOptions,
                        r => r.VideoId,
                        o => o.VideoId,
                        (x, o) => new {Video = x, Options = o}
                    )
                    .SelectMany(
                        x => x.Options.DefaultIfEmpty(),
                        (x, o) => new {x.Video, Options = o}
                    )
                    .GroupJoin(
                        userOptions,
                        r => r.Video.VideoId,
                        o => o.VideoId,
                        (x, o) => new {x.Video, x.Options, UserOptions = o}
                    )
                    .SelectMany(
                        x => x.UserOptions.DefaultIfEmpty(),
                        (x, o) => new {x.Video, x.Options, UserOptions = o}
                    )
                    .Select(
                        x => new SearchResultDto
                        {
                            Title = x.Video.Title,
                            VideoId = x.Video.YouTubeVideoId,
                            ThumbnailUrl = x.Video.ThumbnailUrl,
                            HasLyrics = !string.IsNullOrWhiteSpace(x.Options?.Lyrics),
                            Rating = (x.UserOptions?.Good == true ? 1 : 0) + (x.UserOptions?.Bad == true ? -1 : 0),
                            PlayCount = x.Video.PlayCount,
                            LastPlayed = x.Video.LastPlayed,
                            // TODO: Duration
                        })
                    .Where(v => v.Rating >= 0)
                    .OrderByDescending(v => v.Rating)
                    .ToList();

                _logger.LogDebug("Found {Count} top videos", output.Count);

                // _memoryCache.Set(userCacheKey, previouslyPlayed, _cacheOptions);

                span?.Finish();

                return output;
            }
        }

        public async Task UpdateVideoRecommendations(int videoId)
        {
            return;
            
            var span = SentrySdk.GetSpan()?.StartChild("RecommendationService.UpdateVideoRecommendations");
            span?.SetExtra(nameof(videoId), videoId);

            try
            {
                using (_logger.BeginScope("UpdateVideoRecommendations {VideoId}", videoId))
                {
                    var video = await _videoRepository.Get(videoId);

                    // TODO: Only query the relevant video source
                    var sourceTasks = _videoSources
                        .Select(async s => new
                            {Source = s.GetType().FullName, Related = await s.GetRelated(video.YouTubeVideoId)})
                        .ToList();

                    var recommendationTasks = (await Task.WhenAll(sourceTasks))
                        .SelectMany(s => s.Related.Select(async r =>
                        {
                            // TODO: Perform all of our detail lookups at once, to save API queries
                            var recommendedVideo = await _videoRepository.AddOrUpdate(r);
                            if (recommendedVideo.Duration == 0)
                            {
                                recommendedVideo = await _videoRepository.GetVideoDetails(recommendedVideo.YouTubeVideoId);
                                recommendedVideo = await _videoRepository.AddOrUpdate(recommendedVideo);
                            }
                            
                            return new VideoRecommendation
                            {
                                VideoId = videoId,
                                RecommendedVideoId = recommendedVideo.VideoId,
                                Source = s.Source,
                                LastUpdated = DateTime.UtcNow,
                            };
                        }))
                        .ToList();

                    await Task.WhenAll(recommendationTasks);

                    _logger.LogTrace("Updating recommendations for video {VideoId}", videoId);

                    var updateTasks = recommendationTasks
                        .Select(t => t.Result)
                        .ToList()
                        .Select(async r => await _videoRecommendationRepository.AddOrUpdate(r))
                        .ToList();

                    await Task.WhenAll(updateTasks);

                    _logger.LogInformation("Updated {Count} recommendations for video {VideoId}", updateTasks.Count,
                        videoId);
                }

                span?.Finish();
            }
            catch (VideoSourceLimitException ex)
            {
                span?.Finish(SpanStatus.ResourceExhausted);
                _logger.LogWarning(ex, "Video source limit reached");
            }
            catch (Exception ex)
            {
                span?.Finish(SpanStatus.InternalError);
                _logger.LogError(ex, "Recommendation update error");
            }
        }
    }
}