// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Autofac;
using Autofac.Features.AttributeFilters;
using KaraokeMaster.Application.Autofac;
using KaraokeMaster.Application.Workers;
using KaraokeMaster.Domain.Models.Configuration;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Kiosk;
using KaraokeMaster.Services.Player;
using KaraokeMaster.Services.Queue;
using KaraokeMaster.Services.Reaction;
using KaraokeMaster.Services.Recommendations;
using KaraokeMaster.Services.Search;
using KaraokeMaster.Services.Session;
using Microsoft.Extensions.Hosting;

namespace KaraokeMaster.Application
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SearchService>().As<ISearchService>().WithAttributeFiltering();
            builder.RegisterType<QueueService>().As<IQueueService>();
            builder.RegisterType<RecommendationService>().As<IRecommendationService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<SessionService>().As<ISessionService>();
            builder.RegisterType<PlayerStateService>().As<IPlayerStateService>().SingleInstance();
            builder.RegisterType<UserStateService>().As<IUserStateService>().SingleInstance();
            builder.RegisterType<ReactionService>().As<IReactionService>().SingleInstance();
            builder.RegisterType<KioskService>().As<IKioskService>();
            
            builder.RegisterConfig<RecommendationConfiguration>(RecommendationConfiguration.ConfigurationKey);
            builder.RegisterConfig<VideoUpdateConfiguration>(VideoUpdateConfiguration.ConfigurationKey);
            builder.RegisterConfig<FontAwesomeConfiguration>(FontAwesomeConfiguration.ConfigurationKey);
            builder.RegisterConfig<WebPushConfiguration>(WebPushConfiguration.ConfigurationKey);
            
            builder.RegisterType<VideoUpdateWorker>()
                .As<IHostedService>()
                .InstancePerDependency();
            
            builder.RegisterType<WebPushWorker>()
                .As<IHostedService>()
                .InstancePerDependency();
            
            base.Load(builder);
        }
    }
}