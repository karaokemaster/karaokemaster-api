// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Kiosk;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Application;

public class KioskService : IKioskService
{
    private readonly ILogger<KioskService> _logger;
    private readonly IKioskRepository _kioskRepository;

    public KioskService(ILoggerFactory loggerFactory, IKioskRepository kioskRepository)
    {
        _logger = loggerFactory.CreateLogger<KioskService>();
        _kioskRepository = kioskRepository;
    }
    
    public async Task<Kiosk> EnsureKioskExists(string name, byte[] publicKey)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskService.EnsureKioskExists");
        span?.SetExtra(nameof(name), name);
        span?.SetExtra(nameof(publicKey), publicKey);

        using (_logger.BeginScope("EnsureKioskExists(\"{Name}\", \"{PublicKey}\")", name, publicKey))
        {
            var kiosk = await _kioskRepository.Get(publicKey);
            if (kiosk == null)
            {
                _logger.LogInformation("Kiosk not found; creating new record");
                    
                // TODO: Add key hash to name
                kiosk = new Kiosk {PublicKey = publicKey, Name = name, Adopted = false};

                await _kioskRepository.Add(kiosk);

                kiosk = await _kioskRepository.Get(publicKey);
            }

            _logger.LogDebug("Returning kiosk ID {KioskId}", kiosk.KioskId);
                
            span?.Finish();

            return kiosk;
        }
    }

    public async Task<Kiosk> Get(int kioskId)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskService.Get");
        span?.SetExtra(nameof(kioskId), kioskId);

        using (_logger.BeginScope("Get({KioskId})", kioskId))
        {
            var kiosk = await _kioskRepository.Get(kioskId);

            _logger.LogDebug("Returning kiosk ID {KioskId}", kiosk.KioskId);
                
            span?.Finish();

            return kiosk;
        }
    }
    
    public async Task<Kiosk> Update(Kiosk kiosk)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskService.Update");
        span?.SetExtra(nameof(kiosk), kiosk);

        using (_logger.BeginScope("Update({KioskId})", kiosk.KioskId))
        {
            _logger.LogDebug("Updating kiosk ID {KioskId}", kiosk.KioskId);
                
            await _kioskRepository.Update(kiosk);
            kiosk = await _kioskRepository.Get(kiosk.KioskId);
            
            span?.Finish();

            return kiosk;
        }
    }

    public Task Remove(int kioskId)
    {
        throw new System.NotImplementedException();
    }
}