// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Application
{
    public class SearchService : ISearchService
    {
        private readonly ILogger<SearchService> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IVideoRepository _videoRepository;
        private readonly IVideoOptionsRepository _videoOptionsRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserVideoOptionsRepository _userVideoOptionsRepository;
        private readonly IEnumerable<IVideoSource> _videoSources;
        private readonly ISearchProvider _searchProvider;

        public SearchService(ILoggerFactory loggerFactory, IMemoryCache memoryCache, IVideoRepository videoRepository, IVideoOptionsRepository videoOptionsRepository, IUserRepository userRepository, IUserVideoOptionsRepository userVideoOptionsRepository, IEnumerable<IVideoSource> videoSources, [Optional] ISearchProvider searchProvider)
        {
            _logger = loggerFactory.CreateLogger<SearchService>();
            _memoryCache = memoryCache;
            _videoRepository = videoRepository;
            _videoOptionsRepository = videoOptionsRepository;
            _userRepository = userRepository;
            _userVideoOptionsRepository = userVideoOptionsRepository;
            _videoSources = videoSources;
            _searchProvider = searchProvider;
        }
        
        public async Task<IEnumerable<SearchResultDto>> Search(string query, string username = null)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SearchService.Search");
            span?.SetExtra(nameof(query), query);
            span?.SetExtra(nameof(username), username);

            var (source, videoId) = UrlParser.GetVideoId(query);

            List<Video> results;
            
            if (source != null)
            {
                // User searched for a known video service URL...
                // TODO: Only query the relevant service
                var videoTasks = _videoSources
                    .Select(s => s.GetVideoDetails(videoId))
                    .ToList();

                await Task.WhenAll(videoTasks);

                results = videoTasks
                    .Select(t => t.Result)
                    .ToList();
            }
            else
            {
                var searchTasks = _videoSources
                    .Select(s => s.Search(query))
                    .ToList();

                await Task.WhenAll(searchTasks);

                results = searchTasks
                    .Select(t => t.Result)
                    .SelectMany(v => v)
                    .ToList();
            }

            var user = await _userRepository.Get(username ?? "");
            
            var localResults = (await Autocomplete(query, username)).ToList();

            // TODO: Better way to surface results that exist both locally and in the remote search results
            var searchResults = (await GetVideoDetails(results, user?.UserId))
                .Union(localResults.Where(l => !results.Any(r => r.YouTubeVideoId == l.VideoId)))
                .Select(v => new
                {
                    r = results.Any(r => r.YouTubeVideoId == v.VideoId),
                    l = localResults.Any(l => l.VideoId == v.VideoId),
                    v,
                })
                .Select(s => new
                {
                    p = s.r && s.l ? 3 : (s.r ? 2 : (s.l ? 1 : 0)),
                    s.v,
                })
                .OrderByDescending(s => s.p)
                .ThenByDescending(s => s.v.Rating)
                .Select(s => s.v)
                .ToList();

            span?.Finish();
            
            return searchResults;
        }

        public async Task<IEnumerable<SearchResultDto>> Autocomplete(string query, string username = null)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SearchService.Autocomplete");
            span?.SetExtra(nameof(query), query);
            span?.SetExtra(nameof(username), username);

            List<Video> results;
            
            if (_searchProvider != null)
            {
                results = await _searchProvider.Search(query);
            }
            else
            {
                results = await _videoRepository.Find(query);
            }
            
            var user = await _userRepository.Get(username ?? "");
            
            var searchResults = (await GetVideoDetails(results, user?.UserId))
                .OrderByDescending(v => v.Rating)
                .ToList();

            span?.Finish();
            
            return searchResults;
        }

        public async Task<IEnumerable<SearchResultDto>> GetRelated(string videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SearchService.GetRelated");
            span?.SetExtra(nameof(videoId), videoId);

            var relatedTasks = _videoSources
                .Select(s => s.GetRelated(videoId))
                .ToList();
            
            await Task.WhenAll(relatedTasks);

            var results = relatedTasks
                .Select(t => t.Result)
                .SelectMany(v => v)
                .ToList();

            var related = await GetVideoDetails(results);

            span?.Finish();

            return related;
        }

        private async Task<List<SearchResultDto>> GetVideoDetails(List<Video> results, int? userId = null)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SearchService.GetVideoDetails");
            span?.SetExtra(nameof(userId), userId);

            var videoIds = results
                .Select(v => v.YouTubeVideoId)
                .ToList();

            var videos = await _videoRepository.Get(videoIds);

            var videoOptions = await _videoOptionsRepository.Get(
                videos.Select(v => v.VideoId).ToList()
            );

            var userOptions = new List<UserVideoOptions>();
            if (userId.HasValue)
                userOptions = await _userVideoOptionsRepository.GetAll(userId.Value);
            
            // TODO: Should include thumbnail dimensions as well
            var searchResults = results
                .GroupJoin(videos,
                    result => result.YouTubeVideoId,
                    video => video.YouTubeVideoId,
                    (r, v) => new { Result = r, Video = v }
                )
                .SelectMany(
                    x => x.Video.DefaultIfEmpty(),
                    (x, v) => new { x.Result, Video = v }
                )
                .GroupJoin(
                    videoOptions,
                    r => r.Video?.VideoId,
                    o => o.VideoId,
                    (x, o) => new { x.Result, x.Video, Options = o }
                )
                .SelectMany(
                    x => x.Options.DefaultIfEmpty(),
                    (x, o) => new { x.Result, x.Video, Options = o }
                )
                .GroupJoin(userOptions,
                    x => x.Video?.VideoId,
                    o => o.VideoId,
                    (x, o) => new { x.Result, x.Video, x.Options, UserOptions = o }
                )
                .SelectMany(
                    x => x.UserOptions.DefaultIfEmpty(),
                    (x, o) => new { x.Result, x.Video, x.Options, UserOptions = o }
                )
                .Select(
                    x => new SearchResultDto
                    {
                        Title = x.Result.Title,
                        VideoId = x.Result.YouTubeVideoId,
                        ThumbnailUrl = x.Result.ThumbnailUrl,
                        HasLyrics = !string.IsNullOrWhiteSpace(x.Options?.Lyrics),
                        Rating = (x.UserOptions?.Good == true ? 1 : 0) + (x.UserOptions?.Bad == true ? -1 : 0),
                        // TODO: Duration
                    })
                .ToList();

            span?.Finish();

            return searchResults;
        }
    }
}