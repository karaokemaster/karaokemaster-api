// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using KaraokeMaster.Services.Player;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Application
{
    public class PlayerStateService : IPlayerStateService
    {
        private readonly ILogger<PlayerStateService> _logger;
        private readonly Subject<PlayerState> _subject;
        private readonly IObservable<PlayerState> _stream;
        private PlaybackState? _lastState = null;
        
        public PlayerStateService(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<PlayerStateService>();
            
            _subject = new Subject<PlayerState>();
            var c = _subject.Publish();
            c.Connect();

            _stream = Observable.Create<PlayerState>(observer =>
            {
                var subscription = c.Subscribe(observer);

                return subscription;
            });
        }

        public IDisposable Subscribe(IObserver<PlayerState> observer)
        {
            return _stream.Subscribe(observer);
        }

        public void Update(PlayerState state)
        {
            _subject.OnNext(state);

            if (state.State == _lastState) return;
            
            _logger.LogTrace("PlayerState Update: {State}", state.State);
            _lastState = state.State;
        }

        protected void Error(Exception ex)
        {
            _subject.OnError(ex);
        }
    }
}