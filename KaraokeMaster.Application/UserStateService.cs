// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using WebPush;

namespace KaraokeMaster.Application;

public class UserStateService : IUserStateService
{
    private const string CacheKey = "SessionUsers";
    private const string PushCacheKey = "SessionPush";
    
    private readonly ILogger<UserStateService> _logger;
    private readonly IMemoryCache _memoryCache;
    private readonly Func<IUserService> _userServiceFactory;

    public UserStateService(ILoggerFactory loggerFactory, IMemoryCache memoryCache, Func<IUserService> userServiceFactory)
    {
        _logger = loggerFactory.CreateLogger<UserStateService>();
        _memoryCache = memoryCache;
        _userServiceFactory = userServiceFactory;

        ClearSession();
    }

    // TODO: Don't rebuild this every stinkin' time
    private MemoryCacheEntryOptions CacheOptions
    {
        get
        {
            var expireAt = DateTime.Today.AddHours(6);
            if (expireAt < DateTime.Now)
                expireAt = expireAt.AddDays(1);
        
            return new MemoryCacheEntryOptions().SetAbsoluteExpiration(expireAt);
        }
    }
    public async Task Login(string username, PushSubscription subscription = null)
    {
        var userService = _userServiceFactory();
        var list = GetUserList();
        
        var user = await userService.EnsureUserExists(username);
        
        if (!list.Contains(user.UserId))
            list.Add(user.UserId);

        var subs = GetPushSubscriptions();

        if (subscription != null)
        {
            subs[user.UserId] = subscription;
        }

        _memoryCache.Set(CacheKey, list, CacheOptions);
        _memoryCache.Set(PushCacheKey, subs, CacheOptions);
    }

    public async Task<List<User>> GetUsers()
    {
        var userService = _userServiceFactory();
        var list = GetUserList();

        var userTasks = list.Select(i => userService.GetUserById(i)).ToList();
        await Task.WhenAll(userTasks);
        var users = userTasks.Select(t => t.Result).ToList();

        return users;
    }

    public async Task<List<User>> GetAllUsers()
    {
        var userService = _userServiceFactory();

        var sessionUsers = await this.GetUsers();
        var activeUsers = await userService.GetActive();
        var allUsers = await userService.GetAll();

        var users = allUsers
            .Select(u =>
            {
                u.IsActive = sessionUsers.Any(s => s.UserId == u.UserId) || activeUsers.Any(a => a.UserId == u.UserId && a.IsActive);
                return u;
            })
            .ToList();

        return users;
    }

    public async Task<PushSubscription> GetUserPushSubscription(int userId)
    {
        var subs = GetPushSubscriptions();

        return !subs.ContainsKey(userId) ? null : subs[userId];
    }
    
    public async Task ClearSession()
    {
        _memoryCache.Remove(CacheKey);
        _memoryCache.Remove(PushCacheKey);
    }

    private List<int> GetUserList()
    {
        var list = _memoryCache.GetOrCreate(CacheKey, entry => new List<int>());
        return list;
    }

    private Dictionary<int, PushSubscription> GetPushSubscriptions()
    {
        // TODO: Switch away from dictionary to allow multiple subs per user
        var list = _memoryCache.GetOrCreate(PushCacheKey, entry => new Dictionary<int, PushSubscription>());
        return list;
    }
}