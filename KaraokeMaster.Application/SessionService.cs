// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Queue;
using KaraokeMaster.Services.Search;
using KaraokeMaster.Services.Session;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Application
{
    public class SessionService : ISessionService
    {
        private readonly ILogger<SessionService> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IUserRepository _userRepository;
        private readonly IUserVideoOptionsRepository _userVideoOptionsRepository;
        private readonly ISessionRepository _sessionRepository;
        private readonly MemoryCacheEntryOptions _cacheOptions;

        public SessionService(ILoggerFactory loggerFactory, IMemoryCache memoryCache, IUserRepository userRepository, IUserVideoOptionsRepository userVideoOptionsRepository, ISessionRepository sessionRepository)
        {
            _logger = loggerFactory.CreateLogger<SessionService>();
            _memoryCache = memoryCache;
            _userRepository = userRepository;
            _userVideoOptionsRepository = userVideoOptionsRepository;
            _sessionRepository = sessionRepository;

            _cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10));
        }

        public async Task<List<SessionDto>> GetSessions(string name)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionService.GetSessions");
            span?.SetExtra(nameof(name), name);
            
            using (_logger.BeginScope("GetSessions(\"{Name}\")", name))
            {
                var user = await _userRepository.Get(name);

                if (user == null)
                {
                    span?.Finish(SpanStatus.NotFound);
                    throw new Exception("User not found!");
                }

                var sessions = (await _sessionRepository
                        .GetSessions(user.UserId))
                    .Select(s => new SessionDto
                    {
                        Date = s.Date,
                        VideoCount = s.VideoCount
                    })
                    .ToList();

                span?.Finish();
                
                return sessions;
            }
        }

        public async Task<SessionDto> GetSession(string name, DateTime session)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionService.GetSession");
            span?.SetExtra(nameof(name), name);
            span?.SetExtra(nameof(session), session);
            
            using (_logger.BeginScope("GetSession(\"{Name}\", {Date:yyyy-MM-dd}", name, session))
            {
                var user = await _userRepository.Get(name);
                if (user == null)
                {
                    span?.Finish(SpanStatus.NotFound);
                    throw new Exception("User not found!");
                }

                var dbSession = await _sessionRepository.GetSession(session);
                if (dbSession == null)
                {
                    span?.Finish(SpanStatus.NotFound);
                    throw new Exception("Session not found!");
                }

                var videos = await _sessionRepository
                    .GetSessionVideos(user.UserId, session);
                var options = await _userVideoOptionsRepository.Get(
                    user.UserId,
                    videos.Select(v => v.VideoId).ToList()
                );

                // TODO: Use mapper of some kind instead
                var sessionVideos = videos
                    .GroupJoin(
                        options,
                        r => r.VideoId,
                        o => o.VideoId,
                        (x, o) => new {Video = x, UserOptions = o}
                    )
                    .SelectMany(
                        x => x.UserOptions.DefaultIfEmpty(),
                        (x, o) => new {x.Video, UserOptions = o}
                    )
                    .Select(
                        x => new SearchResultDto
                        {
                            Title = x.Video.Title,
                            VideoId = x.Video.YouTubeVideoId,
                            ThumbnailUrl = x.Video.ThumbnailUrl,
                            HasLyrics = !string.IsNullOrWhiteSpace(x.Video.Options?.Lyrics),
                            Rating = (x.UserOptions?.Good == true ? 1 : 0) + (x.UserOptions?.Bad == true ? -1 : 0),
                            PlayCount = x.Video.PlayCount,
                            LastPlayed = x.Video.LastPlayed,
                            // TODO: Duration
                        })
                    .ToList();

                var users = (await _sessionRepository
                        .GetSessionUsers(session))
                    .Select(u => u.Username)
                    .ToList();

                var result = new SessionDto
                {
                    Date = dbSession.Date,
                    VideoCount = dbSession.VideoCount,
                    Videos = sessionVideos,
                    Users = users
                };

                span?.Finish();
                
                return result;
            }
        }
        
        public async Task<List<QueueEntryDto>> GetAllSessionVideos(DateTime session)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionService.GetAllSessionVideos");
            span?.SetExtra(nameof(session), session);
            
            using (_logger.BeginScope("GetAllSessionVideos({Date:yyyy-MM-dd}", session))
            {
                var videos = await _sessionRepository
                    .GetAllSessionVideos(session);

                // TODO: Use mapper of some kind instead
                var result = videos
                    .Select(q => new QueueEntryDto
                    {
                        QueueId = q.QueueId,
                        VideoId = q.Video.YouTubeVideoId,
                        Title = q.Video.Title,
                        Duration = q.Video.Duration,
                        ThumbnailUrl = q.Video.ThumbnailUrl,
                        Lyrics = q.Video.Options?.Lyrics,
                        Order = q.Order,
                        User = q.User.Username,
                        UserIcon = q.User.Icon,
                        UserColor = q.User.Color,
                        LightingCue = q.UserOptions?.LightingCue ?? q.Video.Options?.LightingCue,
                        Rating = (q.UserOptions?.Good == true ? 1 : 0) + (q.UserOptions?.Bad == true ? -1 : 0),
                        PlaybackDate = q.PlaybackDate
                    })
                    .ToList();

                span?.Finish();
                
                return result;
            }
        }
    }
}