// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Queue;
using KaraokeMaster.Services.Recommendations;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Application
{
    public class QueueService : IQueueService
    {
        private readonly ILogger<QueueService> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IQueueRepository _queueRepository;
        private readonly IVideoRepository _videoRepository;
        private readonly IVideoOptionsRepository _videoOptionsRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserVideoOptionsRepository _userVideoOptionsRepository;
        private readonly IRecommendationService _recommendationService;
        private readonly MemoryCacheEntryOptions _cacheOptions;

        public QueueService(ILoggerFactory loggerFactory, IMemoryCache memoryCache, IQueueRepository queueRepository, IVideoRepository videoRepository, IVideoOptionsRepository videoOptionsRepository, IUserRepository userRepository, IUserVideoOptionsRepository userVideoOptionsRepository, IRecommendationService recommendationService)
        {
            _logger = loggerFactory.CreateLogger<QueueService>();
            _memoryCache = memoryCache;
            _queueRepository = queueRepository;
            _videoRepository = videoRepository;
            _videoOptionsRepository = videoOptionsRepository;
            _userRepository = userRepository;
            _userVideoOptionsRepository = userVideoOptionsRepository;
            _recommendationService = recommendationService;

            _cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10));
        }
        
        public async Task<List<QueueEntryDto>> Get()
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.GetQueue");
            
            var videos = (await _queueRepository
                .GetAll(true))
                // TODO: Use mapper of some kind instead
                .Select(q => new QueueEntryDto
                {
                    QueueId = q.QueueId,
                    VideoId = q.Video.YouTubeVideoId,
                    Title = q.Video.Title,
                    Duration = q.Video.Duration,
                    ThumbnailUrl = q.Video.ThumbnailUrl,
                    Lyrics = q.Video.Options?.Lyrics,
                    Order = q.Order,
                    User = q.User.Username,
                    UserIcon = q.User.Icon,
                    UserColor = q.User.Color,
                    LightingCue = q.UserOptions?.LightingCue ?? q.Video.Options?.LightingCue,
                    Rating = (q.UserOptions?.Good == true ? 1 : 0) + (q.UserOptions?.Bad == true ? -1 : 0),
                    PlaybackDate = q.PlaybackDate
                })
                .ToList();

            span?.Finish();
            
            return videos;
        }

        public async Task<List<QueueEntryDto>> GetRecent()
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.GetRecent");

            var videos = (await Get())
                .Where(q => q.Order < 0)
                .ToList();

            span?.Finish();
            
            return videos;
        }

        public async Task SetLyrics(int queueId, string lyrics)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.SetLyrics");
            span?.SetExtra(nameof(queueId), queueId);
            span?.SetExtra(nameof(lyrics), lyrics);

            using (_logger.BeginScope("SetLyrics({QueueID})", queueId))
            {
                var entry = await _queueRepository.Get(queueId);
                var video = await _videoRepository.Get(entry.VideoId);

                if (video == null)
                {
                    _logger.LogWarning("Video {Id} not found!", entry.VideoId);
                    span?.Finish(SpanStatus.NotFound);
                    return;
                }

                var options = await _videoOptionsRepository.Get(video.VideoId);
                if (options == null)
                {
                    _logger.LogDebug("Video options not found; creating new record");
                    
                    options = new VideoOptions
                    {
                        VideoId = video.VideoId,
                    };
                }
                
                lyrics = lyrics.Trim();
                if (string.IsNullOrWhiteSpace(lyrics)) lyrics = null;

                options.Lyrics = lyrics;

                await _videoOptionsRepository.AddOrUpdate(options);
            }

            span?.Finish();
        }

        public async Task SetLightingCue(int queueId, string cue)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.SetLightingCue");
            span?.SetExtra(nameof(queueId), queueId);
            span?.SetExtra(nameof(cue), cue);

            using (_logger.BeginScope("SetLightingCue({QueueId}, \"{Cue}\")", queueId, cue))
            {
                var entry = await _queueRepository.Get(queueId);
                var user = await _userRepository.Get(entry.UserId);
                var video = await _videoRepository.Get(entry.VideoId);

                if (user == null)
                {
                    _logger.LogWarning("User {Name} not found!", entry.User);
                    span?.Finish(SpanStatus.NotFound);
                    return;
                }
                if (video == null)
                {
                    _logger.LogWarning("Video {Id} not found!", entry.VideoId);
                    span?.Finish(SpanStatus.NotFound);
                    return;
                }

                var options = await _userVideoOptionsRepository.Get(user.UserId, video.VideoId);
                if (options == null)
                {
                    _logger.LogDebug("User video options not found; creating new record");
                    
                    options = new UserVideoOptions
                    {
                        UserId = user.UserId,
                        VideoId = video.VideoId,
                    };
                }
                
                cue = cue.Trim();
                if (string.IsNullOrWhiteSpace(cue)) cue = null;

                options.LightingCue = cue;

                await _userVideoOptionsRepository.AddOrUpdate(options);
            }
            
            span?.Finish();
        }

        public async Task SetRating(int queueId, int rating)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.SetRating");
            span?.SetExtra(nameof(queueId), queueId);
            span?.SetExtra(nameof(rating), rating);

            using (_logger.BeginScope("SetRating({QueueId}, {Rating})", queueId, rating))
            {
                var entry = await _queueRepository.Get(queueId);
                var user = await _userRepository.Get(entry.UserId);
                var video = await _videoRepository.Get(entry.VideoId);

                if (user == null)
                {
                    _logger.LogWarning("User {Name} not found!", entry.User);
                    span?.Finish(SpanStatus.NotFound);
                    return;
                }
                if (video == null)
                {
                    _logger.LogWarning("Video {Id} not found!", entry.VideoId);
                    span?.Finish(SpanStatus.NotFound);
                    return;
                }

                var options = await _userVideoOptionsRepository.Get(user.UserId, video.VideoId);
                if (options == null)
                {
                    _logger.LogDebug("User video options not found; creating new record");
                    
                    options = new UserVideoOptions
                    {
                        UserId = user.UserId,
                        VideoId = video.VideoId,
                    };
                }

                options.Good = rating > 0;
                options.Bad = rating < 0;

                _logger.LogDebug("Setting good = {Good}, bad = {Bad}", options.Good, options.Bad);

                await _userVideoOptionsRepository.AddOrUpdate(options);
            }
            
            span?.Finish();
        }

        public async Task SetUser(int queueId, string username)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.SetUser");
            span?.SetExtra(nameof(queueId), queueId);
            span?.SetExtra(nameof(username), username);

            using (_logger.BeginScope("SetUser({QueueId}, {User})", queueId, username))
            {
                var entry = await _queueRepository.Get(queueId);
                if (entry == null)
                {
                    _logger.LogWarning("Queue entry {Id} not found!", queueId);
                    span?.Finish(SpanStatus.NotFound);
                    return;
                }

                // TODO: This isn't really our responsibility...
                var user = await _userRepository.Get(username);
                if (user == null)
                {
                    user = new User {Username = username};

                    await _userRepository.AddOrUpdate(user);

                    user = await _userRepository.Get(username);
                }

                entry.UserId = user.UserId;

                await _queueRepository.Update(entry);
            }
            
            span?.Finish();
        }

        public async Task Add(SearchResultDto entry, string username)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.Add");
            span?.SetExtra(nameof(entry), entry);
            span?.SetExtra(nameof(username), username);

            using (_logger.BeginScope("Adding video: {VideoId}", entry.VideoId))
            {
                // TODO: Confirm that this is actually working
                // TODO: Don't do this lookup if we already have the details
                var video = await _videoRepository.GetVideoDetails(entry.VideoId);
                var isNew = video.VideoId == 0;
                video = await _videoRepository.AddOrUpdate(video);
                
                if (isNew)
                {
                    await _recommendationService.UpdateVideoRecommendations(video.VideoId);
                }

                // TODO: This isn't really our responsibility...
                var user = await _userRepository.Get(username);
                if (user == null)
                {
                    user = new User {Username = username};

                    await _userRepository.AddOrUpdate(user);

                    user = await _userRepository.Get(username);
                }

                // TODO: Mapper
                var queueEntry = new QueueEntry
                {
                    VideoId = video.VideoId,
                    Order = int.MaxValue,
                    UserId = user.UserId,
                    PlaybackDate = null
                };

                await _queueRepository.Add(queueEntry);
            }
            
            span?.Finish();
        }

        public async Task Move(int queueId, int? afterId, int? beforeId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.Move");
            span?.SetExtra(nameof(queueId), queueId);
            span?.SetExtra(nameof(beforeId), beforeId);
            span?.SetExtra(nameof(afterId), afterId);

            using (_logger.BeginScope("Moving queue item: {QueueId}", queueId))
            {
                await _queueRepository.Move(queueId, afterId, beforeId);
            }
            
            span?.Finish();
        }

        public async Task Remove(int queueId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.Remove");
            span?.SetExtra(nameof(queueId), queueId);

            using (_logger.BeginScope("Removing queue item: {QueueId}", queueId))
            {
                await _queueRepository.Delete(queueId);
            }
            
            span?.Finish();
        }

        public async Task StartVideo(int queueId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.StartVideo");
            span?.SetExtra(nameof(queueId), queueId);

            var entry = await _queueRepository.Get(queueId);

            if (entry == null)
            {
                span?.Finish(SpanStatus.NotFound);
                return;
            }

            entry.Order = 0;
            entry.PlaybackDate = DateTime.Now;

            await _queueRepository.Update(entry);
            
            span?.Finish();
        }

        public async Task EndVideo(int queueId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueService.EndVideo");
            span?.SetExtra(nameof(queueId), queueId);

            var entry = await _queueRepository.Get(queueId);

            if (entry == null)
            {
                span?.Finish(SpanStatus.NotFound);
                return;
            }

            entry.Order = -1;

            await _queueRepository.Update(entry);
            
            span?.Finish();
        }
    }
}