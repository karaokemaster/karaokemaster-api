// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Domain.Models.Configuration;
using KaraokeMaster.Services.Recommendations;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sentry;

namespace KaraokeMaster.Application.Workers
{
    public class VideoUpdateWorker : IHostedService, IDisposable
    {
        private readonly ILogger<VideoUpdateWorker> _logger;
        private readonly IOptionsMonitor<VideoUpdateConfiguration> _videoUpdateConfiguration;
        private readonly IVideoRepository _videoRepository;
        private readonly IRecommendationService _recommendationService;
        private readonly IEnumerable<IVideoSource> _videoSources;

        private TimeSpan _interval;
        private Timer _timer;
        private IDisposable _configListener;
        
        public VideoUpdateWorker(ILoggerFactory loggerFactory, IOptionsMonitor<VideoUpdateConfiguration> videoUpdateConfiguration, IVideoRepository videoRepository, IRecommendationService recommendationService, IEnumerable<IVideoSource> videoSources)
        {
            _logger = loggerFactory.CreateLogger<VideoUpdateWorker>();
            _videoUpdateConfiguration = videoUpdateConfiguration;
            _videoRepository = videoRepository;
            _recommendationService = recommendationService;
            _videoSources = videoSources;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting video update service");

            Start();

            _configListener = _videoUpdateConfiguration.OnChange(OnConfigurationChange);
            
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping video update service");

            _configListener?.Dispose();
            _configListener = null;

            await Stop();
        }

        public void Dispose()
        {
            _configListener?.Dispose();
            _timer?.Dispose();
        }
        
        private void Start()
        {
            _interval = _videoUpdateConfiguration.CurrentValue.Interval;
            
            _logger.LogDebug("Starting video update timer with interval {Interval}", _interval);

            _timer = new Timer(state =>
            {
                SentrySdk.ConfigureScope(async scope =>
                {
                    var transaction = SentrySdk.StartTransaction("VideoUpdateService", "TimerInterval");
                    scope.Transaction = transaction;

                    await UpdateVideos();
                    
                    transaction.Finish();
                });
            }, null, TimeSpan.Zero, _interval);
        }

        private async Task Stop()
        {
            if (_timer == null) return;

            _timer.Change(Timeout.Infinite, 0);
            await _timer.DisposeAsync();
            _timer = null;
        }
        
        private async void OnConfigurationChange(VideoUpdateConfiguration configuration)
        {
            if (_interval == configuration.Interval) return;
            
            _logger.LogInformation("Configuration changed; restarting...");

            await Stop();
            Start();
        }

        private async Task UpdateVideos()
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoUpdateService.UpdateVideos");

            try
            {
                using (_logger.BeginScope("UpdateVideos"))
                {
                    var maxVideos = (int) Math.Floor(_videoUpdateConfiguration.CurrentValue.MaxPerDay /
                                                     (TimeSpan.FromDays(1) / _interval));
                    var minAge = _videoUpdateConfiguration.CurrentValue.MinVideoAge;
                    var updatedBefore = DateTime.UtcNow - minAge;

                    var videos = await _videoRepository.GetStale(updatedBefore, maxVideos);

                    _logger.LogDebug("Found {Count} videos updated before {UpdatedBefore} (max {MaxVideos})",
                        videos.Count, updatedBefore, maxVideos);

                    videos.ForEach(UpdateVideo);

                    span?.Finish();
                }
            }
            catch (VideoSourceLimitException ex)
            {
                span?.Finish(SpanStatus.ResourceExhausted);
                _logger.LogWarning(ex, "Video source limit reached");
            }
            catch (Exception ex)
            {
                span?.Finish(SpanStatus.InternalError);
                _logger.LogError(ex, "Update videos error");
            }
        }

        private async void UpdateVideo(Video video)
        {
            var videoId = video.YouTubeVideoId;
            
            var span = SentrySdk.GetSpan()?.StartChild("VideoUpdateService.UpdateVideo");
            span?.SetExtra(nameof(videoId), videoId);

            try
            {
                using (_logger.BeginScope("UpdateVideo {VideoId}", videoId))
                {
                    // TODO: Only query the relevant video source
                    var detailTasks = _videoSources
                        .Select(s => s.GetVideoDetails(videoId))
                        .ToList();

                    await Task.WhenAll(detailTasks);

                    var videoDetails = detailTasks
                        .Select(t => t.Result)
                        .FirstOrDefault(v => v != null);

                    if (videoDetails == null)
                    {
                        _logger.LogTrace("Video {VideoId} is no longer available", videoId);

                        // Video is no longer available
                        video.Unavailable = true;
                        video.LastUpdate = DateTime.UtcNow;
                    }
                    else
                    {
                        _logger.LogTrace("Updating details of video {VideoId}", videoId);

                        // TODO: It'd be nice to use a mapper or something to do this for us
                        video.Title = videoDetails.Title;
                        video.ThumbnailUrl = videoDetails.ThumbnailUrl;
                        video.Duration = videoDetails.Duration;

                        video.Unavailable = false;
                        video.LastUpdate = DateTime.UtcNow;
                    }

                    await _videoRepository.AddOrUpdate(video);
                }
            
                span?.Finish();

                if (!video.Unavailable && video.PlayCount > 0)
                    await _recommendationService.UpdateVideoRecommendations(video.VideoId);
            }
            catch (VideoSourceLimitException)
            {
                span?.Finish(SpanStatus.ResourceExhausted);
                throw;
            }
            catch (Exception ex)
            {
                span?.Finish(SpanStatus.InternalError);
                _logger.LogError(ex, "Video details update error");
            }
        }
    }
}