// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Domain.Models.Configuration;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Player;
using KaraokeMaster.Services.Queue;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebPush;

namespace KaraokeMaster.Application.Workers;

public class WebPushWorker : IHostedService, IDisposable
{
    private readonly ILogger<WebPushWorker> _logger;
    private readonly IOptionsMonitor<WebPushConfiguration> _webPushConfiguration;
    private readonly IPlayerStateService _playerStateService;
    private readonly IQueueRepository _queueRepository;
    private readonly IQueueService _queueService;
    private readonly IUserStateService _userStateService;

    private VapidDetails _vapidDetails;
    private IDisposable _configListener;
    private CancellationTokenSource _cancellationTokenSource;

    public WebPushWorker(ILoggerFactory loggerFactory, IOptionsMonitor<WebPushConfiguration> webPushConfiguration, IPlayerStateService playerStateService, IQueueRepository queueRepository, IQueueService queueService, IUserStateService userStateService)
    {
        _logger = loggerFactory.CreateLogger<WebPushWorker>();
        _webPushConfiguration = webPushConfiguration;
        _playerStateService = playerStateService;
        _queueRepository = queueRepository;
        _queueService = queueService;
        _userStateService = userStateService;
    }
        
    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Starting web push service");

        _configListener = _webPushConfiguration.OnChange(OnConfigurationChange);
        OnConfigurationChange(_webPushConfiguration.CurrentValue);
        
        Start();

        return Task.CompletedTask;
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Stopping web push service");

        _configListener?.Dispose();
        _configListener = null;

        await Stop();
    }

    public void Dispose()
    {
        _configListener?.Dispose();
    }

    private void Start()
    {
        _cancellationTokenSource = new CancellationTokenSource();
        
        var stream = Observable.Create<PlayerState>(observer =>
        {
            var disposable = _playerStateService.Subscribe(observer);

            return Disposable.Create(() =>
            {
                disposable.Dispose();
                _logger.LogDebug("Player state subscription stopped");

            });
        });
        
        // Notify if time remaining between 60 and 30 seconds (avoids overlap with previous track)
        stream
            .Where(s => s.VideoDuration - s.VideoTime < 60 && s.VideoDuration - s.VideoTime > 30)
            .Select(s => s.QueueId)
            .DistinctUntilChanged()
            .Subscribe(
                NotifySubscribers, 
                () => { }, 
                _cancellationTokenSource.Token
            );
    }

    private async Task Stop()
    {
        _cancellationTokenSource?.Cancel();
    }

    private async void NotifySubscribers(int? queueId)
    {
        if (!queueId.HasValue) return;
        
        _logger.LogTrace($"Track end approaching for queue ID {queueId}");

        var queue = await _queueRepository.GetAll();

        var entry = queue.FirstOrDefault(e => e.QueueId == queueId);
        
        var nextEntry = queue
            .OrderBy(e => e.Order)
            .FirstOrDefault(e => e.Order > 0);

        _logger.LogTrace($"Next entry user ID [{nextEntry?.UserId}]; current entry user ID [{entry?.UserId}]");
        
        if (nextEntry == null) return;
        // if (entry?.UserId == nextEntry.UserId) return;

        var pushSub = await _userStateService.GetUserPushSubscription(nextEntry.UserId);

        _logger.LogTrace($"Push sub for [{nextEntry.UserId}]: {pushSub?.Endpoint}");

        if (pushSub == null) return;
        
        _logger.LogDebug($"Notifying subscribers for queue ID {nextEntry.QueueId} ({nextEntry.UserId})");

        var webPushClient = new WebPushClient();
        try
        {
            // TODO: Find a more efficient way to do this
            var pushQueue = await _queueService.Get();
            var pushEntry = pushQueue.First(e => e.QueueId == nextEntry.QueueId);

            var payload = JsonSerializer.Serialize(pushEntry, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            await webPushClient.SendNotificationAsync(pushSub, payload, _vapidDetails);
        }
        catch (WebPushException exception)
        {
            _logger.LogWarning("Error sending web push", exception);
        }
    }
    
    private async void OnConfigurationChange(WebPushConfiguration configuration)
    {
        _vapidDetails = new VapidDetails(configuration.Subject, configuration.PublicKey, configuration.PrivateKey);
    }
}