// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using KaraokeMaster.Services.Reaction;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Application;

public class ReactionService : IReactionService
{
    private readonly ILogger<ReactionService> _logger;
    private readonly Subject<ReactionDto> _subject;
    private readonly IObservable<ReactionDto> _stream;
        
    public ReactionService(ILoggerFactory loggerFactory)
    {
        _logger = loggerFactory.CreateLogger<ReactionService>();
            
        _subject = new Subject<ReactionDto>();
        var c = _subject.Publish();
        c.Connect();

        _stream = Observable.Create<ReactionDto>(observer =>
        {
            var subscription = c.Subscribe(observer);

            return subscription;
        });
    }

    public IDisposable Subscribe(IObserver<ReactionDto> observer)
    {
        return _stream.Subscribe(observer);
    }

    public async Task Add(ReactionDto reaction)
    {
        _subject.OnNext(reaction);

        _logger.LogTrace("Reaction: {Reaction}", reaction.Reaction);
    }

    protected void Error(Exception ex)
    {
        _subject.OnError(ex);
    }    
}