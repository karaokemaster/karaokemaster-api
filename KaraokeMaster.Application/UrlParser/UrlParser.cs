// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace KaraokeMaster.Application
{
    public static partial class UrlParser
    {
        // Based on https://github.com/radiovisual/get-video-id
        public static (string source, string videoId) GetVideoId(string url)
        {
            var video = url;
            string videoId = null;
            string service = null;

            if (Regex.IsMatch(url, @"<iframe", RegexOptions.IgnoreCase))
            {
                video = GetSrc(url);
                if (video == null) return (null, null);
            }

            video = video.Trim();

            video = Regex.Replace(video, @"-nocookie", "", RegexOptions.IgnoreCase);
            video = Regex.Replace(video, @"\/www.", "/", RegexOptions.IgnoreCase);

            // Try to handle google redirection uri
            if (Regex.IsMatch(video, @"\/\/google"))
            {
                // Find the redirection uri
                var match = Regex.Match(video, "url=([^&]+)&");

                // Decode the found uri and replace current url string - continue with final link
                if (match.Success)
                {
                    video = Uri.UnescapeDataString(match.Value);
                }
            }

            if (Regex.IsMatch(video, @"youtube|youtu\.be|y2u\.be|i.ytimg\.", RegexOptions.IgnoreCase))
            {
                service = "youtube";
                videoId = YouTubeUrl(video);
            }
            else if (Regex.IsMatch(video, @"vimeo", RegexOptions.IgnoreCase))
            {
                service = "vimeo";
                videoId = VimeoUrl(video);
            } 
            else if (Regex.IsMatch(video, @"vine", RegexOptions.IgnoreCase))
            {
                service = "vine";
                videoId = VineUrl(video);
            }
            else if (Regex.IsMatch(video, @"videopress", RegexOptions.IgnoreCase))
            {
                service = "videopress";
                videoId = VideoPressUrl(video);
            } 
            else if (Regex.IsMatch(video, @"microsoftstream", RegexOptions.IgnoreCase))
            {
                service = "microsoftstream";
                videoId = MicrosoftStreamUrl(video);
            }
            
            return (service, videoId);
        }
        
        private static string GetSrc(string input)
        {
            var re = @"src=""(.*?)""";
            var url = Regex.Matches(input, re);

            if (url.Count >= 2) {
                return url[1].Value;
            }

            return null;
        }

        private static string StripParameters(string url)
        {
            // Split parameters or split folder separator
            if (url.IndexOf('?') > -1) {
                return url.Split('?')[0];
            } if (url.IndexOf('/') > -1) {
                return url.Split('/')[0];
            } if (url.IndexOf('&') > -1) {
                return url.Split('&')[0];
            }
            return url;
        }        
    }
}