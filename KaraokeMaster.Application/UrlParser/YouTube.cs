// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Text.RegularExpressions;

namespace KaraokeMaster.Application
{
    public static partial class UrlParser
    {
        private static string YouTubeUrl(string url)
        {
            var video = url;

            // remove time hash at the end of the string
            video = Regex.Replace(video, @"#t=.*$", "", RegexOptions.IgnoreCase);

            // shortcode
            const string shortcode = @"youtube:\/\/|https?:\/\/youtu\.be\/|http:\/\/y2u\.be\/";
            if (Regex.IsMatch(video, shortcode))
            {
                var shortcodeId = Regex.Split(video, shortcode)[1];
                return StripParameters(shortcodeId);
            }

            // /v/ or /vi/
            const string inlineV = @"\/v\/|\/vi\/";
            if (Regex.IsMatch(video, inlineV))
            {
                var inlineId = Regex.Split(video, inlineV)[1];
                return StripParameters(inlineId);
            }

            // v= or vi=
            const string parameterV = @"v=|vi=";
            if (Regex.IsMatch(video, parameterV))
            {
                var arr = Regex.Split(video, parameterV);
                return StripParameters(arr[1].Split('&')[0]);
            }

            // v= or vi=
            const string parameterWebP = @"\/an_webp\/";
            if (Regex.IsMatch(video, parameterWebP))
            {
                var webp = Regex.Split(video, parameterWebP)[1];
                return StripParameters(webp);
            }

            // embed
            const string embed = @"\/embed\/";
            if (Regex.IsMatch(video, embed))
            {
                var embedId = Regex.Split(video, embed)[1];
                return StripParameters(embedId);
            }

            // ignore /user/username pattern
            const string username = @"\/user\/([a-zA-Z0-9]*)$";
            if (Regex.IsMatch(video, username))
                return null;

            // user
            const string user = @"\/user\/(?!.*videos)";
            if (Regex.IsMatch(video, user))
            {
                var elements = Regex.Split(video, user);
                return StripParameters(elements.Last());
            }

            // attribution_link
            const string attr = @"\/attribution_link\?.*v%3D([^%&]*)(%26|&|$)";
            if (Regex.IsMatch(video, attr))
            {
                return StripParameters(Regex.Match(video, attr).Value);
            }

            return null;
        }
    }
}