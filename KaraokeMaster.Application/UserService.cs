// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Domain.Models.Configuration;
using KaraokeMaster.Services;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Application
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IUserRepository _userRepository;
        private readonly IUserVideoOptionsRepository _userVideoOptionsRepository;
        private readonly IOptionsMonitor<FontAwesomeConfiguration> _fontAwesomeConfiguration;
        private readonly MemoryCacheEntryOptions _cacheOptions;

        public UserService(ILoggerFactory loggerFactory, IMemoryCache memoryCache, IUserRepository userRepository, IUserVideoOptionsRepository userVideoOptionsRepository, IOptionsMonitor<FontAwesomeConfiguration> fontAwesomeConfiguration)
        {
            _logger = loggerFactory.CreateLogger<UserService>();
            _memoryCache = memoryCache;
            _userRepository = userRepository;
            _userVideoOptionsRepository = userVideoOptionsRepository;
            _fontAwesomeConfiguration = fontAwesomeConfiguration;

            _cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10));
        }

        public async Task<User> EnsureUserExists(string username)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.EnsureUserExists");
            span?.SetExtra(nameof(username), username);

            using (_logger.BeginScope("EnsureUserExists(\"{Name}\")", username))
            {
                var user = await _userRepository.Get(username);
                if (user == null)
                {
                    _logger.LogInformation("User not found; creating new record");
                    
                    user = new User {Username = username};

                    await _userRepository.AddOrUpdate(user);

                    user = await _userRepository.Get(username);
                }

                _logger.LogDebug("Returning user ID {UserId}", user.UserId);
                
                span?.Finish();

                return user;
            }
        }

        public async Task<User> GetUserById(int userId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.GetUserById");
            span?.SetExtra(nameof(userId), userId);

            using (_logger.BeginScope("GetUserById({UserId})", userId))
            {
                var user = await _userRepository.Get(userId);
                if (user == null)
                {
                    _logger.LogInformation("User not found");
                    
                    span?.Finish(SpanStatus.NotFound);

                    return null;
                }

                _logger.LogDebug("Returning user ID {UserId}", user.UserId);
                
                span?.Finish();

                return user;
            }
        }

        public async Task<User> UpdateUser(User user)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.UpdateUser");
            span?.SetExtra(nameof(user), user);

            using (_logger.BeginScope("UpdateUser({UserId})", user.UserId))
            {
                var dbUser = await _userRepository.Get(user.UserId);
                if (dbUser == null)
                    dbUser = await _userRepository.Get(user.Username);
                
                if (dbUser == null)
                {
                    _logger.LogWarning("User not found!");
                    span?.Finish(SpanStatus.NotFound);

                    return null;
                }

                dbUser.Icon = user.Icon;
                dbUser.Color = user.Color;

                await _userRepository.AddOrUpdate(dbUser);
                
                dbUser = await _userRepository.Get(dbUser.UserId);
                
                span?.Finish();

                return dbUser;
            }
        }

        public async Task<User> ChangeUsername(User user, string username)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.ChangeUsername");
            span?.SetExtra(nameof(user), user);
            span?.SetExtra(nameof(username), username);

            using (_logger.BeginScope("ChangeUsername({UserId}, \"{Username}\")", user.UserId, username))
            {
                var dbUser = await _userRepository.Get(user.UserId);
                if (dbUser == null)
                    dbUser = await _userRepository.Get(user.Username);
                
                if (dbUser == null)
                {
                    _logger.LogWarning("User not found!");
                    span?.Finish(SpanStatus.NotFound);

                    return null;
                }

                var newUser = await _userRepository.Get(username);
                if (newUser != null)
                {
                    _logger.LogWarning("User with username {Username} already exists!", username);
                    span?.Finish(SpanStatus.AlreadyExists);

                    return null;
                }
                
                dbUser.Username = username.Trim();

                await _userRepository.AddOrUpdate(dbUser);
                
                dbUser = await _userRepository.Get(dbUser.UserId);
                
                span?.Finish();

                return dbUser;
            }
        }
        
        public async Task<UserVideoOptions> GetVideoOptions(User user, Video video)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.GetVideoOptions");
            span?.SetExtra(nameof(user), user);
            span?.SetExtra(nameof(video), video);

            using (_logger.BeginScope("GetVideoOptions({UserId}, {VideoId})", user.UserId, video.VideoId))
            {
                var options = await _userVideoOptionsRepository.Get(user.UserId, video.VideoId);
                
                if (options == null)
                    _logger.LogDebug("User options not found for video");

                span?.Finish();
                
                return options;
            }
        }

        public async Task<List<UserVideoOptions>> GetVideoOptions(User user)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.GetVideoOptions");
            span?.SetExtra(nameof(user), user);

            using (_logger.BeginScope("GetVideoOptions({UserId})", user.UserId))
            {
                var options = await _userVideoOptionsRepository.GetAll(user.UserId);
                    
                _logger.LogDebug("Found {Count} video options", options.Count);

                span?.Finish();

                return options;
            }
        }

        public async Task<List<SearchResultDto>> GetRatedVideos(string username)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.GetRatedVideos");
            span?.SetExtra(nameof(username), username);

            using (_logger.BeginScope("GetRatedVideos(\"{Username}\")", username))
            {
                var user = await _userRepository.Get(username);
                var options = await _userVideoOptionsRepository.GetAll(user.UserId);
                
                _logger.LogDebug("Found {Count} video options", options.Count);

                var rated = options
                    .Where(o => o.Good || o.Bad)
                    .Select(
                        o => new SearchResultDto
                        {
                            Title = o.Video.Title,
                            VideoId = o.Video.YouTubeVideoId,
                            ThumbnailUrl = o.Video.ThumbnailUrl,
                            HasLyrics = !string.IsNullOrWhiteSpace(o.Video.Options?.Lyrics),
                            Rating = (o.Good ? 1 : 0) + (o.Bad ? -1 : 0),
                            PlayCount = o.Video.PlayCount,
                            LastPlayed = o.Video.LastPlayed,
                            // TODO: Duration
                        })
                    .OrderByDescending(v => v.Rating)
                    .ThenByDescending(v => v.LastPlayed)
                    .ToList();
                
                span?.Finish();

                return rated;
            }
        }

        public async Task<List<User>> GetActive()
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.GetActive");

            using (_logger.BeginScope("GetActive"))
            {
                var users = await _userRepository.GetActive();
                
                span?.Finish();

                return users;
            }
        }

        public async Task<List<User>> GetAll()
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserService.GetAll");

            using (_logger.BeginScope("GetAll"))
            {
                var users = await _userRepository.GetAll();
                
                span?.Finish();

                return users;
            }
        }

        public async Task<List<string>> SearchIcons(string query)
        {
            var client = new GraphQLHttpClient("https://api.fontawesome.com", new NewtonsoftJsonSerializer());
            client.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetFaApiToken());
            
            var request = new GraphQLRequest {
                Query = @"query Search($version: String!, $query: String!) { search(version: $version, query: $query, first: 15) { id, label, styles } }",
                OperationName = "Search",
                Variables = new {
                    query,
                    version = _fontAwesomeConfiguration.CurrentValue.IconVersion,
                }
            };

            var response = await client.SendQueryAsync<FaSearchResult>(request);

            var result = response.Data.Search
                .Select(i => new
                {
                    icon = JsonConvert.SerializeObject(new {
                        prefix = i.Styles.Contains("duotone") ? "fad" : "far",
                        iconName = i.Id,
                    }),
                    label = i.Label,
                })
                .Select(JsonConvert.SerializeObject)
                .ToList();

            return result;
        }

        private async Task<string> GetFaApiToken()
        {
            if (_memoryCache.TryGetValue("FaApiToken", out string token))
                return token;
            
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _fontAwesomeConfiguration.CurrentValue.ApiKey);
            var response = await client.PostAsync("https://api.fontawesome.com/token", null);

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<FaTokenResult>(responseString);
            token = result.AccessToken;

            _memoryCache.Set("FaApiToken", token, TimeSpan.FromSeconds(result.ExpiresIn - 60));

            return token;
        }

        private class FaTokenResult
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }
            
            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }
        }

        private class FaSearchResult
        {
            public FaIconResult[] Search { get; set; }
            
            public class FaIconResult
            {
                public string Id { get; set; }
                public string Label { get; set; }
                public string[] Styles { get; set; }
            }
        }
    }
}