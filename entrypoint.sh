#!/bin/sh

export $(cat /usr/src/karaokemaster-api/build.env | xargs)
dotnet /usr/src/karaokemaster-api/KaraokeMaster.Api.dll
