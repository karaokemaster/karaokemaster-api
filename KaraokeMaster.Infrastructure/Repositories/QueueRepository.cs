// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Infrastructure.Repositories
{
    public class QueueRepository : IQueueRepository
    {
        private readonly ILogger<QueueRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqliteContext>> _contextFactory;

        public QueueRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqliteContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<QueueRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task Add(QueueEntry entry)
        {
            _logger.LogInformation($"Adding video: {entry.VideoId}");

            // Setting this to int.MaxValue ensure it'll be the last item after we call SortQueue
            entry.Order = int.MaxValue;
            
            using var context = _contextFactory().Value;

            await context.QueueEntries.AddAsync(entry);
            await context.SaveChangesAsync();

            await SortQueue();
        }

        public async Task Update(QueueEntry entry)
        {
            _logger.LogInformation($"Updating entry: {entry.QueueId}");

            using var context = _contextFactory().Value;

            var queueEntry = await context.QueueEntries
                .SingleOrDefaultAsync(q => q.QueueId == entry.QueueId);

            if (queueEntry == null) return;

            // TODO: Map changes from the supplied param instead
            queueEntry.Order = entry.Order;
            queueEntry.PlaybackDate = entry.PlaybackDate;
            
            context.QueueEntries.Update(queueEntry);
            await context.SaveChangesAsync();

            await SortQueue();
        }

        public async Task Delete(int queueId)
        {
            _logger.LogInformation($"Removing entry: {queueId}");

            using var context = _contextFactory().Value;

            var queueEntry = await context.QueueEntries
                .SingleOrDefaultAsync(q => q.QueueId == queueId);

            if (queueEntry == null) return;

            context.QueueEntries.Remove(queueEntry);
            await context.SaveChangesAsync();

            await SortQueue();
        }

        public async Task Delete(QueueEntry entry)
        {
            await Delete(entry.QueueId);
        }

        public async Task<QueueEntry> Get(int queueId)
        {
            _logger.LogInformation($"Get entry: {queueId}");

            using var context = _contextFactory().Value;

            var queueEntry = await context.QueueEntries
                .SingleOrDefaultAsync(q => q.QueueId == queueId);

            return queueEntry;
        }

        public async Task<List<QueueEntry>> GetAll(bool includePlayed = false)
        {
            _logger.LogInformation($"All videos from database: {_configuration["ConnectionStrings:SqliteConnection"]} (includePlayed = {includePlayed})");

            using var context = _contextFactory().Value;

            var videos = await context.QueueEntries
                .Include(q => q.Video)
                .Where(q => includePlayed || q.Order >= 0)
                .OrderBy(q => q.Order)
                .ToListAsync();

            return videos;
        }

        private async Task SortQueue()
        {
            // TOOD: Generate column list from entity, if possible
            var sql = @"
                DROP TABLE IF EXISTS temp_QueueOrder;

                CREATE TABLE temp_QueueOrder (QueueId INTEGER);

                INSERT INTO temp_QueueOrder
                SELECT [QueueId]
                FROM Queue
                WHERE [Order] > 0
                ORDER BY [Order], [QueueId];

                REPLACE INTO Queue
                (QueueId, VideoId, [Order], [User])
                SELECT Queue.QueueId, Queue.VideoId, temp_QueueOrder.[rowid], Queue.[User]
                FROM Queue
                INNER JOIN temp_QueueOrder ON temp_QueueOrder.QueueId = Queue.QueueId;

                DROP TABLE temp_QueueOrder;
            ";

            using var context = _contextFactory().Value;

            await context.Database.ExecuteSqlCommandAsync(sql);
        }
    }
}