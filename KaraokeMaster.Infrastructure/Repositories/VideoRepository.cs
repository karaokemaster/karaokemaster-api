// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Infrastructure.Repositories
{
    public class VideoRepository : IVideoRepository
    {
        private readonly ILogger<VideoRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqliteContext>> _contextFactory;

        
        public VideoRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqliteContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<VideoRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task AddOrUpdate(Video video)
        {
            _logger.LogInformation($"AddOrUpdate video: {video.YouTubeVideoId}");

            try
            {
                using var context = _contextFactory().Value;
                
                if (await context.Videos.AnyAsync(v => v.YouTubeVideoId == video.YouTubeVideoId))
                {
                    context.Videos.Update(video);
                }
                else
                {
                    await context.Videos.AddAsync(video);
                }

                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddOrUpdate");
                throw;
            }
        }

        public async Task<Video> Get(string videoId)
        {
            _logger.LogInformation($"Get video: {videoId}");

            try
            {
                using var context = _contextFactory().Value;
                
                var video = await context.Videos.FirstOrDefaultAsync(v => v.YouTubeVideoId == videoId);

                return video;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in Get");
                throw;
            }
        }

        public async Task<List<Video>> GetAll()
        {
            _logger.LogInformation($"GetAll");

            try
            {
                using var context = _contextFactory().Value;
                
                var videos = await context.Videos.ToListAsync();

                return videos;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in GetAll");
                throw;
            }
        }

        public async Task<List<Video>> GetRecommendationsForUser(string name)
        {
            // TODO: Seems like another great place to get all MemoryCache
            _logger.LogInformation(
                $"Recommended videos from database: {_configuration["ConnectionStrings:SqliteConnection"]}, , for user: {name}");

            var recommendations = new List<Video>();

            // TODO: Reexamine this query in light of the table split
            var sql = @"
                SELECT DISTINCT Q.VideoId, V.Title, V.ThumbnailUrl, V.Lyrics, T.PlaybackDate
                FROM Queue Q
                INNER JOIN Video V
                    ON V.VideoId = Q.VideoId
                INNER JOIN (
                    SELECT VideoId, MAX(PlaybackDate) AS PlaybackDate
                    FROM Queue
                    WHERE User = @Name
                        AND VideoId NOT IN (
                            SELECT DISTINCT VideoId
                            FROM Queue
                            WHERE [Order] > -1
                                OR PlaybackDate > DateTime('Now', 'LocalTime', '-12 Hour')
                        )
                    GROUP BY VideoId
                    ORDER BY COUNT(QueueId) DESC
                    LIMIT 10
                ) T
                    ON T.VideoId = Q.VideoId
                ORDER BY T.PlaybackDate DESC
            ";

            using var context = _contextFactory().Value;
            using var connection = context.Database.GetDbConnection();
            
            var command = connection.CreateCommand();
            command.CommandText = sql;
            command.CommandType = System.Data.CommandType.Text;

            var param = command.CreateParameter();
            param.ParameterName = "Name";
            param.Value = name;
            command.Parameters.Add(param);

            await connection.OpenAsync();

            var reader = await command.ExecuteReaderAsync();
            while (await reader.ReadAsync())
            {
                var video = new Video
                {
                    Title = reader["Title"].ToString(),
                    YouTubeVideoId = reader["VideoId"].ToString(),
                    ThumbnailUrl = reader["ThumbnailUrl"].ToString(),
                    Lyrics = reader["Lyrics"].ToString()
                    // PlaybackDate = DateTime.Parse(reader["PlaybackDate"].ToString()),
                };
                recommendations.Add(video);
            }

            return recommendations;
        }
    }
}