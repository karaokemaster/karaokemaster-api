// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Autofac;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.Data;
using KaraokeMaster.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace KaraokeMaster.Infrastructure
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // TODO: Reconsider the lifetime if we're going to allow config changes?
            builder.Register(x =>
            {
                var config = x.Resolve<IConfiguration>();
                var optionsBuilder = new DbContextOptionsBuilder<SqliteContext>()
                    // TODO: Use strongly-typed configuration object instead
                    .UseSqlite(config.GetConnectionString("SqliteConnection"));

                var context = new SqliteContext(optionsBuilder.Options);
                context.Database.Migrate();

                return optionsBuilder;
            })
                .SingleInstance();

            // TODO: Connection pooling
            builder.Register(x =>
            {
                var optionsBuilder = x.Resolve<DbContextOptionsBuilder<SqliteContext>>();
                var context = new SqliteContext(optionsBuilder.Options);

                return context;
            });

            
            builder.RegisterType<QueueRepository>().As<IQueueRepository>();
            builder.RegisterType<VideoRepository>().As<IVideoRepository>();
            
            base.Load(builder);
        }
    }
}