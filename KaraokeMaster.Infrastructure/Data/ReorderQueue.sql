/*
    
    Copyright 2019 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

CREATE TABLE temp_QueueOrder (QueueId INTEGER);

INSERT INTO temp_QueueOrder
SELECT [QueueId]
FROM Queue
WHERE [Order] > 0
ORDER BY [Order], [QueueId];

REPLACE INTO Queue
(QueueId, Title, Thumbnailurl, [Order], [User], Good, Bad)
SELECT Queue.QueueId, Queue.Title, Queue.ThumbnailUrl, temp_QueueOrder.[rowid], Queue.[User], Queue.Good, Queue.Bad
FROM Queue
INNER JOIN temp_QueueOrder ON temp_QueueOrder.QueueId = Queue.QueueId;

DROP TABLE temp_QueueOrder;
