// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.Data.Migrations
{
    [DbContext(typeof(SqliteContext))]
    [Migration("20190318-DropColumn_Queue_eTag")]
    public class DropColumn_Queue_eTag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                PRAGMA foreign_keys = 0;

                CREATE TABLE Queue_temp AS SELECT *
                                            FROM Queue;

                DROP TABLE Queue;

                CREATE TABLE Queue ( 
                    [QueueId] INTEGER NOT NULL CONSTRAINT PK_Queue PRIMARY KEY AUTOINCREMENT, 
                    [VideoId] varchar(64) NOT NULL, 
                    [Title] varchar(512) NOT NULL, 
                    [ThumbnailUrl] varchar(512) NOT NULL, 
                    [Order] INTEGER NOT NULL, 
                    [User] varchar(512) NULL, 
                    [Good] INTEGER NOT NULL, 
                    [Bad] INTEGER NOT NULL, 
                    [PlaybackDate] TEXT NULL
                );

                INSERT INTO Queue 
                (
                    [QueueId],
                    [VideoId],
                    [Title],
                    [ThumbnailUrl],
                    [Order],
                    [User],
                    [Good],
                    [Bad],
                    [PlaybackDate]
                )
                SELECT 
                    [QueueId],
                    [VideoId],
                    [Title],
                    [ThumbnailUrl],
                    [Order],
                    [User],
                    [Good],
                    [Bad],
                    [PlaybackDate]
                FROM Queue_temp;

                DROP TABLE Queue_temp;

                PRAGMA foreign_keys = 1;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
