// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.Data.Migrations
{
    [DbContext(typeof(SqliteContext))]
    [Migration("20200601_AddTable_Video")]
    public class AddTable_Video : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Video",
                columns: table => new {
                    VideoId = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    ThumbnailUrl = table.Column<string>(nullable: false),
                    Lyrics = table.Column<string>(nullable: true),
                },
                constraints: table => {
                    table.PrimaryKey("PK_VideoId", c => c.VideoId);
                }
            );
            
            migrationBuilder.Sql(@"
                INSERT INTO Video
                SELECT
                    v.VideoId,
                    (SELECT Title FROM Queue WHERE VideoId = v.VideoId ORDER BY PlaybackDate DESC LIMIT 1) AS Title,
                    (SELECT ThumbnailUrl FROM Queue WHERE VideoId = v.VideoId ORDER BY PlaybackDate DESC LIMIT 1) AS ThumbnailUrl,
                    (SELECT Lyrics FROM Queue WHERE VideoId = v.VideoId ORDER BY PlaybackDate DESC LIMIT 1) AS Lyrics
                FROM (
                    SELECT DISTINCT VideoId
                    FROM Queue
                ) AS v
                ;
            ");

            migrationBuilder.Sql(@"
                PRAGMA foreign_keys = 0;

                CREATE TABLE Queue_temp AS SELECT *
                                            FROM Queue;

                DROP TABLE Queue;

                CREATE TABLE Queue ( 
                    [QueueId] INTEGER NOT NULL CONSTRAINT PK_Queue PRIMARY KEY AUTOINCREMENT, 
                    [VideoId] varchar(64) NOT NULL, 
                    [Order] INTEGER NOT NULL, 
                    [User] varchar(512) NULL, 
                    [PlaybackDate] TEXT NULL
                );

                INSERT INTO Queue 
                (
                    [QueueId],
                    [VideoId],
                    [Order],
                    [User],
                    [PlaybackDate]
                )
                SELECT 
                    [QueueId],
                    [VideoId],
                    [Order],
                    [User],
                    [PlaybackDate]
                FROM Queue_temp;

                DROP TABLE Queue_temp;

                PRAGMA foreign_keys = 1;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Queue",
                nullable: false
            );
            
            migrationBuilder.AddColumn<string>(
                name: "ThumbnailUrl",
                table: "Queue",
                nullable: false
            );
            
            migrationBuilder.AddColumn<bool>(
                name: "Good",
                table: "Queue",
                nullable: false
            );
            
            migrationBuilder.AddColumn<bool>(
                name: "Bad",
                table: "Queue",
                nullable: false
            );
            
            migrationBuilder.AddColumn<string>(
                name: "Lyrics",
                table: "Queue",
                nullable: true
            );

            migrationBuilder.Sql(@"
                UPDATE Queue
                SET Title = (SELECT Title FROM Video WHERE Video.VideoId = Queue.VideoId),
                    ThumbnailUrl = (SELECT ThumbnailUrl FROM Video WHERE Video.VideoId = Queue.VideoId),
                    Lyrics = (SELECT Lyrics FROM Video WHERE Video.VideoId = Queue.VideoId)
                WHERE EXISTS (SELECT Title FROM Video WHERE Video.VideoId = Queue.VideoId)
                ;
            ");

            migrationBuilder.DropTable("Video");
        }
    }
}