// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using KaraokeMaster.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace KaraokeMaster.Infrastructure.Data
{
    public class SqliteContext : DbContext
    {
        public SqliteContext(DbContextOptions<SqliteContext> options)
            : base(options)
        { }

        public DbSet<QueueEntry> QueueEntries { get; set; }

        public DbSet<Video> Videos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<QueueEntry>(entry =>
            {
                entry.ToTable("Queue");

                entry
                    .Property(q => q.VideoId)
                    .HasColumnType("varchar(64)");

                entry
                    .Property(q => q.User)
                    .HasColumnType("varchar(512)");

                entry
                    .HasOne(q => q.Video)
                    .WithMany()
                    .HasForeignKey(q => q.VideoId)
                    .IsRequired();
            });

            builder.Entity<QueueEntry>().UsePropertyAccessMode(PropertyAccessMode.Property);
            
            builder.Entity<Video>(video =>
            {
                video.ToTable("Video");

                video
                    .Property(v => v.YouTubeVideoId)
                    .HasColumnType("varchar(64)");
            });
        }
    }

    public static class SqliteContextFactory
    {
        public static SqliteContext Create(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SqliteContext>();
            optionsBuilder.UseSqlite(connectionString);

            var context = new SqliteContext(optionsBuilder.Options);
            context.Database.Migrate();

            return context;
        }
    }
}