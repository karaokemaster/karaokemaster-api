FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env
# Rider's Docker support doesn't like the buildplatform argument, so we need a separate Dockerfile

# Create app directory
WORKDIR /usr/src/karaokemaster-api

COPY . ./

# dotnet publish likes to grab the wrong version of dependencies sometimes
# running it a second time with a specific project fixes that (and still grabs everything else)
# https://github.com/dotnet/sdk/issues/3886
RUN dotnet publish -f net6.0 -c Release -o out
RUN dotnet publish KaraokeMaster.Api -f net6.0 -c Release -o out




# Runtime

FROM mcr.microsoft.com/dotnet/aspnet:7.0

# Create app directory
WORKDIR /usr/src/karaokemaster-api

# Copy assets from the build environment
COPY --from=build-env /usr/src/karaokemaster-api/out ./

ENV COMPlus_DbgEnableMiniDump=1

ENTRYPOINT [ "dotnet", "KaraokeMaster.Api.dll" ]