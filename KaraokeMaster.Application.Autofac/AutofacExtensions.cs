// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace KaraokeMaster.Application.Autofac
{
    public static class AutofacExtensions
    {
        public static void RegisterConfig<T>(this ContainerBuilder builder, string key)
            where T : class
        {
            builder.Register(ctx =>
                {
                    var config = ctx.Resolve<IConfiguration>();
                    var configSection = config.GetSection(key);
                    return new ConfigurationChangeTokenSource<T>(Options.DefaultName, configSection);
                })
                .As<IOptionsChangeTokenSource<T>>()
                .SingleInstance();

            builder.Register(ctx =>
                {
                    var config = ctx.Resolve<IConfiguration>();
                    var configSection = config.GetSection(key);
                    return new NamedConfigureFromConfigurationOptions<T>(Options.DefaultName, configSection, _ => { });
                })
                .As<IConfigureOptions<T>>()
                .SingleInstance();
        }
        
        public static void RegisterConfig<T>(this ContainerBuilder builder, Action<T, IComponentContext> setupAction)
            where T : class
        {
            builder.Register(ctx =>
                {
                    return new ConfigureNamedOptions<T>(Options.DefaultName, obj => setupAction(obj, ctx));
                })
                .As<IConfigureOptions<T>>()
                .SingleInstance();
        }
    }
}