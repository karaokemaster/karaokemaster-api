// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations;

[DbContext(typeof(SqlContext))]
[Migration("20221220_1_CreateTable_Kiosks")]
public class CreateTable_Kiosks : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "Kiosks",
            columns: table => new
            {
                KioskId = table.Column<int>(nullable: false).Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                Name = table.Column<string>(nullable: false, maxLength: 256, unicode: true),
                PublicKey = table.Column<byte>(nullable: false, type: "binary(32)"),
                Adopted = table.Column<bool>(nullable: false),
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_VideoOptions", c => c.KioskId);
            }
        );
    }
        
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable("Kiosks");
    }
}