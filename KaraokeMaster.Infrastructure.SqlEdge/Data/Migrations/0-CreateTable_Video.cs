// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("0_CreateTable_Video")]
    public class CreateTable_Video : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Video",
                columns: table => new
                {
                    VideoId = table.Column<int>().Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    YouTubeVideoId = table.Column<string>(nullable: false, maxLength: 64, unicode: false),
                    Title = table.Column<string>(nullable: false, maxLength: 256, unicode: true),
                    ThumbnailUrl = table.Column<string>(nullable: false, maxLength: 512, unicode: true),
                    Lyrics = table.Column<string>(nullable: true, unicode: true),
                },
                constraints: table => { table.PrimaryKey("PK_VideoId", c => c.VideoId); }
            );
        }
        
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("User");
        }
    }
}