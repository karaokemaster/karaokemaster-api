// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations;

[DbContext(typeof(SqlContext))]
[Migration("20221220_2_CreateTable_KioskDisplays")]
public class CreateTable_KioskDisplays : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "KioskDisplays",
            columns: table => new
            {
                KioskId = table.Column<int>(nullable: false),
                DisplayId = table.Column<int>(nullable: false),
                DisplayUrl = table.Column<string>(nullable: true, maxLength: 512, unicode: false),
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_KioskDisplays", c => new { c.KioskId, c.DisplayId });
                table.ForeignKey("FK_KioskDisplays_KioskId", c => c.KioskId, "Kiosks", "KioskId");
            }
        );
    }
        
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable("KioskDisplays");
    }
}