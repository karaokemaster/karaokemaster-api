// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210429_CreateIndex_YouTubeVideoId")]
    public class CreateIndex_YouTubeVideoId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE QueueEntries
SET VideoId = NewVideoId
FROM (
        SELECT V.VideoId AS OldVideoId, DV.VideoId AS NewVideoId
        FROM Videos V
        INNER JOIN (
            SELECT YouTubeVideoId, MIN(VideoId) AS VideoId
            FROM Videos
            GROUP BY YouTubeVideoId
            HAVING COUNT(VideoId) > 1
        ) DV
            ON V.YouTubeVideoId = DV.YouTubeVideoId
        WHERE V.VideoId != DV.VideoId
     ) VV
WHERE VideoId = OldVideoId

UPDATE VideoRecommendations
SET VideoId = NewVideoId
FROM (
        SELECT V.VideoId AS OldVideoId, DV.VideoId AS NewVideoId
        FROM Videos V
        INNER JOIN (
            SELECT YouTubeVideoId, MIN(VideoId) AS VideoId
            FROM Videos
            GROUP BY YouTubeVideoId
            HAVING COUNT(VideoId) > 1
        ) DV
            ON V.YouTubeVideoId = DV.YouTubeVideoId
        WHERE V.VideoId != DV.VideoId
    ) VV
WHERE VideoId = OldVideoId
    AND NOT EXISTS (
        SELECT *
        FROM VideoRecommendations VR
        WHERE VR.VideoId = NewVideoId AND VR.RecommendedVideoId = VideoRecommendations.RecommendedVideoId
    )

INSERT INTO VideoRecommendations (VideoId, RecommendedVideoId, Source, LastUpdated)
SELECT VV.VideoId, VV.RecommendedVideoId, MAX(VRS.Source) AS Source, MAX(VRS.LastUpdated) AS LastUpdated
FROM (
        SELECT DISTINCT VR.VideoId, DV.VideoId AS RecommendedVideoId
        FROM VideoRecommendations VR
        INNER JOIN Videos V
            ON VR.RecommendedVideoId = V.VideoId
        INNER JOIN (
            SELECT YouTubeVideoId, MIN(VideoId) AS VideoId
            FROM Videos
            GROUP BY YouTubeVideoId
            HAVING COUNT(VideoId) > 1
        ) DV
            ON V.YouTubeVideoId = DV.YouTubeVideoId
        WHERE V.VideoId != DV.VideoId
        AND NOT EXISTS (
            SELECT *
            FROM VideoRecommendations R
            WHERE R.VideoId = VR.VideoId AND R.RecommendedVideoId = DV.VideoId
        )
    ) VV
INNER JOIN VideoRecommendations VRS
    ON VRS.VideoId = VV.VideoId AND VRS.RecommendedVideoId = VV.RecommendedVideoId
GROUP BY VV.VideoId, VV.RecommendedVideoId

DELETE FROM VideoRecommendations
WHERE VideoId IN (
        SELECT V.VideoId
        FROM Videos V
        INNER JOIN (
            SELECT YouTubeVideoId, MIN(VideoId) AS VideoId
            FROM Videos
            GROUP BY YouTubeVideoId
            HAVING COUNT(VideoId) > 1
        ) DV
            ON V.YouTubeVideoId = DV.YouTubeVideoId
        WHERE V.VideoId != DV.VideoId
    )

DELETE FROM VideoRecommendations
WHERE RecommendedVideoId IN (
        SELECT V.VideoId
        FROM Videos V
        INNER JOIN (
            SELECT YouTubeVideoId, MIN(VideoId) AS VideoId
            FROM Videos
            GROUP BY YouTubeVideoId
            HAVING COUNT(VideoId) > 1
        ) DV
            ON V.YouTubeVideoId = DV.YouTubeVideoId
        WHERE V.VideoId != DV.VideoId
    )

DELETE FROM Videos
WHERE VideoId IN (
    SELECT V.VideoId
    FROM Videos V
    INNER JOIN (
        SELECT YouTubeVideoId, MIN(VideoId) AS VideoId
        FROM Videos
        GROUP BY YouTubeVideoId
        HAVING COUNT(VideoId) > 1
    ) DV
        ON V.YouTubeVideoId = DV.YouTubeVideoId
    WHERE V.VideoId != DV.VideoId
)
");
            
            migrationBuilder.CreateIndex(
                "IX_Videos_YouTubeVideoId", 
                "Videos", 
                "YouTubeVideoId",
                unique: true
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex("IX_Videos_YouTubeVideoId", "Videos");
        }
    }
}