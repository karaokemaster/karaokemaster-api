// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("2_CreateTable_Queue")]
    public class CreateTable_Queue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Queue",
                columns: table => new {
                    QueueId = table.Column<int>().Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VideoId = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    PlaybackDate = table.Column<DateTime>(nullable: true),
                    Good = table.Column<bool>(nullable: false, defaultValue: false),
                    Bad  = table.Column<bool>(nullable: false, defaultValue: false),
                },
                constraints: table => {
                    table.PrimaryKey("PK_QueueId", c => c.QueueId);
                    table.ForeignKey("FK_VideoId", c => c.VideoId, "Video", "VideoId");
                    table.ForeignKey("FK_UserId", c => c.UserId, "User", "UserId");
                }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("Queue");
        }
    }
}