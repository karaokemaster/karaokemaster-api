// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210417_CreateTable_VideoRecommendations")]
    public class CreateTable_VideoRecommendations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VideoRecommendations",
                columns: table => new
                {
                    VideoId = table.Column<int>(nullable: false),
                    RecommendedVideoId = table.Column<int>(nullable: false),
                    Source = table.Column<string>(nullable: false, maxLength: 128, unicode: false),
                    LastUpdated = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoRecommendations", c => new { c.VideoId, c.RecommendedVideoId, c.Source });
                    table.ForeignKey("FK_VideoRecommendations_VideoId", c => c.VideoId, "Videos", "VideoId");
                    table.ForeignKey("FK_VideoRecommendations_RecommendedVideoId", c => c.RecommendedVideoId, "Videos", "VideoId");
                }
            );
        }
        
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("VideoRecommendations");
        }        
    }
}