// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210121_AddColumns_Video")]
    public class AddColumns_Video : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LightingCue",
                table: "Video",
                type: "nvarchar(50)",
                nullable: true
            );

            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "Video",
                type: "int",
                defaultValue: -1,
                nullable: false
            );

            migrationBuilder.AddColumn<bool>(
                name: "Unavailable",
                table: "Video",
                type: "bit",
                defaultValue: false,
                nullable: false
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn("LightingCue", "Video");
            migrationBuilder.DropColumn("Duration", "Video");
            migrationBuilder.DropColumn("Unavailable", "Video");
        }
    }
}