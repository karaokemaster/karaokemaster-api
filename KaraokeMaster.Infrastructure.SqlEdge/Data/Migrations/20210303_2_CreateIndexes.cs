// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210303_2_CreateIndexes")]
    public class CreateIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                "FI_QueueEntries_WithPlaybackDate", 
                "QueueEntries", 
                new[] {"QueueId", "PlaybackDate"},
                filter: "PlaybackDate IS NOT NULL"
            );

            migrationBuilder.CreateIndex(
                "IX_QueueEntries_VideoAndUser", 
                "QueueEntries", 
                new[] {"VideoId", "UserId"}
            );

            migrationBuilder.CreateIndex(
                "FI_Videos_WithNotUnavailable", 
                "Videos", 
                "VideoId",
                filter: "Unavailable = 0"
            );

            migrationBuilder.CreateIndex(
                "IX_Users_Username",
                "Users",
                "Username"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex("FI_QueueEntries_WithPlaybackDate", "QueueEntries");
            migrationBuilder.DropIndex("IX_QueueEntries_VideoAndUser", "QueueEntries");
            
            migrationBuilder.DropIndex("FI_Videos_WithNotUnavailable", "Videos");
            
            migrationBuilder.DropIndex("IX_Users_Username", "Users");
        }
    }
}