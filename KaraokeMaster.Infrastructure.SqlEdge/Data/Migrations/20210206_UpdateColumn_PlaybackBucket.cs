// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210206_UpdateColumn_PlaybackBucket")]
    public class UpdateColumn_PlaybackBucket_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP TRIGGER Queue_PlaybackBucket");
            
            migrationBuilder.DropColumn("PlaybackBucket", "Queue");
            
            migrationBuilder.AddColumn<DateTime>(
                name: "PlaybackBucket",
                table: "Queue",
                type: "date",
                computedColumnSql: "CAST(DATE_BUCKET(day, 1, PlaybackDate, CAST('1900-01-01 12:00:00.000' AS datetime2)) AS date)",
                nullable: true
            );
        }
    }
}