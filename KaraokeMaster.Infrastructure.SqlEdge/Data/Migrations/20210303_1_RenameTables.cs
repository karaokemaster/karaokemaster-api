// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210303_1_RenameTables")]
    public class RenameTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey("FK_VideoId", "Queue");
            migrationBuilder.DropForeignKey("FK_UserId", "Queue");

            migrationBuilder.DropForeignKey("FK_UserVideoOptions_UserId", "UserVideoOptions");
            migrationBuilder.DropForeignKey("FK_UserVideoOptions_VideoId", "UserVideoOptions");

            migrationBuilder.DropForeignKey("FK_VideoOptions_VideoId", "VideoOptions");
            
            // User -> Users
            migrationBuilder.DropPrimaryKey("PK_UserId", "User");
            migrationBuilder.RenameTable("User", newName: "Users");
            migrationBuilder.AddPrimaryKey("PK_Users_UserId", "Users", "UserId");

            // Video -> Videos
            migrationBuilder.DropPrimaryKey("PK_VideoId", "Video");
            migrationBuilder.RenameTable("Video", newName: "Videos");
            migrationBuilder.AddPrimaryKey("PK_Videos_VideoId", "Videos", "VideoId");

            // Queue -> QueueEntries
            migrationBuilder.DropPrimaryKey("PK_QueueId", "Queue");
            migrationBuilder.RenameTable("Queue", newName: "QueueEntries");
            migrationBuilder.AddPrimaryKey("PK_QueueEntries_QueueId", "QueueEntries", "QueueId");
            migrationBuilder.AddForeignKey("FK_QueueEntries_Videos_VideoId", "QueueEntries", "VideoId", "Videos", principalColumn: "VideoId");
            migrationBuilder.AddForeignKey("FK_QueueEntries_Users_UserId", "QueueEntries", "UserId", "Users", principalColumn: "UserId");

            // UserVideoOptions indexes
            migrationBuilder.DropPrimaryKey("PK_UserVideoOptions", "UserVideoOptions");
            migrationBuilder.AddPrimaryKey("PK_UserVideoOptions_UserId_VideoId", "UserVideoOptions", new [] { "UserId", "VideoId" });
            migrationBuilder.AddForeignKey("FK_UserVideoOptions_Users_UserId", "UserVideoOptions", "UserId", "Users", principalColumn: "UserId");
            migrationBuilder.AddForeignKey("FK_UserVideoOptions_Videos_VideoId", "UserVideoOptions", "VideoId", "Videos", principalColumn: "VideoId");

            // VideoOptions indexes
            migrationBuilder.DropPrimaryKey("PK_VideoOptions", "VideoOptions");
            migrationBuilder.AddPrimaryKey("PK_VideoOptions_VideoId", "VideoOptions", "VideoId");
            migrationBuilder.AddForeignKey("FK_VideoOptions_Videos_VideoId", "VideoOptions", "VideoId", "Videos", principalColumn: "VideoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        { 
            migrationBuilder.DropForeignKey("FK_QueueEntries_Videos_VideoId", "QueueEntries");
            migrationBuilder.DropForeignKey("FK_QueueEntries_Users_UserId", "QueueEntries");

            migrationBuilder.DropForeignKey("FK_UserVideoOptions_Users_UserId", "UserVideoOptions");
            migrationBuilder.DropForeignKey("FK_UserVideoOptions_Videos_VideoId", "UserVideoOptions");

            migrationBuilder.DropForeignKey("FK_VideoOptions_Videos_VideoId", "VideoOptions");

            // User -> Users
            migrationBuilder.DropPrimaryKey("PK_Users_UserId", "Users");
            migrationBuilder.RenameTable("Users", newName: "User");
            migrationBuilder.AddPrimaryKey("PK_UserId", "User", "UserId");

            // Video -> Videos
            migrationBuilder.DropPrimaryKey("PK_Videos_VideoId", "Videos");
            migrationBuilder.RenameTable("Videos", newName: "Video");
            migrationBuilder.AddPrimaryKey("PK_VideoId", "Video", "VideoId");

            // Queue -> QueueEntries
            migrationBuilder.DropPrimaryKey("PK_QueueEntries_QueueId", "QueueEntries");
            migrationBuilder.RenameTable("QueueEntries", newName: "Queue");
            migrationBuilder.AddPrimaryKey("PK_QueueId", "Queue", "QueueId");
            migrationBuilder.AddForeignKey("FK_VideoId", "Queue", "VideoId", "Video", principalColumn: "VideoId");
            migrationBuilder.AddForeignKey("FK_UserId", "Queue", "UserId", "User", principalColumn: "UserId");

            // UserVideoOptions indexes
            migrationBuilder.DropPrimaryKey("PK_UserVideoOptions_UserId_VideoId", "UserVideoOptions");
            migrationBuilder.AddPrimaryKey("PK_UserVideoOptions", "UserVideoOptions", new [] { "UserId", "VideoId" });
            migrationBuilder.AddForeignKey("FK_UserVideoOptions_UserId", "UserVideoOptions", "UserId", "User", principalColumn: "UserId");
            migrationBuilder.AddForeignKey("FK_UserVideoOptions_VideoId", "UserVideoOptions", "VideoId", "Video", principalColumn: "VideoId");

            // VideoOptions indexes
            migrationBuilder.DropPrimaryKey("PK_VideoOptions_VideoId", "VideoOptions");
            migrationBuilder.AddPrimaryKey("PK_VideoOptions", "VideoOptions", "VideoId");
            migrationBuilder.AddForeignKey("FK_VideoOptions_VideoId", "VideoOptions", "VideoId", "Video", principalColumn: "VideoId");
        }
    }
}