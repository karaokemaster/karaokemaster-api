// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210301_CreateTable_VideoOptions")]
    public class CreateTable_VideoOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VideoOptions",
                columns: table => new
                {
                    VideoId = table.Column<int>(nullable: false),
                    Lyrics = table.Column<string>(nullable: true, type: "nvarchar(max)"),
                    LightingCue = table.Column<string>(nullable: true, type: "nvarchar(50)"),
                    AudioGain = table.Column<double>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoOptions", c => c.VideoId);
                    table.ForeignKey("FK_VideoOptions_VideoId", c => c.VideoId, "Video", "VideoId");
                }
            );

            var sql = @"INSERT INTO VideoOptions (VideoId, Lyrics, LightingCue)
                        SELECT VideoId, Lyrics, LightingCue
                        FROM     Video
                        WHERE Lyrics IS NOT NULL OR LightingCue IS NOT NULL";

            migrationBuilder.Sql(sql);
            
            migrationBuilder.DropColumn("LightingCue", "Video");
            migrationBuilder.DropColumn("Lyrics", "Video");
        }
        
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("VideoOptions");
            
            migrationBuilder.AddColumn<string>(
                name: "LightingCue",
                table: "Video",
                type: "nvarchar(50)",
                nullable: true
            );

            migrationBuilder.AddColumn<string>(
                name: "Lyrics",
                table: "Video",
                type: "nvarchar(max)",
                nullable: true
            );
        }        
    }
}