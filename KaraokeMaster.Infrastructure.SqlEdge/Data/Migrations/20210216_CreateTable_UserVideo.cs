// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20210216_CreateTable_UserVideo")]
    public class CreateTable_UserVideo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserVideoOptions",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    VideoId = table.Column<int>(nullable: false),
                    Good = table.Column<bool>(nullable: false, defaultValue: false),
                    Bad  = table.Column<bool>(nullable: false, defaultValue: false),
                    LightingCue = table.Column<string>(nullable: true, type: "nvarchar(50)"),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserVideoOptions", c => new { c.UserId, c.VideoId });
                    table.ForeignKey("FK_UserVideoOptions_UserId", c => c.UserId, "User", "UserId");
                    table.ForeignKey("FK_UserVideoOptions_VideoId", c => c.VideoId, "Video", "VideoId");
                }
            );
        }
        
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("UserVideoOptions");
        }        
    }
}