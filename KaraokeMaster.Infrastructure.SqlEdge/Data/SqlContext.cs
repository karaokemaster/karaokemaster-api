// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using KaraokeMaster.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace KaraokeMaster.Infrastructure.SqlEdge.Data
{
    public class SqlContext : DbContext
    {
        public SqlContext(DbContextOptions<SqlContext> options)
            : base(options)
        { }

        public DbSet<QueueEntry> QueueEntries { get; set; }

        public DbSet<Video> Videos { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserVideoOptions> UserVideoOptions { get; set; }

        public DbSet<VideoOptions> VideoOptions { get; set; }

        public DbSet<VideoRecommendation> VideoRecommendations { get; set; }

        public DbSet<Kiosk> Kiosks { get; set; }
        
        public DbSet<KioskDisplay> KioskDisplays { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder
                .Entity<QueueEntry>(entry =>
                {
                    entry.ToTable("QueueEntries");

                    entry
                        .Property(q => q.VideoId);

                    entry
                        .Property(q => q.UserId);

                    entry
                        .Property(q => q.PlaybackBucket)
                        .HasComputedColumnSql();
                    
                    entry
                        .HasOne(q => q.Video)
                        .WithMany()
                        .HasForeignKey(q => q.VideoId)
                        .IsRequired();

                    entry
                        .HasOne(q => q.User)
                        .WithMany()
                        .HasForeignKey(q => q.UserId)
                        .IsRequired();

                    entry
                        .HasOne(q => q.UserOptions)
                        .WithMany()
                        .HasForeignKey(q => new { q.UserId, q.VideoId })
                        .IsRequired(false);
                })
                .UsePropertyAccessMode(PropertyAccessMode.Property);
            
            builder
                .Entity<Video>(video =>
                {
                    video.ToTable("Videos");
                    
                    video
                        .HasOne(v => v.Options)
                        .WithOne()
                        .HasForeignKey<VideoOptions>(o => o.VideoId)
                        .IsRequired(false);
                });

            builder
                .Entity<VideoOptions>(video =>
                {
                    video.ToTable("VideoOptions");
                });

            builder
                .Entity<User>(user =>
                {
                    user.ToTable("Users");
                });
            
            builder
                .Entity<UserVideoOptions>(options =>
                {
                    options.ToTable("UserVideoOptions");

                    options
                        .HasKey(o => new {o.UserId, o.VideoId});
                    
                    options
                        .HasOne(o => o.Video)
                        .WithMany()
                        .HasForeignKey(o => o.VideoId)
                        .IsRequired();

                    options
                        .HasOne(o => o.User)
                        .WithMany()
                        .HasForeignKey(o => o.UserId)
                        .IsRequired();
                })
                .UsePropertyAccessMode(PropertyAccessMode.Property);
            
            builder
                .Entity<VideoRecommendation>(options =>
                {
                    options.ToTable("VideoRecommendations");

                    options
                        .HasKey(o => new {o.VideoId, o.RecommendedVideoId});
                    
                    options
                        .HasOne(o => o.Video)
                        .WithMany()
                        .HasForeignKey(o => o.VideoId)
                        .IsRequired();

                    options
                        .HasOne(o => o.RecommendedVideo)
                        .WithMany()
                        .HasForeignKey(o => o.RecommendedVideoId)
                        .IsRequired();
                })
                .UsePropertyAccessMode(PropertyAccessMode.Property);

            builder
                .Entity<Kiosk>(options =>
                {
                    options.ToTable("Kiosks");

                    options.HasKey(o => o.KioskId);

                    options
                        .Property(o => o.PublicKey)
                        .HasColumnType("binary(32)");
                    
                    options
                        .HasMany(o => o.Displays)
                        .WithOne();
                });
            
            builder
                .Entity<KioskDisplay>(options =>
                {
                    options.ToTable("KioskDisplays");

                    options.HasKey(o => new {o.KioskId, o.DisplayId});
                });
        }
    }

    public static class SqlContextFactory
    {
        public static SqlContext Create(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SqlContext>();
            optionsBuilder.UseSqlServer(connectionString);

            var context = new SqlContext(optionsBuilder.Options);
            context.Database.Migrate();

            return context;
        }
    }
}