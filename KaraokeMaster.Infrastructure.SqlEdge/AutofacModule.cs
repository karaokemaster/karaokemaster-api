// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Autofac;
using Autofac.Features.AttributeFilters;
using KaraokeMaster.Application.Autofac;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using KaraokeMaster.Infrastructure.SqlEdge.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.Infrastructure.SqlEdge
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // TODO: Reconsider the lifetime if we're going to allow config changes?
            builder.Register(x =>
                {
                    var config = x.Resolve<IConfiguration>();
                    var optionsBuilder = new DbContextOptionsBuilder<SqlContext>()
                        // TODO: Use strongly-typed configuration object instead
                        .UseLoggerFactory(x.Resolve<ILoggerFactory>())
                        .EnableDetailedErrors()
                        .UseSqlServer(config.GetConnectionString("SqlEdgeConnection"));
            
                    var context = new SqlContext(optionsBuilder.Options);
                    context.Database.Migrate();
            
                    return optionsBuilder;
                })
                .SingleInstance();
            
            // TODO: Connection pooling
            builder.Register(x =>
            {
                var optionsBuilder = x.Resolve<DbContextOptionsBuilder<SqlContext>>();
                var context = new SqlContext(optionsBuilder.Options);
            
                return context;
            });

            // Register SQL distributed cache
            builder.RegisterConfig<SqlServerCacheOptions>((options, ctx) =>
            {
                options.ConnectionString = ctx.Resolve<IConfiguration>().GetConnectionString("SqlEdgeConnection");
                options.SchemaName = "dbo";
                options.TableName = "Cache";
                options.DefaultSlidingExpiration = TimeSpan.FromHours(1);
            });
            builder.RegisterType<SqlServerCache>().As<IDistributedCache>().SingleInstance();
            
            builder.RegisterType<QueueRepository>().As<IQueueRepository>();
            builder.RegisterType<VideoRepository>().As<IVideoRepository>().WithAttributeFiltering();
            builder.RegisterType<VideoOptionsRepository>().As<IVideoOptionsRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<UserVideoOptionsRepository>().As<IUserVideoOptionsRepository>();
            builder.RegisterType<SessionRepository>().As<ISessionRepository>();
            builder.RegisterType<VideoRecommendationRepository>().As<IVideoRecommendationRepository>();
            builder.RegisterType<KioskRepository>().As<IKioskRepository>();
            builder.RegisterType<KioskDisplayRepository>().As<IKioskDisplayRepository>();
            
            builder.RegisterType<BackupService>()
                .As<IHostedService>()
                .InstancePerDependency();
            
            base.Load(builder);
        }
    }
}