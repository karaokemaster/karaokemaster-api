// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge
{
    public class BackupService : IHostedService, IDisposable
    {
        private readonly IConfiguration _config;
        private readonly ILogger<BackupService> _logger;
        private Timer _timer;

        public BackupService(ILoggerFactory loggerFactory, IConfiguration config)
        {
            _logger = loggerFactory.CreateLogger<BackupService>();
            _config = config;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting database backup service");

            // TODO: Allow configuration of backup time, frequency
            
            var dueTime = DateTime.Today.AddDays(1.5) - DateTime.Now;
            if (dueTime.TotalHours > 24)
                dueTime = dueTime.Subtract(TimeSpan.FromDays(1));

            _timer = new Timer(state =>
            {
                SentrySdk.ConfigureScope(scope =>
                {
                    var transaction = SentrySdk.StartTransaction("BackupService", "TimerInterval");
                    scope.Transaction = transaction;

                    DoBackup();
                    PurgeStaleBackups();
                    
                    transaction.Finish();
                });
            }, null, dueTime, TimeSpan.FromDays(1));

            return Task.CompletedTask;
        }

        private void DoBackup()
        {
            var span = SentrySdk.GetSpan()?.StartChild("SqlBackup");
            
            using (_logger.BeginScope("Database backup"))
            {
                var connectionString = _config.GetConnectionString("SqlEdgeConnection");

                _logger.LogDebug("Using database connection string \"{ConnString}\"", connectionString);

                var connection = new SqlConnection(connectionString);
                var dbName = connection.Database;
                var backupRoot = _config.GetSection("BackupPath").Value;
                var backupFile = $"{dbName}_{DateTime.Today:yyyyMMdd}.bak";
                var backupPath = Path.Join(backupRoot, backupFile);

                _logger.LogInformation("Beginning backup of database [{DbName}] to {BackupPath}", dbName, backupPath);

                var backupSql = $"BACKUP DATABASE [{dbName}] TO DISK = '{backupPath}';";

                connection.Open();

                var command = connection.CreateCommand();
                command.CommandText = backupSql;
                command.CommandType = CommandType.Text;

                command.ExecuteNonQuery();

                command.Dispose();

                connection.Close();
                connection.Dispose();

                _logger.LogInformation("Completed backup of database [{DbName}] to {BackupPath}", dbName, backupPath);
            }
            
            span?.Finish();
        }

        private void PurgeStaleBackups()
        {
            var purgeSpan = SentrySdk.GetSpan()?.StartChild("PurgeStale");
                
            var backupRoot = _config.GetSection("BackupPath").Value;
            var connectionString = _config.GetConnectionString("SqlEdgeConnection");
            var csBuilder = new SqlConnectionStringBuilder(connectionString);
            var dbName = csBuilder.InitialCatalog;
            
            var backupDirectory = new DirectoryInfo(backupRoot);
            var backups = backupDirectory.EnumerateFiles($"{dbName}_*.bak");

            backups
                .OrderByDescending(b => b.Name)
                .Skip(3)
                .ToList()
                .ForEach(b =>
                {
                    _logger.LogInformation("Removing stale backup file {BackupFile}", b.Name);
                    b.Delete();
                });
                
            purgeSpan?.Finish();
        }
        
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping database backup service");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}