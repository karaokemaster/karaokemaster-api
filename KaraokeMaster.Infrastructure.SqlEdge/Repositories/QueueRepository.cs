// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using KaraokeMaster.Services.Search;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class QueueRepository : IQueueRepository
    {
        private readonly ILogger<QueueRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;
        private readonly ISearchProvider _searchProvider;

        public QueueRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory, [Optional] ISearchProvider searchProvider)
        {
            _logger = loggerFactory.CreateLogger<QueueRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
            _searchProvider = searchProvider;
        }
        
        public async Task Add(QueueEntry entry)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.Add");
            span?.SetExtra(nameof(entry), entry);
            
            using (_logger.BeginScope("Adding video: {VideoId}", entry.VideoId))
            {
                // Setting this to int.MaxValue ensure it'll be the last item after we call SortQueue
                entry.Order = int.MaxValue;

                await using var context = _contextFactory().Value;

                await context.QueueEntries.AddAsync(entry);
                await context.SaveChangesAsync();

                await PrioritizeQueueEntry(entry.QueueId);
                
                // TODO: Get updated order, etc.
                if (_searchProvider != null)
                    await _searchProvider.AddOrUpdate(entry);
            }
            
            span?.Finish();
        }

        public async Task Update(QueueEntry entry)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.Update");
            span?.SetExtra(nameof(entry), entry);
            
            using (_logger.BeginScope("Updating entry: {QueueId}", entry.QueueId))
            {
                await using var context = _contextFactory().Value;

                var queueEntry = await context.QueueEntries
                    .SingleOrDefaultAsync(q => q.QueueId == entry.QueueId);

                if (queueEntry == null) return;

                context.Entry(queueEntry).CurrentValues.SetValues(entry);
                context.QueueEntries.Update(queueEntry);

                await context.SaveChangesAsync();

                await SortQueue();
                
                // TODO: Get updated sort order, etc.
                if (_searchProvider != null)
                    await _searchProvider.AddOrUpdate(queueEntry);
            }

            span?.Finish();
        }

        public async Task Move(int queueId, int? afterId, int? beforeId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.Move");
            span?.SetExtra(nameof(queueId), queueId);
            span?.SetExtra(nameof(afterId), afterId);
            span?.SetExtra(nameof(beforeId), beforeId);
            
            // TODO: Seems like another great place to get all MemoryCache
            using (_logger.BeginScope("Move queue entry: {QueueId}", queueId))
            {
                await using var context = _contextFactory().Value;
                await using var connection = context.Database.GetDbConnection();

                var command = connection.GetSqlCommand("MoveQueueEntry");

                var param = command.CreateParameter();
                param.ParameterName = "QueueId";
                param.Value = queueId;
                command.Parameters.Add(param);

                var afterParam = command.CreateParameter();
                afterParam.ParameterName = "AfterId";
                afterParam.Value = afterId ?? -1;
                command.Parameters.Add(afterParam);

                var beforeParam = command.CreateParameter();
                beforeParam.ParameterName = "BeforeId";
                beforeParam.Value = beforeId ?? -1;
                command.Parameters.Add(beforeParam);

                await connection.OpenAsync();

                await command.ExecuteNonQueryAsync();

                // TODO: Get updated sort order, etc.
                // if (_searchProvider != null)
                //    await _searchProvider.AddOrUpdate(queueEntry);

                span?.Finish();
            }
        }

        public async Task Delete(int queueId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.Delete");
            span?.SetExtra(nameof(queueId), queueId);
            
            using (_logger.BeginScope("Removing entry: {QueueId}", queueId))
            {
                await using var context = _contextFactory().Value;

                var queueEntry = await context.QueueEntries
                    .SingleOrDefaultAsync(q => q.QueueId == queueId);

                if (queueEntry == null) return;

                context.QueueEntries.Remove(queueEntry);
                await context.SaveChangesAsync();

                await SortQueue();
                
                // TODO: Remove
                // if (_searchProvider != null)
                //    await _searchProvider.AddOrUpdate(video);
            }

            span?.Finish();
        }

        public async Task Delete(QueueEntry entry)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.Delete");
            span?.SetExtra(nameof(entry), entry);
            
            await Delete(entry.QueueId);

            span?.Finish();
        }

        public async Task<QueueEntry> Get(int queueId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.Get");
            span?.SetExtra(nameof(queueId), queueId);
            
            using (_logger.BeginScope("Get entry: {QueueId}", queueId))
            {
                await using var context = _contextFactory().Value;

                var queueEntry = await context.QueueEntries
                    .SingleOrDefaultAsync(q => q.QueueId == queueId);

                span?.Finish();

                return queueEntry;
            }
        }

        public async Task<List<QueueEntry>> GetAll(bool includePlayed = false)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.GetAll");
            span?.SetExtra(nameof(includePlayed), includePlayed);
            
            using (_logger.BeginScope("All videos from database (includePlayed = {IncludePlayed})", includePlayed))
            {
                await using var context = _contextFactory().Value;

                var videosQuery = context.QueueEntries.AsQueryable();
                if (includePlayed)
                {
                    var sessionBucket = await GetSessionBucket();
                    videosQuery = videosQuery.Where(q => q.PlaybackBucket == sessionBucket || q.Order >= 0);
                }
                else
                {
                    videosQuery = videosQuery.Where(q => q.Order >= 0);
                }

                var videos = await videosQuery
                    .Include(q => q.Video)
                        .ThenInclude(v => v.Options)
                    .Include(q => q.User)
                    .Include(q => q.UserOptions)
                    .OrderBy(q => q.Order)
                    .ToListAsync();

                span?.Finish();
                
                return videos;
            }
        }

        private async Task SortQueue()
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.SortQueue");
            
            await using var context = _contextFactory().Value;

            await context.Database.ExecuteEmbeddedSql("SortQueue");
            
            span?.Finish();
        }   

        private async Task PrioritizeQueueEntry(int queueId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.PrioritizeQueueEntry");
            span?.SetExtra(nameof(queueId), queueId);
            
            await using var context = _contextFactory().Value;
            await using var connection = context.Database.GetDbConnection();

            var command = connection.GetSqlCommand("PrioritizeQueueEntry");

            var param = command.CreateParameter();
            param.ParameterName = "QueueId";
            param.Value = queueId;
            command.Parameters.Add(param);

            await connection.OpenAsync();

            await command.ExecuteNonQueryAsync();
            
            span?.Finish();
        }   

        private async Task<DateTime> GetSessionBucket()
        {
            var span = SentrySdk.GetSpan()?.StartChild("QueueRepository.GetSessionBucket");
            
            await using var context = _contextFactory().Value;
            await using var connection = context.Database.GetDbConnection();

            var command = connection.GetSqlCommand("SessionBucket");

            await connection.OpenAsync();

            var result = await command.ExecuteScalarAsync() as DateTime? ?? DateTime.Today;
            
            span?.Finish();

            return result;
        }   
    }
}