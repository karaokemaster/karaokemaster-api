// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class VideoOptionsRepository : IVideoOptionsRepository
    {
        private readonly ILogger<VideoOptionsRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;

        public VideoOptionsRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<VideoOptionsRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task AddOrUpdate(VideoOptions options)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoOptionsRepository.AddOrUpdate");
            span?.SetExtra(nameof(options), options);
            
            using (_logger.BeginScope("AddOrUpdate video options: {VideoId}", options.VideoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var dbOptions = await context.VideoOptions.FirstOrDefaultAsync(o => o.VideoId == options.VideoId);
                    if (dbOptions != null)
                    {
                        context.Entry(dbOptions).CurrentValues.SetValues(options);
                        context.VideoOptions.Update(dbOptions);
                    }                    
                    else
                    {
                        await context.VideoOptions.AddAsync(options);
                    }

                    await context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in AddOrUpdate");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }        
            
            span?.Finish();
        }

        public async Task<VideoOptions> Get(int videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoOptionsRepository.Get");
            span?.SetExtra(nameof(videoId), videoId);
            
            using (_logger.BeginScope("Get user video options: {VideoId}", videoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var options = await context.VideoOptions.FirstOrDefaultAsync(o => o.VideoId == videoId);

                    span?.Finish();

                    return options;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<VideoOptions>> Get(IEnumerable<int> videoIds)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserVideoOptionsRepository.Get");
            
            var ids = videoIds.ToList();
            
            using (_logger.BeginScope("Get user video options for {VideoIds} videos", ids.Count))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var options = await context.VideoOptions.Where(o => ids.Contains(o.VideoId)).ToListAsync();

                    span?.Finish();

                    return options;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }
    }
}