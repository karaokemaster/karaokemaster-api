// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class UserVideoOptionsRepository : IUserVideoOptionsRepository
    {
        private readonly ILogger<UserVideoOptionsRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;

        public UserVideoOptionsRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<UserVideoOptionsRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task AddOrUpdate(UserVideoOptions options)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserVideoOptionsRepository.AddOrUpdate");
            span?.SetExtra(nameof(options), options);
            
            using (_logger.BeginScope("AddOrUpdate user video options: {UserId}, {VideoId}", options.UserId, options.VideoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var dbOptions = await context.UserVideoOptions.FirstOrDefaultAsync(o =>
                        o.UserId == options.UserId && o.VideoId == options.VideoId);
                    if (dbOptions != null)
                    {
                        context.Entry(dbOptions).CurrentValues.SetValues(options);
                        context.UserVideoOptions.Update(dbOptions);
                    }
                    else
                    {
                        await context.UserVideoOptions.AddAsync(options);
                    }

                    await context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in AddOrUpdate");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }        
            
            span?.Finish();
        }

        public async Task<UserVideoOptions> Get(int userId, int videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserVideoOptionsRepository.Get");
            span?.SetExtra(nameof(userId), userId);
            span?.SetExtra(nameof(videoId), videoId);
            
            using (_logger.BeginScope("Get user video options: {UserId}, {VideoId}", userId, videoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var options = await context.UserVideoOptions.FirstOrDefaultAsync(o => o.UserId == userId && o.VideoId == videoId);

                    span?.Finish();

                    return options;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<UserVideoOptions>> Get(int userId, IEnumerable<int> videoIds)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserVideoOptionsRepository.Get");
            span?.SetExtra(nameof(userId), userId);
            
            var ids = videoIds.ToList();
            
            using (_logger.BeginScope("Get user video options: {UserId}, {VideoIds}", userId, string.Join(",", ids)))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var options = await context.UserVideoOptions.Where(o => o.UserId == userId && ids.Contains(o.VideoId)).ToListAsync();

                    span?.Finish();

                    return options;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<UserVideoOptions>> GetAll(int userId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserVideoOptionsRepository.GetAll");
            span?.SetExtra(nameof(userId), userId);
            
            using (_logger.BeginScope("Get user video options: {UserId}", userId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var options = await context
                        .UserVideoOptions
                        .Include(q => q.Video)
                        .Include(q => q.Video.Options)
                        .Where(o => o.UserId == userId && !o.Video.Unavailable).ToListAsync();

                    span?.Finish();

                    return options;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }
    }
}