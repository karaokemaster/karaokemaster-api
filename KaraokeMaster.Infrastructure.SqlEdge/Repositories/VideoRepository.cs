// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Domain.Models.Configuration;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using KaraokeMaster.Services.Search;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class VideoRepository : IVideoRepository
    {
        private readonly ILogger<VideoRepository> _logger;
        private readonly IOptionsMonitor<RecommendationConfiguration> _recommendationConfiguration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;
        private readonly IEnumerable<IVideoSource> _videoSources;
        private readonly ISearchProvider _searchProvider;

        public VideoRepository(ILoggerFactory loggerFactory, IOptionsMonitor<RecommendationConfiguration> recommendationConfiguration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory, IEnumerable<IVideoSource> videoSources, [Optional] ISearchProvider searchProvider)
        {
            _logger = loggerFactory.CreateLogger<VideoRepository>();
            _recommendationConfiguration = recommendationConfiguration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
            _videoSources = videoSources;
            _searchProvider = searchProvider;
        }
        
        public async Task<Video> AddOrUpdate(Video video)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.AddOrUpdate");
            span?.SetExtra(nameof(video), video);
            
            using (_logger.BeginScope("AddOrUpdate video: {VideoId}", video.YouTubeVideoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;
                    
                    var dbVideo = await context.Videos.FirstOrDefaultAsync(v => v.YouTubeVideoId == video.YouTubeVideoId);
                    if (dbVideo != null)
                    {
                        _logger.LogTrace("Update video ID {VideoId}, YouTubeVideoId {YouTubeVideoId}", dbVideo.VideoId, video.YouTubeVideoId);

                        video.VideoId = dbVideo.VideoId;
                        video.LastUpdate = DateTime.UtcNow;
                        context.Entry(dbVideo).CurrentValues.SetValues(video);
                        video = context.Videos.Update(dbVideo).Entity;
                    }
                    else
                    {
                        _logger.LogTrace("Add video YouTubeVideoId {YouTubeVideoId}", video.YouTubeVideoId);
                        video.LastUpdate = DateTime.UtcNow;
                        await context.Videos.AddAsync(video);
                    }

                    await context.SaveChangesAsync();

                    if (_searchProvider != null)
                        await _searchProvider.AddOrUpdate(video);

                    return video;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in AddOrUpdate");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
                finally
                {
                    span?.Finish();
                }
            }
        }

        public async Task<Video> Get(int videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.Get");
            span?.SetExtra(nameof(videoId), videoId);
            
            using (_logger.BeginScope("Get video: {VideoId}", videoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var video = await context.Videos.FirstOrDefaultAsync(v => v.VideoId == videoId);

                    span?.Finish();

                    return video;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }        
        }

        public async Task<Video> Get(string youTubeVideoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.Get");
            span?.SetExtra(nameof(youTubeVideoId), youTubeVideoId);
            
            using (_logger.BeginScope("Get video: {VideoId}", youTubeVideoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var video = await context.Videos.FirstOrDefaultAsync(v => v.YouTubeVideoId == youTubeVideoId);

                    span?.Finish();

                    return video;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<Video>> Get(IEnumerable<string> videoIds)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.Get");
            
            var ids = videoIds.ToList();
            using (_logger.BeginScope("Get videos: {VideoIds}", string.Join(",", ids)))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var videos = await context.Videos.Where(v => ids.Contains(v.YouTubeVideoId)).ToListAsync();

                    span?.Finish();

                    return videos;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<Video>> GetAll()
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.GetAll");
            
            using (_logger.BeginScope("GetAll"))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var videos = await context.Videos.ToListAsync();

                    span?.Finish();

                    return videos;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in GetAll");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<Video>> Find(string query)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.Find");
            span?.SetExtra(nameof(query), query);
            
            using (_logger.BeginScope("Find videos: {Query}", query))
            {
                try
                {
                    var videos = new List<Video>();

                    await using var context = _contextFactory().Value;
                    await using var connection = context.Database.GetDbConnection();

                    var command = connection.GetSqlCommand("Search");

                    var param = command.CreateParameter();
                    param.ParameterName = "Query";
                    param.Value = query;
                    command.Parameters.Add(param);

                    await connection.OpenAsync();

                    var reader = await command.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var video = new Video
                        {
                            Title = reader["Title"].ToString(),
                            YouTubeVideoId = reader["YouTubeVideoId"].ToString(),
                            Duration = (int) reader["Duration"],
                            ThumbnailUrl = reader["ThumbnailUrl"].ToString(),
                            // Lyrics = reader["Lyrics"].ToString(),
                        };

                        videos.Add(video);
                    }

                    return videos;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Find");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<Video>> GetStale(DateTime updatedBefore, int maxVideos)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.GetStale");
            span?.SetExtra(nameof(updatedBefore), updatedBefore);
            span?.SetExtra(nameof(maxVideos), maxVideos);
            
            using (_logger.BeginScope("GetStale"))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var videos = await context.Videos
                        .Where(v => !v.Unavailable)
                        .Where(v => v.LastUpdate < updatedBefore)
                        .OrderBy(v => v.LastUpdate)
                        .ThenBy(v => v.VideoId)
                        .Take(maxVideos)
                        .Select(v => new Video(v)
                        {
                            PlayCount = context.QueueEntries.Count(q => q.VideoId == v.VideoId)
                        })
                        .ToListAsync();

                    span?.Finish();

                    return videos;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in GetStale");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<Video>> GetRecommendationsForUser(string name)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.GetRecommendationsForUser");
            span?.SetExtra(nameof(name), name);
            
            // TODO: Seems like another great place to get all MemoryCache
            using (_logger.BeginScope("Recommended videos from database for user: {Name}", name))
            {
                var recommendations = new List<Video>();

                var dbCount = _recommendationConfiguration.CurrentValue.DatabaseCount;

                await using var context = _contextFactory().Value;
                await using var connection = context.Database.GetDbConnection();

                // TODO: Reexamine this query in light of the table split
                // TODO: Grab datebucket definition from a central location
                var command = connection.GetSqlCommand("RecommendationsForUser");

                // TODO: Pass in user ID instead of name
                var param = command.CreateParameter();
                param.ParameterName = "Name";
                param.Value = name;
                command.Parameters.Add(param);

                var countParam = command.CreateParameter();
                countParam.ParameterName = "MaxCount";
                countParam.Value = dbCount;
                command.Parameters.Add(countParam);

                await connection.OpenAsync();

                var reader = await command.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    DateTime.TryParse(reader["LastPlayed"].ToString(), out var lastPlayed);
                    int.TryParse(reader["PlayCount"].ToString(), out var playCount);
                    
                    var video = new Video
                    {
                        Title = reader["Title"].ToString(),
                        YouTubeVideoId = reader["YouTubeVideoId"].ToString(),
                        Duration = (int) reader["Duration"],
                        ThumbnailUrl = reader["ThumbnailUrl"].ToString(),
                        // Lyrics = reader["Lyrics"].ToString(),
                        PlayCount = playCount,
                        LastPlayed = lastPlayed
                    };

                    recommendations.Add(video);
                }

                span?.Finish();

                return recommendations;
            }
        }

        public async Task<List<Video>> GetPreviouslyPlayedForUser(string name)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.GetPreviouslyPlayedForUser");
            span?.SetExtra(nameof(name), name);
            
            // TODO: Seems like another great place to get all MemoryCache
            using (_logger.BeginScope("Previously played videos from database for user: {Name}", name))
            {
                var previouslyPlayed = new List<Video>();

                await using var context = _contextFactory().Value;
                await using var connection = context.Database.GetDbConnection();

                // TODO: Reexamine this query in light of the table split
                // TODO: Grab datebucket definition from a central location
                var command = connection.GetSqlCommand("PreviouslyPlayedForUser") as SqlCommand;

                // TODO: Pass in user ID instead of name
                var param = command.CreateParameter();
                param.ParameterName = "Name";
                param.Value = name;
                command.Parameters.Add(param);

                await connection.OpenAsync();

                var reader = await command.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    var video = new Video
                    {
                        VideoId = (int)reader["VideoId"],
                        Title = reader["Title"].ToString(),
                        YouTubeVideoId = reader["YouTubeVideoId"].ToString(),
                        Duration = (int) reader["Duration"],
                        ThumbnailUrl = reader["ThumbnailUrl"].ToString(),
                        // Lyrics = reader["Lyrics"].ToString(),
                        PlayCount = (int) reader["PlayCount"],
                        LastPlayed = DateTime.Parse(reader["LastPlayed"].ToString() ?? "")
                    };
                    previouslyPlayed.Add(video);
                }

                span?.Finish();

                return previouslyPlayed;
            }
        }

        public async Task<Video> GetVideoDetails(string videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRepository.GetVideoDetails");
            span?.SetExtra(nameof(videoId), videoId);
            
            using (_logger.BeginScope("GetVideoDetails {VideoId}", videoId))
            {
                // TODO: Only query the relevant video source
                var detailTasks = _videoSources
                    .Select(s => s.GetVideoDetails(videoId))
                    .ToList();

                await Task.WhenAll(detailTasks);

                var video = detailTasks
                    .Select(t => t.Result)
                    .FirstOrDefault(v => v != null);

                // TODO: Eliminate this step
                var video2 = await Get(videoId);
                if (video != null && video2 != null)
                {
                    video.VideoId = video2.VideoId;
                }
                
                span?.Finish();

                return video;
            }
        }
    }
}