// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ILogger<UserRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;
        
        public UserRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<UserRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task AddOrUpdate(User user)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserRepository.AddOrUpdate");
            span?.SetExtra(nameof(user), user);
            
            using (_logger.BeginScope("AddOrUpdate user: {Username} ({UserId})", user.Username, user.UserId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var dbUser = await context.Users.FirstOrDefaultAsync(u => u.UserId == user.UserId || u.Username == user.Username);
                    if (dbUser != null)
                    {
                        user.UserId = dbUser.UserId;
                        context.Entry(dbUser).CurrentValues.SetValues(user);
                        context.Users.Update(dbUser);
                    }
                    // else if (await context.Users.AnyAsync(u => u.Username == user.Username))
                    // {
                    //     var u = await context.Users.FirstAsync(u => u.Username == user.Username);
                    //     // TODO: Map values
                    // }
                    else
                    {
                        await context.Users.AddAsync(user);
                    }

                    await context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in AddOrUpdate");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
            
            span?.Finish();
        }

        public async Task<User> Get(int userId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserRepository.Get");
            span?.SetExtra(nameof(userId), userId);
            
            using (_logger.BeginScope("Get user ID: {UserId}", userId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var user = await context.Users.FirstOrDefaultAsync(u => u.UserId == userId);

                    span?.Finish();

                    return user;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<User> Get(string name)
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserRepository.Get");
            span?.SetExtra(nameof(name), name);
            
            using (_logger.BeginScope("Get user: {Name}", name))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var user = await context.Users.FirstOrDefaultAsync(u => u.Username == name.Trim());

                    span?.Finish();

                    return user;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<User>> GetAll()
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserRepository.GetAll");
            
            using (_logger.BeginScope("GetAll"))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var users = await context.Users.ToListAsync();

                    span?.Finish();

                    return users;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in GetAll");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<User>> GetActive()
        {
            var span = SentrySdk.GetSpan()?.StartChild("UserRepository.GetActive");
            
            using (_logger.BeginScope("Get active users from database"))
            {
                try
                {
                    var users = new List<User>();
                    
                    await using var context = _contextFactory().Value;
                    await using var connection = context.Database.GetDbConnection();

                    var command = connection.GetSqlCommand("ActiveUsers");

                    await connection.OpenAsync();

                    var reader = await command.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var user = new User
                        {
                            UserId = reader.GetInt32("UserId"),
                            Username = reader.GetString("Username"),
                            IsActive = reader.GetBoolean("IsActive")
                        };

                        users.Add(user);
                    }

                    return users;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in GetActive");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }
    }
}