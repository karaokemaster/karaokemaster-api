// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;
using User = KaraokeMaster.Domain.Models.User;
using Session = KaraokeMaster.Domain.Models.Session;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class SessionRepository : ISessionRepository
    {
        private readonly ILogger<SessionRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;

        public SessionRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<SessionRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task<List<Session>> GetSessions(int userId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionRepository.GetSessions");
            span?.SetExtra(nameof(userId), userId);
            
            using (_logger.BeginScope("All sessions from database (userId = {UserId})", userId))
            {
                var sessions = new List<Session>();

                await using var context = _contextFactory().Value;
                await using var connection = context.Database.GetDbConnection();

                var command = connection.GetSqlCommand("SessionsForUser");

                var param = command.CreateParameter();
                param.ParameterName = "UserId";
                param.Value = userId;
                command.Parameters.Add(param);

                await connection.OpenAsync();

                var reader = await command.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    var session = new Session
                    {
                        Date = (DateTime) reader["PlaybackBucket"],
                        VideoCount = (int) reader["VideoCount"]
                    };

                    sessions.Add(session);
                }

                return sessions;
            }
        }

        public async Task<Session> GetSession(DateTime session)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionRepository.GetSession");
            span?.SetExtra(nameof(session), session);
            
            using (_logger.BeginScope("Session from database (session = {Date})", session.Date))
            {
                await using var context = _contextFactory().Value;
                await using var connection = context.Database.GetDbConnection();

                var command = connection.GetSqlCommand("SessionEntries");

                var param = command.CreateParameter();
                param.ParameterName = "PlaybackBucket";
                param.Value = session.Date;
                command.Parameters.Add(param);

                await connection.OpenAsync();

                var reader = await command.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    var result = new Session
                    {
                        Date = (DateTime) reader["PlaybackBucket"],
                        VideoCount = (int) reader["VideoCount"]
                    };

                    return result;
                }

                return null;
            }
        }

        public async Task<List<Video>> GetSessionVideos(int userId, DateTime session)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionRepository.GetSessionVideos");
            span?.SetExtra(nameof(userId), userId);
            span?.SetExtra(nameof(session), session);

            using (_logger.BeginScope("All sessions from database (userId = {UserId})", userId))
            {
                await using var context = _contextFactory().Value;
                var videos = context
                    .QueueEntries
                    .FromSqlInterpolated(
                        $"SELECT TOP 100 PERCENT * FROM QueueEntries WHERE UserId = {userId} AND PlaybackBucket = CAST({session.Date} AS date) ORDER BY PlaybackDate")
                    .Include(q => q.Video.Options)
                    .OrderBy(q => q.PlaybackDate)
                    .Select(q => q.Video)
                    .ToList();

                return videos;
            }
        }

        public async Task<List<QueueEntry>> GetAllSessionVideos(DateTime session)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionRepository.GetAllSessionVideos");
            span?.SetExtra(nameof(session), session);

            using (_logger.BeginScope("All session videos from database (session = {Session})", session))
            {
                await using var context = _contextFactory().Value;
                var videos = context
                    .QueueEntries
                    .FromSqlInterpolated(
                        $"SELECT TOP 100 PERCENT * FROM QueueEntries WHERE PlaybackBucket = CAST({session.Date} AS date) ORDER BY PlaybackDate")
                    .Include(q => q.Video)
                        .ThenInclude(v => v.Options)
                    .Include(q => q.User)
                    .Include(q => q.UserOptions)
                    .OrderBy(q => q.PlaybackDate)
                    .ToList();

                return videos;
            }
        }

        public async Task<List<User>> GetSessionUsers(DateTime session)
        {
            var span = SentrySdk.GetSpan()?.StartChild("SessionRepository.GetSessionUsers");
            span?.SetExtra(nameof(session), session);
            
            using (_logger.BeginScope("All users in session from database (session = {Session})", session))
            {
                await using var context = _contextFactory().Value;
                var users = context
                    .Users
                    .FromSqlInterpolated(
                        $"SELECT TOP 100 PERCENT * FROM Users WHERE UserId IN (SELECT DISTINCT UserId FROM QueueEntries WHERE PlaybackBucket = CAST({session.Date} AS date))")
                    .ToList();

                return users;
            }
        }
    }
}