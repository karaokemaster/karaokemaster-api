// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories
{
    public class VideoRecommendationRepository : IVideoRecommendationRepository
    {
        private readonly ILogger<VideoRecommendationRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        private readonly Func<Owned<SqlContext>> _contextFactory;

        public VideoRecommendationRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
        {
            _logger = loggerFactory.CreateLogger<VideoRecommendationRepository>();
            _configuration = configuration;
            _memoryCache = memoryCache;
            _contextFactory = contextFactory;
        }
        
        public async Task AddOrUpdate(VideoRecommendation recommendation)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRecommendationRepository.AddOrUpdate");
            span?.SetExtra(nameof(recommendation), recommendation);
            
            using (_logger.BeginScope("AddOrUpdate video recommendation: {VideoId}", recommendation.VideoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    recommendation.LastUpdated = DateTime.UtcNow;
                    
                    var dbRecommendation = await context.VideoRecommendations.FirstOrDefaultAsync(
                        r => r.VideoId == recommendation.VideoId 
                             && r.RecommendedVideoId == recommendation.RecommendedVideoId 
                             && r.Source == recommendation.Source);
                    if (dbRecommendation != null)
                    {
                        context.Entry(dbRecommendation).CurrentValues.SetValues(recommendation);
                        context.VideoRecommendations.Update(dbRecommendation);
                    }
                    else
                    {
                        await context.VideoRecommendations.AddAsync(recommendation);
                    }

                    await context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in AddOrUpdate");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }        
            
            span?.Finish();
        }

        public async Task<VideoRecommendation> Get(int videoId)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRecommendationRepository.Get");
            span?.SetExtra(nameof(videoId), videoId);
            
            using (_logger.BeginScope("Get video recommendation: {VideoId}", videoId))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var recommendations = await context.VideoRecommendations.FirstOrDefaultAsync(o => o.VideoId == videoId);

                    span?.Finish();

                    return recommendations;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<VideoRecommendation>> Get(IEnumerable<int> videoIds)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRecommendationRepository.Get");
            
            var ids = videoIds.ToList();
            
            using (_logger.BeginScope("Get video recommendations: {VideoIds}", string.Join(",", ids)))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var recommendations = await context.VideoRecommendations.Where(o => ids.Contains(o.VideoId)).ToListAsync();

                    span?.Finish();

                    return recommendations;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in Get");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }

        public async Task<List<int>> GetStaleVideoIds(DateTime updatedBefore, int maxVideos)
        {
            var span = SentrySdk.GetSpan()?.StartChild("VideoRecommendationRepository.GetStale");
            span?.SetExtra(nameof(updatedBefore), updatedBefore);
            span?.SetExtra(nameof(maxVideos), maxVideos);
            
            using (_logger.BeginScope("GetStale"))
            {
                try
                {
                    await using var context = _contextFactory().Value;

                    var staleVideos = context.Videos
                        .Where(v => !v.Unavailable && context.QueueEntries.Any(q => q.VideoId == v.VideoId))
                        .GroupJoin(
                            context.VideoRecommendations, 
                            v => v.VideoId,
                            r => r.VideoId,
                            (v, r) => new { Video = v, Recommendations = r }
                        )
                        .SelectMany(
                            v => v.Recommendations.DefaultIfEmpty(),
                            (v, r) => new { v.Video, LastUpdated = r.LastUpdated }
                        )
                        .GroupBy(
                            v => v.Video.VideoId,
                            v => new { v.Video, v.LastUpdated }
                        )
                        .Select(v => new { VideoId = v.Key, LastUpdated = v.Max(r => r.LastUpdated) })
                        .OrderBy(v => v.LastUpdated)
                        .Select(v => v.VideoId)
                        .Take(maxVideos)
                        .ToList()
                        ;

                    span?.Finish();

                    return staleVideos;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in GetStale");
                    span?.Finish(SpanStatus.InternalError);
                    throw;
                }
            }
        }
    }
}