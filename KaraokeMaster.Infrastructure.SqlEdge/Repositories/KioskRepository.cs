// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories;

public class KioskRepository : IKioskRepository
{
    private readonly ILogger<KioskRepository> _logger;
    private readonly IConfiguration _configuration;
    private readonly IMemoryCache _memoryCache;
    private readonly Func<Owned<SqlContext>> _contextFactory;

    public KioskRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
    {
        _logger = loggerFactory.CreateLogger<KioskRepository>();
        _configuration = configuration;
        _memoryCache = memoryCache;
        _contextFactory = contextFactory;
    }
    
    public async Task Add(Kiosk kiosk)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskRepository.Add");
        span?.SetExtra(nameof(kiosk), kiosk);
            
        using (_logger.BeginScope("Adding kiosk: {PublicKey}", kiosk.PublicKey))
        {
            await using var context = _contextFactory().Value;

            await context.Kiosks.AddAsync(kiosk);
            await context.SaveChangesAsync();
        }
            
        span?.Finish();
    }

    public async Task Update(Kiosk kiosk)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskRepository.Update");
        span?.SetExtra(nameof(kiosk), kiosk);
            
        using (_logger.BeginScope("Updating kiosk: {KioskId}", kiosk.KioskId))
        {
            await using var context = _contextFactory().Value;

            var dbKiosk = await context.Kiosks
                .Include(k => k.Displays)
                .SingleOrDefaultAsync(d => d.KioskId == kiosk.KioskId);

            if (dbKiosk == null) return;

            context.Entry(dbKiosk).CurrentValues.SetValues(kiosk);
            
            // Delete children
            dbKiosk.Displays
                .Where(d => kiosk.Displays.All(dd => dd.DisplayId != d.DisplayId))
                .ToList()
                .ForEach(d => dbKiosk.Displays.Remove(d));

            // Update children
            dbKiosk.Displays
                .Where(d => kiosk.Displays.Any(dd => dd.DisplayId == d.DisplayId))
                .ToList()
                .ForEach(d => context.Entry(d).CurrentValues.SetValues(kiosk.Displays.First(dd => dd.DisplayId == d.DisplayId)));

            // Insert children
            kiosk.Displays
                .Where(d => dbKiosk.Displays.All(dd => dd.DisplayId != d.DisplayId))
                .ToList()
                .ForEach(d => dbKiosk.Displays.Add(d));
            
            context.Kiosks.Update(dbKiosk);

            await context.SaveChangesAsync();
        }

        span?.Finish();
    }

    public Task Delete(int kioskId)
    {
        throw new System.NotImplementedException();
    }

    public Task Delete(Kiosk kiosk)
    {
        throw new System.NotImplementedException();
    }

    public async Task<Kiosk> Get(int kioskId)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskRepository.Get");
        span?.SetExtra(nameof(kioskId), kioskId);
            
        using (_logger.BeginScope("Get kiosk: {KioskId}", kioskId))
        {
            await using var context = _contextFactory().Value;

            var kiosk = await context.Kiosks
                .Include(k => k.Displays)
                .SingleOrDefaultAsync(k => k.KioskId == kioskId);

            span?.Finish();

            return kiosk;
        }
    }

    public async Task<Kiosk> Get(byte[] publicKey)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskRepository.Get");
        span?.SetExtra(nameof(publicKey), publicKey);
            
        using (_logger.BeginScope("Get kiosk: {PublicKey}", publicKey))
        {
            await using var context = _contextFactory().Value;

            var kiosk = await context.Kiosks
                .Include(k => k.Displays)
                .SingleOrDefaultAsync(k => k.PublicKey == publicKey);

            span?.Finish();

            return kiosk;
        }
    }

    public async Task<List<Kiosk>> GetAll()
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskRepository.GetAll");
            
        using (_logger.BeginScope("All kiosks from database"))
        {
            await using var context = _contextFactory().Value;

            var kiosks = await context
                .Kiosks
                .Include(k => k.Displays)
                .ToListAsync();

            span?.Finish();
                
            return kiosks;
        }
    }
}