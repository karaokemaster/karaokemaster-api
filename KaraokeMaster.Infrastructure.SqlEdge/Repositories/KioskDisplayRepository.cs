// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Infrastructure.SqlEdge.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.SqlEdge.Repositories;

public class KioskDisplayRepository : IKioskDisplayRepository
{
    private readonly ILogger<KioskDisplayRepository> _logger;
    private readonly IConfiguration _configuration;
    private readonly IMemoryCache _memoryCache;
    private readonly Func<Owned<SqlContext>> _contextFactory;

    public KioskDisplayRepository(ILoggerFactory loggerFactory, IConfiguration configuration, IMemoryCache memoryCache, Func<Owned<SqlContext>> contextFactory)
    {
        _logger = loggerFactory.CreateLogger<KioskDisplayRepository>();
        _configuration = configuration;
        _memoryCache = memoryCache;
        _contextFactory = contextFactory;
    }

    public async Task Add(KioskDisplay display)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskDisplayRepository.Add");
        span?.SetExtra(nameof(display), display);
            
        using (_logger.BeginScope("Adding display {DisplayId} for kiosk {KioskId}", display.DisplayId, display.KioskId))
        {
            await using var context = _contextFactory().Value;

            await context.KioskDisplays.AddAsync(display);
            await context.SaveChangesAsync();
        }
            
        span?.Finish();
    }

    public async Task Update(KioskDisplay display)
    {
        var span = SentrySdk.GetSpan()?.StartChild("KioskDisplayRepository.Update");
        span?.SetExtra(nameof(display), display);
            
        using (_logger.BeginScope("Updating display {DisplayId} for kiosk {KioskId}", display.DisplayId, display.KioskId))
        {
            await using var context = _contextFactory().Value;

            var dbDisplay = await context.KioskDisplays
                .SingleOrDefaultAsync(d => d.KioskId == display.KioskId && d.DisplayId == display.DisplayId);

            if (dbDisplay == null) return;

            context.Entry(dbDisplay).CurrentValues.SetValues(display);
            context.KioskDisplays.Update(dbDisplay);

            await context.SaveChangesAsync();
        }

        span?.Finish();
    }

    public Task Delete(int kioskId)
    {
        throw new System.NotImplementedException();
    }

    public Task Delete(KioskDisplay kiosk)
    {
        throw new System.NotImplementedException();
    }

    public Task<KioskDisplay> Get(int kioskId)
    {
        throw new System.NotImplementedException();
    }

    public Task<List<KioskDisplay>> GetForKiosk(Kiosk kiosk)
    {
        throw new System.NotImplementedException();
    }

    public Task<List<KioskDisplay>> GetAll()
    {
        throw new System.NotImplementedException();
    }
}