// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace KaraokeMaster.Infrastructure.SqlEdge
{
    internal static class ResourceHelpers
    {
        public static string LoadEmbeddedResourceFile(string filename)
        {
            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                using (var reader = new StreamReader(assembly.GetManifestResourceStream(typeof(ResourceHelpers), filename)))
                {
                    var output = new StringBuilder();

                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        output.AppendLine(line);
                    }

                    return output.ToString();
                }
            }
            catch (Exception)
            {
                // TODO: Log the exception appropriately.
            }

            return null;
        }

        public static DbCommand GetSqlCommand(this DbConnection db, string name)
        {
            var sql = GetEmbeddedSql(name);

            var command = db.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = sql;

            return command;
        }

        public static async Task ExecuteEmbeddedSql(this DatabaseFacade db, string name)
        {
            var sql = GetEmbeddedSql(name);

            await db.ExecuteSqlRawAsync(sql);
        }

        private static string GetEmbeddedSql(string name)
        {
            var resourceName = $"Queries.{name}.sql";

            var query = LoadEmbeddedResourceFile(resourceName);

            query = query.Substring(
                query.IndexOf("BEGIN", StringComparison.Ordinal) + 5,
                query.LastIndexOf("END", StringComparison.Ordinal) - query.IndexOf("BEGIN", StringComparison.Ordinal) - 5
            );

            return query;
        }
    }
}
