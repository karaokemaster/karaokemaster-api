DECLARE @Name nvarchar(50) = 'Mike'

BEGIN

    DECLARE @BucketAnchor datetime2 = CAST('1900-01-01 12:00:00.000' AS datetime2)
    DECLARE @BigBucket date = CAST(DATE_BUCKET(quarter, 1, CAST(getdate() AS datetime2), @BucketAnchor) AS date)

    ;
    WITH BigBuckets (QueueId, BigBucket) AS
             (
                 SELECT QueueEntries.QueueId,
                        CAST(DATE_BUCKET(quarter, 1, QueueEntries.PlaybackDate, @BucketAnchor) AS date) AS BigBucket
                 FROM QueueEntries WITH ( INDEX ( FI_QueueEntries_WithPlaybackDate ) )
                 WHERE QueueEntries.PlaybackDate IS NOT NULL
             ),
         PlaybackCounts (UserId, VideoId, BigBucket, PlayCount) AS
             (
                 SELECT  QueueEntries.UserId,
                         QueueEntries.VideoId,
                         q.BigBucket,
                         COUNT(QueueEntries.QueueId) AS PlayCount
                 FROM BigBuckets AS q
                          INNER JOIN QueueEntries ON QueueEntries.QueueId = q.QueueId
                 GROUP BY QueueEntries.UserId, QueueEntries.VideoId, q.BigBucket
                 HAVING COUNT(QueueEntries.QueueId) > 1
             )

    SELECT DISTINCT p.UserId,
                    p.VideoId,
                    v.YouTubeVideoId,
                    v.Title,
                    v.Duration,
                    v.ThumbnailUrl,
                    m.PlayCount,
                    m.LastPlayed
    FROM PlaybackCounts p
             INNER JOIN Users u
                        ON u.UserId = p.UserId
             INNER JOIN Videos v WITH ( INDEX ( FI_Videos_WithNotUnavailable ) )
                        ON v.VideoId = p.VideoId
             INNER JOIN (
        SELECT  VideoId,
                UserId,
                COUNT(QueueId)    AS PlayCount,
                MAX(PlaybackDate) AS LastPlayed
        FROM QueueEntries
        GROUP BY VideoId, UserId
    ) AS m
        ON m.UserId = u.UserId AND m.VideoId = p.VideoId
    WHERE u.Username = @Name
      AND p.VideoId NOT IN (SELECT f.VideoId FROM PlaybackCounts f WHERE f.UserId = u.UserId AND f.BigBucket > p.BigBucket)
      AND p.BigBucket < @BigBucket
      AND v.Unavailable = 0
    ORDER BY LastPlayed DESC

END