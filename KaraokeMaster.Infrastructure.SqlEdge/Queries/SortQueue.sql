BEGIN
    
    ;
    WITH QueueOrder AS
             (
                 SELECT
                     QueueId,
                     [Order],
                     ROW_NUMBER() OVER (ORDER BY [Order]) AS NewOrder
                 FROM QueueEntries
                 WHERE [Order] > 0
             )
    UPDATE QueueOrder
    SET [Order] = NewOrder
    WHERE [Order] > 0

END
