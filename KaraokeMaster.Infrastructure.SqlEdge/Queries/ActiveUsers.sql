BEGIN

    DECLARE @BucketAnchor datetime2 = CAST('1900-01-01 12:00:00.000' AS datetime2)
    DECLARE @SessionBucket date = CAST(DATE_BUCKET(day, 1, CAST(getdate() AS datetime2), @BucketAnchor) AS date)

    SELECT
        u.UserId,
        u.Username,
        CAST(IIF(s.UserId IS NULL, 0, 1) AS bit) AS IsActive
    FROM Users AS u
             LEFT OUTER JOIN (
        SELECT DISTINCT
            UserId
        FROM QueueEntries
        WHERE [Order] > -1
            OR PlaybackBucket = @SessionBucket
    ) AS s
        ON u.UserId = s.UserId

END