BEGIN

    DECLARE @BucketAnchor datetime2 = CAST('1900-01-01 12:00:00.000' AS datetime2)
    DECLARE @SessionBucket date = CAST(DATE_BUCKET(day, 1, CAST(getdate() AS datetime2), @BucketAnchor) AS date)

    SELECT @SessionBucket AS SessionBucket
    
END