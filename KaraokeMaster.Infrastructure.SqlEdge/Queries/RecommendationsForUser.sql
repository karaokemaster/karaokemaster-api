DECLARE @Name     nvarchar(50) = 'Mike'
DECLARE @MaxCount int = 50

BEGIN

    DECLARE @BucketAnchor datetime2 = CAST('1900-01-01 12:00:00.000' AS datetime2)
    DECLARE @TodayBucket date = CAST(DATE_BUCKET(day, 1, CAST(getdate() AS datetime2), @BucketAnchor) AS date)
    DECLARE @UserId int = (SELECT UserId FROM Users WHERE Username = @Name)

    ;
    WITH PlaybackCounts (UserId, VideoId, LastPlayed, PlayCount) AS
             (
                 SELECT q.UserId,
                        q.VideoId,
                        MAX(q.PlaybackDate) AS LastPlayed,
                        COUNT(q.QueueId)    AS PlayCount
                 FROM QueueEntries AS q WITH (INDEX (FI_QueueEntries_WithPlaybackDate) )
                 WHERE q.PlaybackDate IS NOT NULL
                   AND q.UserId = @UserId
                 GROUP BY q.UserId, q.VideoId
             ),
         RelatedPlaybackCounts (UserId, VideoId, RelatedVideoId, PlayCount) AS
             (
                 SELECT q.UserId,
                        q.VideoId,
                        r.VideoId        AS RelatedVideoId,
                        COUNT(r.QueueId) AS PlayCount
                 FROM QueueEntries AS q WITH (INDEX (FI_QueueEntries_WithPlaybackDate) )
                          INNER JOIN QueueEntries AS r
                                     ON r.PlaybackBucket = q.PlaybackBucket
                                         AND r.VideoId != q.VideoId
                 WHERE q.PlaybackDate IS NOT NULL
                   AND q.UserId = @UserId
                   AND r.UserId = @UserId
                 GROUP BY q.UserId, q.VideoId, r.VideoId
             ),
         Recommendations (VideoId, PlayCount, Weight) AS
             (
                 -- Get videos that the user has frequently played at the same time as today's tracks
                 SELECT TOP (@MaxCount) fr.RelatedVideoId AS VideoId,
                                        SUM(fr.PlayCount) AS PlayCount,
                                        COUNT(fr.VideoId) AS Weight
                 FROM RelatedPlaybackCounts fr
                          INNER JOIN QueueEntries fq
                                     ON fq.VideoId = fr.RelatedVideoId
                          INNER JOIN Users fu -- This join isn't being used, but removing it kills performance
                                     ON fr.UserId = fu.UserId
                 WHERE fr.VideoId IN (
                     SELECT VideoId
                     FROM QueueEntries AS qq
                     WHERE qq.UserId = @UserId
                       AND (qq.PlaybackBucket = @TodayBucket
                         OR qq.[Order] > 0)
                 )
                 GROUP BY fr.RelatedVideoId, fr.PlayCount
                 ORDER BY SUM(fr.PlayCount) DESC, COUNT(fr.VideoId) DESC

                 UNION

                 -- Get the user's most frequently played videos
                 SELECT TOP (@MaxCount) fq.VideoId, 0 AS PlayCount, 1 AS Weight
                 FROM QueueEntries AS fq
                 WHERE fq.UserId = @UserId
                   AND fq.VideoId NOT IN (
                     SELECT DISTINCT VideoId
                     FROM QueueEntries
                     WHERE [Order] > -1
                 )
                 GROUP BY fq.VideoId
                 ORDER BY COUNT(fq.QueueId) DESC

                 UNION

                 -- Get videos from recommendations based on today's tracks
                 SELECT TOP (@MaxCount) vr.RecommendedVideoId AS VideoId,
                                        0 AS PlayCount,
                                        COUNT(vr.VideoId) AS Weight
                 FROM VideoRecommendations AS vr
                 WHERE vr.VideoId IN (
                     SELECT VideoId
                     FROM QueueEntries AS qq
                     WHERE qq.UserId = @UserId
                       AND (qq.PlaybackBucket = @TodayBucket
                         OR qq.[Order] > 0)
                 )
                 GROUP BY vr.RecommendedVideoId

                 UNION

                 -- Get videos from recommendations based on today's tracks
                 SELECT TOP (@MaxCount) vr.RecommendedVideoId AS VideoId,
                                        0 AS PlayCount,
                                        COUNT(vr.VideoId) AS Weight
                 FROM VideoRecommendations AS vr
                 WHERE vr.VideoId IN (
                     SELECT TOP(@MaxCount) fq.VideoId
                     FROM QueueEntries AS fq
                     WHERE fq.UserId = @UserId
                     GROUP BY fq.VideoId
                     ORDER BY COUNT(fq.QueueId) DESC
                 )
                 GROUP BY vr.RecommendedVideoId
             )
    SELECT DISTINCT V.VideoId,
                    V.YouTubeVideoId,
                    V.Title,
                    V.Duration,
                    V.ThumbnailUrl,
                    C.LastPlayed,
                    C.PlayCount,
                    T.Weight
    FROM  Videos V WITH (INDEX (FI_Videos_WithNotUnavailable) )
              INNER JOIN (
        SELECT DISTINCT VideoId, SUM(Weight) AS Weight
        FROM Recommendations
        GROUP BY VideoId
    ) T
                         ON T.VideoId = V.VideoId
              LEFT OUTER JOIN PlaybackCounts AS C
                              ON C.VideoId = V.VideoId
    WHERE V.Unavailable = 0
    ORDER BY T.Weight DESC, C.PlayCount DESC, C.LastPlayed DESC

END
