DECLARE @UserId int = (SELECT UserId FROM [User] WHERE Username = 'Mike')

BEGIN

    DECLARE @BucketAnchor datetime2 = CAST('1900-01-01 12:00:00.000' AS datetime2)
    DECLARE @SessionBucket date = CAST(DATE_BUCKET(day, 1, CAST(getdate() AS datetime2), @BucketAnchor) AS date)
    
    SELECT DISTINCT PlaybackBucket,
                    COUNT(VideoId) AS VideoCount
    FROM QueueEntries WITH ( INDEX ( FI_QueueEntries_WithPlaybackDate ) )
    WHERE UserId = @UserId
      AND PlaybackDate IS NOT NULL
      AND PLaybackBucket != @SessionBucket
    GROUP BY PlaybackBucket
    ORDER BY PlaybackBucket DESC

END