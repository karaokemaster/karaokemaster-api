BEGIN

    SELECT DISTINCT PlaybackBucket,
                    COUNT(VideoId) AS VideoCount
    FROM QueueEntries
    WHERE PlaybackBucket = CAST(@PlaybackBucket AS date)
    GROUP BY PlaybackBucket

END