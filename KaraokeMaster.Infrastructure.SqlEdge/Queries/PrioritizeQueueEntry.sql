DECLARE @QueueId INT = 2021

BEGIN

    DECLARE @BucketAnchor datetime2 = CAST('1900-01-01 12:00:00.000' AS datetime2)
    DECLARE @SessionBucket date = CAST(DATE_BUCKET(day, 1, CAST(getdate() AS datetime2), @BucketAnchor) AS date)

    ;
    WITH TodayTracks AS
             (
                 SELECT
                     QueueId,
                     UserId,
                     ROW_NUMBER() OVER (ORDER BY [PlaybackDate] DESC) AS TracksAgo
                 FROM QueueEntries
                 WHERE PlaybackBucket = @SessionBucket
             ),
         UserHistory AS (
             SELECT
                 U.UserId,
                 ISNULL(MIN(T.TracksAgo), 9999) AS SinceLastTrack,
                 COUNT(Q.QueueId) AS TracksInQueue
             FROM Users U
                      LEFT OUTER JOIN TodayTracks T
                                      ON T.UserId = U.UserId
                      LEFT OUTER JOIN QueueEntries Q
                                      ON U.UserId = Q.UserId
                                          AND Q.[Order] > 0
             GROUP BY U.UserId
         ),
         PriorityQueueOrder AS
             (
                 SELECT QueueId,
                        [Order],
                        ROW_NUMBER() OVER (ORDER BY [Order]) * 10 AS WideOrder,
                        ROW_NUMBER() OVER (ORDER BY SinceLastTrack DESC, [Order]) * 10 + 5 AS PriorityOrder,
                        TracksInQueue
                 FROM QueueEntries Q
                          INNER JOIN UserHistory H
                                     ON H.UserId = Q.UserId
                 WHERE [Order] > 0
             ),
         QueueOrder AS
             (
                 SELECT Q.QueueId,
                        [Order],
                        ROW_NUMBER() OVER (ORDER BY IIF(Q.QueueId = @QueueId AND TracksInQueue <= 1, PriorityOrder, WideOrder)) AS NewOrder
                 FROM PriorityQueueOrder Q
             )
    UPDATE QueueOrder
    SET [Order] = NewOrder
    WHERE [Order] > 0

END