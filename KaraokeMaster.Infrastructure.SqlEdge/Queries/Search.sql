DECLARE @Query nvarchar(256) = '1975'

BEGIN
    DECLARE @QueryParts TABLE (
        value nvarchar(256)
                              )
    DECLARE @PartCount INT
    INSERT INTO @QueryParts SELECT value FROM STRING_SPLIT(@Query, ' ')
    SELECT @PartCount = COUNT(value) FROM @QueryParts

    SELECT *
    FROM Videos
    WHERE VideoId IN (
        SELECT TOP 50 v.VideoId
        FROM dbo.Videos v
                 LEFT JOIN @QueryParts q
                           ON v.Title LIKE '%' + q.value + '%'
        GROUP BY v.VideoId
        HAVING COUNT(q.value) = @PartCount
    )
      AND Unavailable = 0
END
