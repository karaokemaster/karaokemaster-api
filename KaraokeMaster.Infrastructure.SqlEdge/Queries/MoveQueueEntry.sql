DECLARE @QueueId INT = 1014
DECLARE @AfterId INT = NULL
DECLARE @BeforeId INT = 5

BEGIN 
BEGIN TRANSACTION

    DECLARE @NewOrder INT

    IF (@BeforeId IS NOT NULL AND @BeforeId > -1)
        BEGIN
            SELECT @NewOrder = [Order]
            FROM QueueEntries
            WHERE QueueId = @BeforeId

            UPDATE QueueEntries
            SET [Order] = [Order] + 1
            WHERE [Order] >= (SELECT [Order] FROM QueueEntries WHERE QueueId = @BeforeId)
        END
    ELSE
        BEGIN
            SELECT @NewOrder = [Order] + 1
            FROM QueueEntries
            WHERE QueueId = @AfterId
        END

    UPDATE QueueEntries
    SET [Order] = @NewOrder
    WHERE QueueId = @QueueId

    ;
    WITH QueueOrder AS
             (
                 SELECT
                     QueueId,
                     [Order],
                     ROW_NUMBER() OVER (ORDER BY [Order]) AS NewOrder
                 FROM QueueEntries
                 WHERE [Order] > 0
             )
    UPDATE QueueOrder
    SET [Order] = NewOrder
    WHERE [Order] > 0

COMMIT TRANSACTION
END 
