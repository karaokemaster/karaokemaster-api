// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using Autofac;
using KaraokeMaster.Application.Autofac;
using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nest;

namespace KaraokeMaster.Infrastructure.ElasticSearch
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(x =>
            {
                var configuration = x.Resolve<IOptionsMonitor<ElasticSearchConfiguration>>();

                var settings = new ConnectionSettings(new Uri(configuration.CurrentValue.SearchEndpoint))
                    .DefaultMappingFor<Video>(v => v
                        .Ignore(p => p.PlayCount)
                        .Ignore(p => p.LastPlayed)
                        .IndexName($"{configuration.CurrentValue.IndexPrefix}_videos")
                        .IdProperty(p => p.VideoId)
                    )
                    .DefaultMappingFor<QueueEntry>(q => q
                        .Ignore(p => p.User)
                        .Ignore(p => p.Video)
                        .Ignore(p => p.UserOptions)
                        .IndexName($"{configuration.CurrentValue.IndexPrefix}_playbackevents")
                        .IdProperty(p => p.QueueId)
                    )
                    ;
        
                var client = new ElasticClient(settings);

                return client;
            });

            builder.RegisterType<SearchProvider>().As<ISearchProvider>();

            builder.RegisterType<IndexingService>()
                .As<IHostedService>()
                .InstancePerDependency();

            builder.RegisterConfig<ElasticSearchConfiguration>(ElasticSearchConfiguration.ConfigurationKey);
            
            base.Load(builder);
        }
    }
}