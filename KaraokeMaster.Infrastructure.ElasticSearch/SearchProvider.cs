// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using Sentry;

namespace KaraokeMaster.Infrastructure.ElasticSearch;

public class SearchProvider : ISearchProvider
{
    private readonly ILogger<SearchProvider> _logger;
    private readonly IOptionsMonitor<ElasticSearchConfiguration> _elasticSearchConfiguration;
    private readonly Func<ElasticClient> _elasticClientFactory;

    public SearchProvider(ILoggerFactory loggerFactory, IOptionsMonitor<ElasticSearchConfiguration> elasticSearchConfiguration, Func<ElasticClient> elasticClientFactory)
    {
        _logger = loggerFactory.CreateLogger<SearchProvider>();
        _elasticSearchConfiguration = elasticSearchConfiguration;
        _elasticClientFactory = elasticClientFactory;
    }
    
    public async Task AddOrUpdate(Video video)
    {
        var span = SentrySdk.GetSpan()?.StartChild("SearchProvider.AddOrUpdate");
            
        using (_logger.BeginScope("Index AddOrUpdate video"))
        {
            try
            {
                var client = _elasticClientFactory();

                var result = await client.IndexDocumentAsync(video);
        
                // TODO: Act on response
                _logger.LogInformation($"Video indexed - errors: {!result.IsValid}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddOrUpdate");
                span?.Finish(SpanStatus.InternalError);
                throw;
            }
        }
    }

    public async Task AddOrUpdate(IEnumerable<Video> videos)
    {
        var span = SentrySdk.GetSpan()?.StartChild("SearchProvider.AddOrUpdate");
            
        using (_logger.BeginScope("Index AddOrUpdate multiple videos"))
        {
            try
            {
                var client = _elasticClientFactory();

                var result = await client.IndexManyAsync(videos);
        
                // TODO: Act on response
                _logger.LogInformation($"Videos indexed - errors: {result.Errors}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddOrUpdate");
                span?.Finish(SpanStatus.InternalError);
                throw;
            }
        }
    }
    
    public async Task AddOrUpdate(QueueEntry entry)
    {
        var span = SentrySdk.GetSpan()?.StartChild("SearchProvider.AddOrUpdate");
            
        using (_logger.BeginScope("Index AddOrUpdate queue entry"))
        {
            try
            {
                var client = _elasticClientFactory();

                var result = await client.IndexDocumentAsync(entry);
        
                // TODO: Act on response
                _logger.LogInformation($"Entry indexed - errors: {!result.IsValid}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddOrUpdate");
                span?.Finish(SpanStatus.InternalError);
                throw;
            }
        }
    }

    public async Task AddOrUpdate(IEnumerable<QueueEntry> entries)
    {
        var span = SentrySdk.GetSpan()?.StartChild("SearchProvider.AddOrUpdate");
            
        using (_logger.BeginScope("Index AddOrUpdate multiple queue entries"))
        {
            try
            {
                var client = _elasticClientFactory();

                var result = await client.IndexManyAsync(entries);
        
                // TODO: Act on response
                _logger.LogInformation($"Entries indexed - errors: {result.Errors}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddOrUpdate");
                span?.Finish(SpanStatus.InternalError);
                throw;
            }
        }
    }
    
    public async Task<List<Video>> Search(string query)
    {
        var span = SentrySdk.GetSpan()?.StartChild("SearchProvider.Search");
        span?.SetExtra(nameof(query), query);

        using (_logger.BeginScope("Search"))
        {
            try
            {
                var client = _elasticClientFactory();

                var results = await client.SearchAsync<Video>(v => v
                    .Query(q => q
                        .Bool(b => b
                            .Must(mu => mu
                                .Match(m => m
                                    .Field(f => f.Title)
                                    .Query(query)
                                    .Fuzziness(Fuzziness.Auto)
                                )
                            )
                            .Filter(fi => fi.Term(f => f.Unavailable, false))
                        )
                    )
                );

                return results.Documents.ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in Search");
                span?.Finish(SpanStatus.InternalError);
                throw;
            }
        }
    }
}