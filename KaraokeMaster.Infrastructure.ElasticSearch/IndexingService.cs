// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using KaraokeMaster.Domain.Models;
using KaraokeMaster.Services.Search;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sentry;

namespace KaraokeMaster.Infrastructure.ElasticSearch
{
    public class IndexingService : IHostedService, IDisposable
    {
        private readonly ILogger<IndexingService> _logger;
        private readonly IVideoRepository _videoRepository;
        private readonly IQueueRepository _queueRepository;
        private readonly ISearchProvider _searchProvider;

        public IndexingService(ILoggerFactory loggerFactory, IVideoRepository videoRepository, IQueueRepository queueRepository, ISearchProvider searchProvider)
        {
            _logger = loggerFactory.CreateLogger<IndexingService>();
            _videoRepository = videoRepository;
            _queueRepository = queueRepository;
            _searchProvider = searchProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting ElasticSearch indexing service");

            // // TODO: Allow configuration of backup time, frequency
            //
            // var dueTime = DateTime.Today.AddDays(1.5) - DateTime.Now;
            // if (dueTime.TotalHours > 24)
            //     dueTime = dueTime.Subtract(TimeSpan.FromDays(1));
            //
            // _timer = new Timer(state =>
            // {
            //     SentrySdk.ConfigureScope(scope =>
            //     {
            //         var transaction = SentrySdk.StartTransaction("BackupService", "TimerInterval");
            //         scope.Transaction = transaction;
            //
            //         DoBackup();
            //         PurgeStaleBackups();
            //         
            //         transaction.Finish();
            //     });
            // }, null, dueTime, TimeSpan.FromDays(1));

            IndexAll();
            
            return Task.CompletedTask;
        }

        private async Task IndexAll()
        {
            var span = SentrySdk.GetSpan()?.StartChild("SqlBackup");
            
            using (_logger.BeginScope("Indexing service"))
            {
                _logger.LogInformation("Indexing all videos...");
                
                // TODO: Chunks
                var videos = await _videoRepository.GetAll();

                await _searchProvider.AddOrUpdate(videos);
                
                _logger.LogInformation("Indexing all videos complete");
                
                
                _logger.LogInformation("Indexing all queue entries...");
                
                // TODO: Chunks
                var entries = await _queueRepository.GetAll();

                await _searchProvider.AddOrUpdate(entries);
                
                _logger.LogInformation("Indexing all queue entries complete");
                
            }
            
            span?.Finish();
        }
        
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping ElasticSearch indexing service");

            // _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            // _timer?.Dispose();
        }
    }
}